var dataSets = {}

exports.create = function(code, fields, key, n, values) {
	var ds = {key:key, fields:fields, rows:[]};
	console.log("values:", typeof values);
	if (typeof values == "function") {
		for (var i=1; i<=n; i++) {
			ds.rows.push(values(i));
		}
	} else {
		for (var i=1; i<=n; i++) {
			var r = {};
			fields.forEach((f, j)  => {
				var v = values[j];
				if (v == "true") v = true;
				else if (v == "false") v = false;
				else if (v == "string") v = f + " " + i;
				else if (v == "correlativo") v = i;
				else if (!isNaN(parseFloat(v))) v = parseFloat(v);
				r[f] = v;
			})
			ds.rows.push(r);
		}
	}
	dataSets[code] = ds;
	console.log("creado dataSet " + code + " con " + n + " registros de prueba");
}
exports.exists = function(code) {
	return dataSets[code]?true:false;
}
exports.remove = function(code) {
	delete dataSets[code];
}
exports.getRows = function(code) {
	return dataSets[code].rows;
}
exports.getRow = function(code, keyValue) {	
	var ds = dataSets[code];
	var found = null;
	ds.rows.forEach((row) => {
		if (row[ds.key] == keyValue) {
			found = row;
			return false;
		}
	});
	return found;
}
exports.getRowIndex = function(code, keyValue) {	
	var ds = dataSets[code];
	var idx = -1;
	ds.rows.forEach((row, index) => {
		if (row[ds.key] == keyValue) {
			idx = index;
			return false;
		}
	});
	return idx;
}
exports.addRow = function(code, row) {
	var ds = dataSets[code];
	if (row[ds.key]) {
		if (exports.getRow(code, row[ds.key])) throw("Registro duplicado");
	} else {
		var max = 0;
		ds.rows.forEach(r => {
			if (r[ds.key] > max) max = r[ds.key];
		});
		row[ds.key] = max + 1;
	}
	
	ds.rows.push(row);
}
exports.updateRow = function(code, row) {
	var ds = dataSets[code];
	var r = exports.getRow(code, row[ds.key]);
	ds.fields.forEach(f => r[f] = row[f]);
}
exports.deleteRow = function(code, keyValue) {
	var idx = exports.getRowIndex(code, keyValue);
	dataSets[code].rows.splice(idx, 1);
}