var zDBG = {
}
exports.zDBG = zDBG;

exports.module = {
	getInitialPath:function() {
		return require("path").resolve("./");
	},
	getSubDirs:function(path) {
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.readdir(path, (err, entries) => {
				var promises = [];
				entries.forEach((entry, index) => {
					if (!entry.startsWith(".")) {
						var subItem = path + "/" + entry;
						promises.push(new Promise((onOk2, onError2) => {
							fs.stat(subItem, (err, stat) => {
								if (!err) onOk2({filename:subItem, stat:stat});
								else onError2(err);
							});
						}));
					}
				});
				Promise.all(promises)
				.then((stats) => {
					var subdirs = [];
					stats.forEach((item, index) => {
						if (item.stat.isDirectory()) subdirs.push(item.filename);
					});
					subdirs.sort(function (a, b) {
					    return a.toLowerCase().localeCompare(b.toLowerCase());
					});
					onOk(subdirs);
				})
				.catch((err) => onError(err));
			});			
		});
	},
	getNivelArbolArchivos:function(path, nivel) {
		var self = this;
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.readdir(path, (err, entries) => {
				if (err) {
					onError(err);
					return;
				}
				var promises = [];
				entries.forEach((entry, index) => {
					if (!entry.startsWith(".")) {
						var subItem = path + "/" + entry;
						promises.push(new Promise((onOk2, onError2) => {
							fs.stat(subItem, (err, stat) => {
								if (!err) onOk2({filename:subItem, stat:stat});
								else onError2(err);
							});
						}));
					}
				});
				Promise.all(promises)
				.then((stats) => {
					var promises = [];
					var controles = {};
					var candidatos = {};
					stats.forEach((item) => {
						if (item.stat.isDirectory()) {
							var subnivel = [];
							var lastName = item.filename.substring(item.filename.lastIndexOf("/") + 1);
							var dir = {tipo:"folder", path:item.filename, subnivel:subnivel, name:lastName};
							nivel.push(dir);
							promises.push(self.getNivelArbolArchivos(item.filename, subnivel));
						} else {
							var f = item.filename;
							var p = f.lastIndexOf(".");
							if (p) {
								var ext = f.substring(p+1).toLowerCase();
								var name = f.substring(0, p);
								var lastName = name.substring(name.lastIndexOf("/") + 1);
								if (ext == "html" || ext == "js") {
									if (controles[name]) {
										nivel.push({tipo:"control", path:name, name:lastName});
										delete candidatos[name + (ext == "html"?".js":".html")];
									} else {
										controles[name] = true;
										candidatos[f] = true;
									}
								} else if (ext == "css") {
									nivel.push({tipo:"css", path:f, name:f.substring(f.lastIndexOf("/") + 1)});
								} else if (ext == "znav") {
									nivel.push({tipo:"znav", path:f, name:f.substring(f.lastIndexOf("/") + 1)});
								}
							}
						}
					});
					var keys = Object.keys(candidatos);
					for (var i=0; i<keys.length; i++) {
						var f = keys[i];
						var tipo = f.endsWith(".js")?"js":"html";
						var name = f.substring(f.lastIndexOf("/") + 1);
						nivel.push({tipo:tipo, path:f, name:name});
					}
					Promise.all(promises)
					.then(() => onOk(nivel))
					.catch((err) => onError(err));
				})
				.catch((err) => onError(err));
			});
		});
	},
	ordenaNivel:function(nivel) {
		var self = this;
		nivel.sort(function (a, b) {
			if (a.tipo == "folder") {
				if (b.tipo == "folder") {
					return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
				} else {
					return -1;
				}
			} else {
				if (b.tipo == "folder") {
					return 1;
				} else {
					return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
				}
			}		    
		});
		nivel.forEach((item) => {
			if (item.tipo == "folder") self.ordenaNivel(item.subnivel);
		});
	},
	getNivelArbol:function(nivel, parent, level, arbol) {
		var self = this;
		nivel.forEach((item) => {
			arbol.push({
				id:"" + (arbol.length + 1),			
				path:item.path,
				nombre:item.name,
				tipo:item.tipo,
				parent:parent,
				level:level,
				isLeaf:(item.tipo != "folder"),
				expanded:false,
				loaded:true
			});
			if (item.tipo == "folder") {
				self.getNivelArbol(item.subnivel, "" + arbol.length, level+1, arbol);
			}
		});
	},
	getArbolArchivos:function(path) {
		var self = this;
		return new Promise((onOk, onError) => {
			var nivel = [];
			self.getNivelArbolArchivos(path, nivel)
			.then((root) => {				
				self.ordenaNivel(root);
				var nombre = path.substring(path.lastIndexOf("/") + 1);
				var nodoRaiz = {
					id:1,			
					path:path,
					nombre:nombre,
					tipo:"folder",
					parent:null,
					level:0,
					isLeaf:false,
					expanded:true,
					loaded:true
				};
				var arbol = [nodoRaiz];
				self.getNivelArbol(root, "1", 1, arbol);
				onOk(arbol);
			})
			.catch((err) => onError(err));
		});
	},
	getVista:function(path) {
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.readFile(path + ".html", "utf8", function(err, data) {
				if (err) onError(err);
				else onOk(data);
			});
		});
	},
	getControlador:function(path) {
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.readFile(path + ".js", "utf8", function(err, data) {
				if (err) onError(err);
				else onOk(data);
			});
		});
	},
	getControl:function(path) {
		var self = this;
		return new Promise((onOk, onError) => {
			var p = path.lastIndexOf("/");
			var control = {
				path:path,
				nombre:path.substring(p+1)
			};
			var vistaPromise = self.getVista(path);
			var controladorPromise = self.getControlador(path);
			Promise.all([vistaPromise, controladorPromise])
			.then((results) => {
				control.vista = results[0];
				control.controlador = results[1];
				onOk(control);
			})
			.catch((error) => onError(err));
		});
	},
	getArchivo:function(path) {
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.readFile(path, "utf8", function(err, data) {
				if (err) onError(err);
				else onOk(data?data:"");
			});
		});
	},
	saveControl:function(control) {
		var self = this;
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.writeFile(control.path + ".html", control.vista, "utf8", (err) => {
				if (err) {
					onError(err);
					return;
				}
				fs.writeFile(control.path + ".js", control.controlador, "utf8", (err) => {
					if (err) {
						onError(err);
						return;
					}
					self.getControl(control.path)
					.then((newControl) => onOk(newControl))
					.catch((err) => onError(err));
				});	
			});			
		});		
	},
	saveArchivo:function(path, contenido) {
		var self = this;
		return new Promise((onOk, onError) => {
			var fs = require("fs");
			fs.writeFile(path, contenido, "utf8", (err) => {
				if (err) {
					onError(err);
					return;
				}
				onOk();	
			});			
		});		
	},
	syncExists:function(path) {
		return require("fs").existsSync(path);		
	},
	creaArchivo:function(path, contenido) {
		var self = this;
		return new Promise((onOk, onError) => {
			if (self.syncExists(path)) {
				onError("El archivo ya existe");
				return;
			}
			self.saveArchivo(path, contenido)
			.then(() => onOk())
			.catch((err) => onError(err));
		});
	},
	creaControl:function(path, html, js) {
		var self = this;
		return new Promise((onOk, onError) => {
			var promesas = [
				self.creaArchivo(path + ".html", html),
				self.creaArchivo(path + ".js", js)
			];
			Promise.all(promesas)
			.then(() => onOk())
			.catch((error) => onError(error));
		});
	},
	creaMockServer:function(name) {
		var self = this;
		return new Promise((onOk, onError) => {
			var mocksDir = require("path").resolve("./") + "/_zmocks";
			if (!self.syncExists(mocksDir)) require("fs").mkdirSync(mocksDir);
			var path = mocksDir + "/" + name;
			var code = 'var ds = require("./servicios/test-data.js");\n';
			code += "\n/*\nEjemplo de dataSet:\n";
			code += 'if (!ds.exists("persona")) ds.create("persona", ["id", "rut", "nombre", "activa"], "id", 90, ["correlativo", "string", "string", true]);\n';
			code += "*/\n\n";
			code += "var mock = {\n\t\n}";
			self.creaArchivo(path, code)
			.then(() => onOk())
			.catch((err) => onError(err));
		});
	},
	creaFolder:function(path, name) {
		require("fs").mkdirSync(path + "/" + name);
	},
	renameFile:function(path, newPath) {
		require("fs").renameSync(path, newPath);
	},
	renameControl:function(path, newPath) {
		var fs = require("fs");
		fs.renameSync(path + ".html", newPath + ".html");
		fs.renameSync(path + ".js", newPath + ".js");
	},
	deleteFile:function(path) {
		require("fs").unlinkSync(path);
	},
	deleteControl:function(path) {
		var fs = require("fs");
		fs.unlinkSync(path + ".html");
		fs.unlinkSync(path + ".js");
	},
	deleteFolder:function(path) {
		return new Promise((onOk, onError) => {
			var rimraf = require("rimraf");
			rimraf(path, function() {
				onOk();
			});
		});		
	},
	startDebugSession:function(basePath, controlPath, control, opciones) {
		zDBG.basePath = basePath;
		zDBG.controlPath = controlPath;
		zDBG.control = control;
		zDBG.opciones = opciones;
	},
	inicializaDirectorio:function(basePath) {
		return new Promise((onOk, onError) => {
			var targetPath = require("path").resolve("./");
			console.log("Inicializando directorio de sitio web en '" + targetPath + "'");
			var ncp = require("ncp").ncp;
			ncp(basePath, targetPath, (err) => {
				if (err) {
					onError(err);
				} else {
					onOk();
				}
			});
		});
		
	}
}