var controller = {
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Información",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:480,
			height:240,
			modal:true,
			position:{my:"center"},
			buttons:{"Cerrar":{
			  text:"Cerrar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		
		w.find("#lblMessage").text(initialData.message);
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	init:function(w, initialData) {		
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}