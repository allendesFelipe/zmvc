var controller = {
	flowValidator:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Confirmación",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:200,
			modal:true,
			position:{my:"center"},
			buttons:{
			    cancel:{
			    	text:"Cancelar",
				  click:function() {thisController.cancel(w, initialData);}			
				}, 
				dicard:{
				  text:"Descartar",
				  click:function() {thisController.discard(w, initialData);}			
				},
				save:{
				  text:"Guardar",
				  click:function() {thisController.save(w, initialData);}
				}
			}
		});
		
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			if (initialData.onClose) initialData.onClose();
			thisController.destroyInstance(w, initialData);
		});		

		return w;
	},	
	init:function(w, initialData) {
		w.find("#lblMessage").text(initialData.message);
	},
	save:function(w, initialData) {
		if (initialData.onSave) initialData.onSave();
		w.dialog("close");
	},
	cancel:function(w, initialData) {
		if (initialData.onCancel) initialData.onCancel();
		w.dialog("close");
	},
	discard:function(w, initialData) {
		if (initialData.onDiscard) initialData.onDiscard();
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}