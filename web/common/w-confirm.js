var controller = {
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Información",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:480,
			height:240,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		
		w.find("#lblMessage").text(initialData.message);
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	init:function(w, initialData) {		
	},
	onOk:function(w, initialData) {
		w.dialog("close");
		if (initialData.onOk) initialData.onOk();
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
		if (initialData.onCancel) initialData.onCancel();
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}