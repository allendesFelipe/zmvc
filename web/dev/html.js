var controller = {
	$this:null,
	parentController:null,
	changed:false,
	path:null,
	editor:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		var cmdSave = this.$this.find("#cmdSave").button({icons:{secondary:"ui-icon-disk"}}).click(function() {
			thisController.save();
		});
		enableButton(cmdSave, false);
	},
	
	inicializa:function(parentController, path, contenido) {
		var thisController = this;
		this.parentController = parentController;
		this.path = path;
		this.changed = false;
		this.$this.find("#lblTitulo").text(path);
		var editorId = "ed-a-" + guid();
		this.$this.find("#contenedor-editor").html("<div id='" + editorId + "'></div>");
		ace.require("ace/ext/language_tools");
		this.editor = ace.edit(editorId);
		this.editor.$blockScrolling = Infinity;
		this.$this.find("#contenedor-editor").zLayout("doLayout");
		this.editor.setTheme("ace/theme/eclipse");
		this.editor.getSession().setMode("ace/mode/html");
		this.editor.setOptions({
	        enableBasicAutocompletion: true,
	        enableSnippets: true,
	        enableLiveAutocompletion: false
	    });
		if (contenido) this.editor.insert(contenido);
		this.editor.setShowPrintMargin(false);
		this.$this.find("#" + editorId).data("on-layout-resize", function() {
			thisController.editor.resize();				
		});
		this.editor.on("change", function() {
			thisController.markChanged();
		});
	},
	isChanged:function() {return this.changed;},
	markChanged:function() {
		enableButton(this.$this.find("#cmdSave"), true);
		this.changed = true;
	},
	save:function(cb) {
		var thisController = this;
		httpInvoke("saveArchivo.zdev", {path:this.path, contenido:this.editor.getValue()}, function() {
			enableButton(thisController.$this.find("#cmdSave"), false);
			thisController.changed = false;
			if (cb) cb();
		});
	}
}