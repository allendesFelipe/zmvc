var controller = {
	path:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Directorio de Trabajo",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:360,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		this.path = app.path;
		this.refrescaDirs(w, initialData);
		
		w.find("#edDir").dblclick(function() {
			thisController.path = $(this).val();
			thisController.refrescaDirs(w, initialData);
		});
						
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	refrescaDirs:function(w, initialData) {
		var thisController = this;
		var path = this.path;
		w.find("#lblPath").text(path);
		w.find("#edDir").html("<option disabled selected>Buscando ...</option>");
		httpInvoke("getSubDirs.zdev", {path:path}, function(dirs) {
			var html = "";
			if (path.length > 1) {
				var parentDir = path.substring(0, path.lastIndexOf("/"));
				html = "<option value='" + parentDir + "'>..</option>";
			} 
			dirs.forEach(function(d) {
				html += "<option value='" + d + "'>" + d.substring(path.length + 1) + "</option>";
			});
			w.find("#edDir").html(html);
		});
	},
	init:function(w, initialData) {
	},	
	onOk:function(w, initialData) {
		app.path = this.path;
		httpInvoke("setInitialPath.zdev",{path:this.path});
		w.dialog("close");
		initialData.onOk(this.path);
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}