var controller = {
	path:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Wizards de Controles Z-MVC",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:680,
			height:490,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		
		var arbol = w.find("#arbolWizards").jqGrid({
            datatype: "local",
            gridview: true,
            rowNum: 10000,
            sortname: "id",
            treeGrid: true,            
            treeGridModel: 'adjacency',
            treedatatype: "local",
            ExpandColumn: "nombre",
            ExpandColClick: true,            
            autowidth: true,
            hidegrid: false,
            shrinkToFit: false,
            colNames: ["id", "Wizards", "tipo"],
            colModel: [
              { name: "id", index: "id", key: true, hidden: true },
			  { name: "nombre", index: "nombre", width: 250 },
              { name: "tipo", index: "tipo", hidden: true }
            ],
            caption: false,
            onSelectRow:function() {
            	thisController.refrescaDetalles(w, initialData);           	
            }            
        });
        attachJQGridToContainerPanel(w.find("#arbolWizardsContainer"), arbol, true);
					
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		w.keyup(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				thisController.onOk(w, initialData);
				return false;
			}
		});
		
		return w;
	},
	init:function(w, initialData) {
		var arbol = w.find("#arbolWizards");
		arbol.jqGrid("clearGridData");
		arbol[0].addJSONData(getArbolWizardsMVC());
		this.refrescaDetalles(w, initialData);
	},
	getNodoSeleccionado:function(w, initialData) {
		var arbol = w.find("#arbolWizards");
		var id = arbol.jqGrid("getGridParam", "selrow");
		if (!id) return null;
		return arbol.jqGrid("getRowData", id);
	},
	getWizard:function(w, initialData) {
		var n = this.getNodoSeleccionado(w, initialData);
		if (!n || n.tipo != "wizard") return null;
		return getWizardMVC(n.id);
	},
	refrescaDetalles:function(w, initialData) {
		w.find("#img").hide();
		w.find("#lblHelp").html("");
		var wz = this.getWizard(w, initialData);
		if (!wz) return;
		w.find("#img").show();
		w.find("#img").prop("src", "dev/mvc-wizards/" + wz.codigo + "/" + wz.nombreImagen);
		w.find("#lblHelp").html(wz.help);
	},
	onOk:function(w, initialData) {
		var wz = this.getWizard(w, initialData);
		if (!wz) return;
		var nombre = w.find("#edNombre").val();
		if (!nombre) {
			openDialog("common/w-error", {message:"Debe indicar el nombre de archivo del nuevo control"});
			return;
		}
		if (wz.esVentana) {
			if (!nombre.startsWith("w")) {
				openDialog("common/w-error", {message:"El wizard seleccionado genera una ventana. El nombre del control debe comenzar com 'w'"});
				return;
			}
		} else {
			if (nombre.startsWith("w")) {
				openDialog("common/w-error", {message:"El wizard seleccionado genera un control tipo panel. El nombre del control no debe comenzar com 'w'"});
				return;
			}
		}
		w.dialog("close");
		initialData.onOk(wz, nombre);
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}