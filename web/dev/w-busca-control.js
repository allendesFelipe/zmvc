var controller = {
	path:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Buscar Control Z-MVC",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:360,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		
		var arbol = w.find("#arbolBuscador").jqGrid({
            datatype: "local",
            gridview: true,
            rowNum: 10000,
            sortname: "id",
            treeGrid: true,            
            treeGridModel: 'adjacency',
            treedatatype: "local",
            ExpandColumn: "nombre",
            ExpandColClick: true,            
            autowidth: true,
            hidegrid: false,
            shrinkToFit: false,
            colNames: ["id", "path", "", "Nombre", "tipo"],
            colModel: [
              { name: "id", index: "id", key: true, hidden: true },
			  { name: "path", index: "path", hidden: true },
			  { name: "imgTipo", index: "imgTipo", width: 30, align:"center" },
			  { name: "nombre", index: "nombre", width: 250 },
              { name: "tipo", index: "tipo", hidden: true }
            ],
            caption: false,
            ondblClickRow:function(rowId, iRow, iCol, e) {
            	thisController.onOk(w, initialData);
            }
        });
        attachJQGridToContainerPanel(w.find("#arbolBuscadorContainer"), arbol, true);
        arbol[0].addJSONData(app.arbolProyecto);
						
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	getNodoSeleccionado:function(w, initialData) {
		var arbol = w.find("#arbolBuscador");
		var id = arbol.jqGrid("getGridParam", "selrow");
		if (!id) return null;
		return arbol.jqGrid("getRowData", id);
	},	
	init:function(w, initialData) {
	},	
	onOk:function(w, initialData) {
		var n = this.getNodoSeleccionado(w, initialData);
		if (!n || n.tipo != "control") return;
		w.dialog("close");
		initialData.onOk(n);
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}