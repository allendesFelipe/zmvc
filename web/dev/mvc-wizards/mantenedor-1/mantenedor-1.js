/*
contrato::{
    "metodos": [
        "refresca()"
    ],
    "eventos": [
        "cambioSeleccion(row)"
    ]
}
*/
var controller = {
	buscador:null,
	refresca:function() {
	    this.lista.refresh();
	},
	onLista_getRows:function(callback) {
		var thisController = this;
		if (this.buscador) this.buscador.abort();
	    this.buscador = httpInvoke("getTabla1.dat", {valor1:this.edSelect1.val(), filtro:this.edBuscar.val()}, function(rows) {
	    	thisController.buscador = null;
	        callback(rows);
	    });
	},
	onEdSelect1_change:function() {this.refresca();},
	onEdBuscar_change:function() {this.refresca();},
	onCmdLimpiadorBuscar_click:function() {this.edBuscar.clear();},
	onLista_rowSelected:function(rowIndex, row) {this.trigger("cambioSeleccion", row);},
	onCmdAgregar_click:function() {
	    var thisController = this;
	    openDialog("${pathRelativo}/wed-${nombre}", {nuevoRegistro:true}, function(record) {
	        thisController.lista.update();
	    });
	},
	onLista_editRequest:function(rowIndex, row) {
	    var thisController = this;
	    openDialog("${pathRelativo}/wed-${nombre}", {nuevoRegistro:false, record:row}, function(record) {
	        thisController.lista.updateRow(rowIndex, record);
	        thisController.trigger("cambioSeleccion", thisController.lista.getSelectedRow());
	    });
	},
	onLista_deleteRequest:function(rowIndex, row) {
	    var thisController = this;
	    openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar el registro '" + row.nombre + "'?"}, function() {
	        httpInvoke("deleteRecord.dat", {codigo:row.codigo}, function() {
	           thisController.lista.deleteRow(rowIndex);
	           thisController.trigger("cambioSeleccion", thisController.lista.getSelectedRow());
	        });
	    });
	}
};