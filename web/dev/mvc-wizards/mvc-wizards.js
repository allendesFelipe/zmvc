var mvcWizards = [];
var mvcArbolWizards = null;

/* Uso General */
mvcWizards.push({
	codigo:"vacio",
	grupo:"Uso General",
	nombre:"Panel Vacío",
	help:"Genera un Panel Z-MVC vacío",
	nombreImagen:"vacio.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:false
});

mvcWizards.push({
	codigo:"main-1",
	grupo:"Uso General",
	nombre:"Panel 'main' para login -> menu",
	help:"Panel con pager que apunta a 'login/login' y 'main/menu'. Incluye lógica de control para eventos de login y logout",
	nombreImagen:"vacio.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"main", nombreArchivo:"${nombre}"}
	]
});

mvcWizards.push({
	codigo:"menu-1",
	grupo:"Uso General",
	nombre:"Menú Principal con Loader",
	help:"Panel con menú principal y un loader",
	nombreImagen:"menu-1.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"menu", nombreArchivo:"${nombre}"}
	]
});

mvcWizards.push({
	codigo:"w-vacia-ok-cancel",
	grupo:"Uso General",
	nombre:"Diálogo Vacío - Aceptar / Cancelar",
	help:"Ventana de Diálogo vacía con botones 'Aceptar' y 'Cancelar'",
	nombreImagen:"w-vacia-ok-cancel.png",
	esVentana:true,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"w-vacia-ok-cancel", nombreArchivo:"${nombre}"}
	]
});

mvcWizards.push({
	codigo:"w-vacia-close",
	grupo:"Uso General",
	nombre:"Diálogo Vacío - Cerrar",
	help:"Ventana de Diálogo vacía con botón 'Cerrar'",
	nombreImagen:"w-vacia-close.png",
	esVentana:true,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"w-vacia-close", nombreArchivo:"${nombre}"}
	]
});

/* Mantenedores */
mvcWizards.push({
	codigo:"w-ed-1",
	grupo:"Mantenedores",
	nombre:"Ventana Básica de Edición",
	help:"Ventana Modal para Edición de un Registro",
	nombreImagen:"w-ed-1.png",
	esVentana:true,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"w-ed-1", nombreArchivo:"${nombre}"}
	]
});
mvcWizards.push({
	codigo:"mantenedor-1",
	grupo:"Mantenedores",
	nombre:"Mantenedor con Filtro",
	help:"Panel con tabla, opciones de mantenedor y ventana básica de edición de un registro",
	nombreImagen:"mantenedor-1.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"mantenedor-1", nombreArchivo:"${nombre}"},
		{plantilla:"w-ed-1", nombreArchivo:"wed-${nombre}"}
	]
});

/* Login */
mvcWizards.push({
	codigo:"login-1",
	grupo:"Login",
	nombre:"Panel de Login por e-mail",
	help:"Panel de Login con ventana de cambio de contraseña. El usuario se identifica con su e-mail. Invoca servicios del módulo 'seg'",
	nombreImagen:"login-1.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"login", nombreArchivo:"${nombre}"},
		{plantilla:"w-cambiar-pwd", nombreArchivo:"w-${nombre}-cambiar-pwd"},
		{plantilla:"p-cambiar-pwd", nombreArchivo:"p-${nombre}-cambiar-pwd"},
		{plantilla:"p-enviar-codigo", nombreArchivo:"p-${nombre}-enviar-codigo"}
	]
});

mvcWizards.push({
	codigo:"login-2",
	grupo:"Login",
	nombre:"Panel de Login por RUT",
	help:"Panel de Login con ventana de cambio de contraseña. El usuario se identifica con su RUT. Invoca servicios del módulo 'seg'",
	nombreImagen:"login-2.png",
	esVentana:false,
	tieneVentana:false,
	usaPlantillas:[
		{plantilla:"login", nombreArchivo:"${nombre}"},
		{plantilla:"w-cambiar-pwd", nombreArchivo:"w-${nombre}-cambiar-pwd"},
		{plantilla:"p-cambiar-pwd", nombreArchivo:"p-${nombre}-cambiar-pwd"},
		{plantilla:"p-enviar-codigo", nombreArchivo:"p-${nombre}-enviar-codigo"}
	]
});

function getWizardMVC(codigo) {
	var found = null;
	mvcWizards.forEach((w) => {
		if (w.codigo == codigo) {
			found = w;
			return false;
		}
	});
	return found;
}
function getArbolWizardsMVC() {
	if (mvcArbolWizards) return mvcArbolWizards;
	var grupos = [];
	var wizardsEnGrupo = {};
	mvcWizards.forEach((w) => {
		if (grupos.indexOf(w.grupo) < 0) {
			grupos.push(w.grupo);
			wizardsEnGrupo[w.grupo] = [w];
		} else {
			wizardsEnGrupo[w.grupo].push(w);
		}
	});
	var arbol = [];
	grupos.forEach((g) => {
		arbol.push({
			id:"G_" + g,						
			nombre:g,
			tipo:"grupo",
			parent:null,
			level:0,
			isLeaf:false,
			expanded:false,
			loaded:true
		});
		wizardsEnGrupo[g].forEach((w) => {
			arbol.push({
				id:w.codigo,						
				nombre:w.nombre,
				tipo:"wizard",
				parent:"G_" + g,
				level:1,
				isLeaf:true,
				expanded:false,
				loaded:true
			});
		});
	});
	return arbol;
}