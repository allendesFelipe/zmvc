/*
contrato::{
    "metodos": [
        "inicializa(rut)",
        "enviarCodigo()"
    ],
    "eventos": [
        "codigoEnviado(rut)",
        "errorCodigo()"
    ]
}
*/
var controller = {
    inicializa:function(rut) {
        this.edRut.val(rut);
    },
	enviarCodigo:function() {
		thisController = this;
		this.mensajeError.hide();
		var rut = this.edRut.val();
		if (!validaRut(rut)) {
		    this.edRut.setValidationError("El RUT es inválido");
		    this.trigger("errorCodigo");
		    return;
		}
		this.mensajeEnviando.show();
		httpInvoke("enviaCodigoCambioPwd.seg", {rut:rut}, function() {
		    thisController.mensajeEnviando.hide();
		    thisController.trigger("codigoEnviado", rut);
		}, function(error) {
			thisController.mensajeEnviando.hide();
			thisController.mensajeError.text(error);
			thisController.mensajeError.show();
			thisController.trigger("errorCodigo");
		});
	}
}