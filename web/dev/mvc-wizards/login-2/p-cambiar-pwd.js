/*
contrato::{
    "metodos": [
        "inicializa(rut)",
        "cambiaPwd()"
    ],
    "eventos": [
        "cambioPwd(rut, pwd)",
        "errorPwd()"
    ]
}
*/
var controller = {
	rut:null,
	inicializa:function(rut) {
		this.rut = rut;
	},
	cambiaPwd:function() {
		var thisController = this;
		thisController.mensajeError.hide();
		httpInvoke("cambiaPwdConCodigoSeguridad.seg", {codigoSeguridad:this.edCodigoSeguridad.val(), rut:this.rut, pwd:this.edPwd1.val(), pwd2:this.edPwd2.val()}, function() {
			thisController.trigger("cambioPwd", thisController.rut, thisController.edPwd1.val());
		}, function(error) {
			thisController.mensajeError.text(error);
			thisController.mensajeError.show();
			thisController.trigger("errorPwd");
		});
	}
}