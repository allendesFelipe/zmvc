/*
contrato::{
    "metodos": [],
    "eventos": [
        "login(sesion)"
    ]
}
*/
var controller = {
	onThis_activate: function () {
	    this.edRut.clear();
	    this.edPwd.val("");
	    this.edRut.focus();
	    this.mensajeBuscando.hide();
	    this.cmdLogin.prop("disabled", true);
	},
	onCmdOlvidoPwd_click:function() {
		var thisController = this;
		openDialog("${pathRelativo}/w-${nombre}-cambiar-pwd", {rut:this.edRut.val()}, function(record) {
			thisController.edRut.val(record.rut);
			thisController.edPwd.val(record.pwd);
		});
	},
	onEdRut_change: function () {
	    this.edRut.clearValidation();
	    this.cmdLogin.prop("disabled", true);
	    var rut = this.edRut.val().trim();
	    if (!rut) return;
	    var rutValido = validaRut(rut);
	    if (!rutValido) {
	        this.edRut.setValidationError("RUT Inválido");
	        return;
	    }
	    this.cmdLogin.prop("disabled", false);
	},
	onCmdLogin_click:function() {
	    var thisController = this;
	    this.mensajeBuscando.show();
	    this.mensajeError.hide();
	    var rut = this.edRut.val();
		httpInvoke("login.seg", {rut:rut, pwd:this.edPwd.val()}, function(sesion) {
		    thisController.mensajeError.hide();
		    thisController.mensajeBuscando.hide();
			thisController.trigger("login", sesion);
		}, function(error) {
		    thisController.mensajeError.text(error);
		    thisController.mensajeBuscando.hide();
			thisController.mensajeError.show();
		});
	}
}