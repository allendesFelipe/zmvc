/*
contrato::{
    "metodos": [
        "home()"
    ],
    "eventos": [
        "logout"
    ]
}
*/
var controller = {
    onCmdLogout_click: function () {
        var thisController = this;
        openDialog("common/w-confirm", { message: "¿Confirma que desea cerrar la sesión de usuario?" }, function () {
            thisController.trigger("logout");
        });
    },
    onThis_activate: function () {
        this.home();
    },
    activaOpcion: function (opcion, subopcion) {
        this.contenedorMenu.find(".active").removeClass("active");
        this[opcion].addClass("active");
        if (subopcion) this[subopcion].addClass("active");
    },
    home:function() {
        this.onOpUno1_click();
    },
    onOpUno1_click: function () {
        this.activaOpcion("menu1", "opUno1");
        this.loader.zMVC("load", "main/op-uno/op-uno-1");
    },
    onOpUno2_click: function () {
        this.activaOpcion("menu1", "opUno2");
        this.loader.zMVC("load", "main/op-uno/op-uno-2");
    },
    onOpDos1_click: function () {
        this.activaOpcion("menu2", "opDos1");
        this.loader.zMVC("load", "main/op-dos/op-dos-1");
    },
    onOpDos2_click: function () {
        this.activaOpcion("menu2", "opDos2");
        this.loader.zMVC("load", "main/op-dos/op-dos-2");
    }
}