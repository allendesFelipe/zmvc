﻿var controller = {
    onPagerMain_login: function (sesion) {
        sesion.tienePrivilegio = function (privilegio) {
            return sesion.privilegios[privilegio]?true:false;
        }
        app.sesion = sesion;
        securityToken = sesion.token;
        this.pagerMain.zMVC("page", "menu").home();
    },
    onPagerMain_logout: function () {
        app.sesion = null;
        securityToken = null;
        this.pagerMain.zMVC("page", "login");
    }
}