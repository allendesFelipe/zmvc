var controller = {
	options:null,
    onThis_init:function(options) {
        this.options = options;
        if (!options.nuevoRegistro) {
        	var r = options.record;
        	this.edCodigo.val(r.codigo);
        	this.edCodigo.prop("disabled", true);
        	this.edNombre.val(r.nombre);
        }
        this.titulo.text(options.nuevoRegistro?"Crear registro":"Editar registro");
    },
	onCmdCancelar_click:function() {
		this.cancel();
	},	
	onCmdAceptar_click:function() {
		var thisController = this;
		var valido = true;
		this.edCodigo.clearValidation();
		var codigo = this.edCodigo.val();
		if (!codigo) {
			valido = false;
			this.edCodigo.setValidationError("Debe ingresar un código");
		}
		this.edNombre.clearValidation();
		var nombre = this.edNombre.val();
		if (!nombre) {
			valido = false;
			this.edNombre.setValidationError("Debe ingresar un nombre");
		}
		if (!valido) return;
		var record = this.options.nuevoRegistro?{}:this.options.record;
		record.codigo = codigo;
		record.nombre = nombre;
		if (this.options.nuevoRegistro) {
			httpInvoke("addRecord.dat", {record:record}, function() {
				thisController.close(record);
			});			
		} else {
			httpInvoke("saveRecord.dat", {record:record}, function() {
				thisController.close(record);
			});
		}
	}
}