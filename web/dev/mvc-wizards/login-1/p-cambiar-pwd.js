/*
contrato::{
    "metodos": [
        "inicializa(email)",
        "cambiaPwd()"
    ],
    "eventos": [
        "cambioPwd(email, pwd)"
    ]
}
*/
var controller = {
	email:null,
	inicializa:function(email) {
		this.email = email;
	},
	cambiaPwd:function() {
		var thisController = this;
		thisController.mensajeError.hide();
		httpInvoke("cambiaPwdConCodigoSeguridad.seg", {codigoSeguridad:this.edCodigoSeguridad.val(), email:this.email, pwd:this.edPwd1.val(), pwd2:this.edPwd2.val()}, function() {
			thisController.trigger("cambioPwd", thisController.email, thisController.edPwd1.val());
		}, function(error) {
			thisController.mensajeError.text(error);
			thisController.mensajeError.show();
			thisController.trigger("errorPwd");
		});
	}
}