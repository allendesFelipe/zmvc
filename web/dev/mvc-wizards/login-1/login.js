/*
contrato::{
    "metodos": [],
    "eventos": [
        "login(sesion)"
    ]
}
*/
var controller = {
	onThis_activate: function () {
	    this.edEmail.clear();
	    this.edPwd.val("");
	    this.edEmail.focus();
	    this.mensajeBuscando.hide();
	    this.cmdLogin.prop("disabled", true);
	},
	onCmdOlvidoPwd_click:function() {
		var thisController = this;
		openDialog("${pathRelativo}/w-${nombre}-cambiar-pwd", {email:this.edEmail.val()}, function(record) {
			thisController.edEmail.val(record.email);
			thisController.edPwd.val(record.pwd);
		});
	},
	onEdEmail_change: function () {
	    this.edEmail.clearValidation();
	    this.cmdLogin.prop("disabled", true);
	    var email = this.edEmail.val().trim();
	    if (!email) return;
	    var emailValido = validaEmail(email);
	    if (!emailValido) {
	        this.edEmail.setValidationError("e-mail Inválido");
	        return;
	    }
	    this.cmdLogin.prop("disabled", false);
	},
	onCmdLogin_click:function() {
	    var thisController = this;
	    this.mensajeBuscando.show();
	    this.mensajeError.hide();
	    var email = this.edEmail.val();
		httpInvoke("login.seg", {email:email, pwd:this.edPwd.val()}, function(sesion) {
		    thisController.mensajeError.hide();
		    thisController.mensajeBuscando.hide();
			thisController.trigger("login", sesion);
		}, function(error) {
		    thisController.mensajeError.text(error);
		    thisController.mensajeBuscando.hide();
			thisController.mensajeError.show();
		});
	}
}