/*
contrato::{
    "metodos": [
        "inicializa(email)",
        "enviarCodigo"
    ],
    "eventos": [
        "errorCodigo()",
        "codigoEnviado(email)"
    ]
}
*/
var controller = {
    inicializa:function(email) {
        this.edEmail.val(email);
    },
	enviarCodigo:function() {
		thisController = this;
		this.mensajeError.hide();
		var email = this.edEmail.val();
		if (!validaEmail(email)) {
		    this.edEmail.setValidationError("La dirección de correo es inválida");
		    this.trigger("errorCodigo");
		    return;
		}
		this.mensajeEnviando.show();
		httpInvoke("enviaCodigoCambioPwd.seg", {email:email}, function() {
		    thisController.mensajeEnviando.hide();
		    thisController.trigger("codigoEnviado", email);
		}, function(error) {
			thisController.mensajeEnviando.hide();
			thisController.mensajeError.text(error);
			thisController.mensajeError.show();
			thisController.trigger("errorCodigo");
		});
	}
}