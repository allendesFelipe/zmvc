var controller = {
    onThis_init:function(options) {
        this.pager.zMVC("getPageController", "enviar-codigo").inicializa(options.email);
    },
	onCmdCancelar_click:function() {
		this.cancel();
	},	
	onCmdAceptar_click:function() {
		var thisController = this;
		var pager = this.pager;
		this.cmdAceptar.prop("disabled", true);
		if (pager.zMVC("page") == "enviar-codigo") {
		    pager.zMVC("getPageController", "enviar-codigo").enviarCodigo();
		} else if (pager.zMVC("page") == "cambiar-pwd") {
			pager.zMVC("getPageController", "cambiar-pwd").cambiaPwd();
		}
	},
	onPager_codigoEnviado: function (email) {
	    this.pager.zMVC("getPageController", "cambiar-pwd").inicializa(email);
	    this.pager.zMVC("page", "cambiar-pwd");
	    this.cmdAceptar.text("Aceptar");
	    this.cmdAceptar.prop("disabled", false);
	},
	onPager_errorCodigo: function () {
	    this.cmdAceptar.prop("disabled", false);
	},
	onPager_cambioPwd: function (email, pwd) {
	    this.close({ email: email, pwd: pwd });
	},
	onPager_errorPwd: function () {
	    this.cmdAceptar.prop("disabled", false);
	},
}