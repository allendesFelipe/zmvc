var controller = {
	path:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:(initialData.titulo?initialData.titulo:"Crear Archivo " + initialData.tipo),
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:160,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
					
		if (initialData.etiqueta) w.find("#lblEtiqueta").text(initialData.etiqueta);
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		w.keyup(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				thisController.onOk(w, initialData);
				return false;
			}
		});
		
		return w;
	},
	init:function(w, initialData) {
		if (initialData.oldFileName) w.find("#edNombre").val(initialData.oldFileName);
	},	
	onOk:function(w, initialData) {
		var nombre = w.find("#edNombre").val();
		if (!nombre) {
			openDialog("common/w-error", {message:"Debe indicar el nombre del archivo"});
			return;
		}
		if (initialData.tipo != "control" && initialData.tipo != "folder" && !nombre.endsWith("." + initialData.tipo)) nombre += "." + initialData.tipo;
		w.dialog("close");
		initialData.onOk(nombre);
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}