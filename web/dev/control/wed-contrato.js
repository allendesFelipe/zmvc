var controller = {
	contrato:null,
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Editar Contrato del Control",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:500,
			height:360,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		
		this.contrato = initialData.contrato;
		w.find("#cmdAgregarMetodo").button({icons:{secondary:"ui-icon-circle-plus"}})
		.click(function() {
			thisController.agregarMetodo(w, initialData);
		});
		w.find("#cmdEditarMetodo").button({icons:{secondary:"ui-icon-pencil"}})
		.click(function() {
			thisController.editarMetodo(w, initialData);
		});
		w.find("#cmdEliminarMetodo").button({icons:{secondary:"ui-icon-trash"}})
		.click(function() {
			thisController.eliminarMetodo(w, initialData);
		});
		
		w.find("#edMetodo").change(function() {
			enableButton(w.find("#cmdEditarMetodo"), true);
			enableButton(w.find("#cmdEliminarMetodo"), true);
		});
		w.find("#edMetodo").dblclick(function() {
			thisController.editarMetodo(w, initialData);
		});
		
		w.find("#cmdAgregarEvento").button({icons:{secondary:"ui-icon-circle-plus"}})
		.click(function() {
			thisController.agregarEvento(w, initialData);
		});
		w.find("#cmdEditarEvento").button({icons:{secondary:"ui-icon-pencil"}})
		.click(function() {
			thisController.editarEvento(w, initialData);
		});
		w.find("#cmdEliminarEvento").button({icons:{secondary:"ui-icon-trash"}})
		.click(function() {
			thisController.eliminarEvento(w, initialData);
		});
		
		w.find("#edEvento").change(function() {
			enableButton(w.find("#cmdEditarEvento"), true);
			enableButton(w.find("#cmdEliminarEvento"), true);
		});
		w.find("#edEvento").dblclick(function() {
			thisController.editarEvento(w, initialData);
		});
		
		var tabs = w.find("#tabsContrato").tabs({active:0});
		attachElementsToTabs(tabs, [w.find("#detMetodos"), w.find("#detEventos")], [], true);
		
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	refrescaMetodos:function(w, initialData) {
		var c = this.contrato;
		enableButton(w.find("#cmdEditarMetodo"), false);
		enableButton(w.find("#cmdEliminarMetodo"), false);
		var html = "";
		$.each(c.metodos, function(i, m) {
			html += "<option value='" + i + "'>" + m + "</option>";
		});
		w.find("#edMetodo").html(html);
	},
	agregarMetodo:function(w, initialData) {
		var thisController = this;
		openDialog("dev/control/wed-metodo-contrato", {nuevoRegistro:true, onOk:function(r) {
			thisController.contrato.metodos.push(r.metodo);
			thisController.refrescaMetodos(w, initialData);
		}});
	},
	editarMetodo:function(w, initialData) {
		var thisController = this;
		var idx = parseInt(w.find("#edMetodo").val());
		if (isNaN(idx)) return;
		var r = {metodo:this.contrato.metodos[idx]};
		openDialog("dev/control/wed-metodo-contrato", {nuevoRegistro:false, record:r, onOk:function(r) {
			thisController.contrato.metodos[idx] = r.metodo;
			thisController.refrescaMetodos(w, initialData);
		}});
	},
	eliminarMetodo:function(w, initialData) {
		var thisController = this;
		var idx = parseInt(w.find("#edMetodo").val());
		if (isNaN(idx)) return;
		openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar el método seleccionado?", onOk:function() {
			thisController.contrato.metodos.splice(idx,1);
			thisController.refrescaMetodos(w, initialData);
		}});
	},
	refrescaEventos:function(w, initialData) {
		var c = this.contrato;
		enableButton(w.find("#cmdEditarEvento"), false);
		enableButton(w.find("#cmdEliminarEvento"), false);
		var html = "";
		$.each(c.eventos, function(i, e) {
			html += "<option value='" + i + "'>" + e + "</option>";
		});
		w.find("#edEvento").html(html);
	},
	agregarEvento:function(w, initialData) {
		var thisController = this;
		openDialog("dev/control/wed-evento-contrato", {nuevoRegistro:true, onOk:function(r) {
			thisController.contrato.eventos.push(r.evento);
			thisController.refrescaEventos(w, initialData);
		}});
	},
	editarEvento:function(w, initialData) {
		var thisController = this;
		var idx = parseInt(w.find("#edEvento").val());
		if (isNaN(idx)) return;
		var r = {evento:this.contrato.eventos[idx]};
		openDialog("dev/control/wed-evento-contrato", {nuevoRegistro:false, record:r, onOk:function(r) {
			thisController.contrato.eventos[idx] = r.evento;
			thisController.refrescaEventos(w, initialData);
		}});
	},
	eliminarEvento:function(w, initialData) {
		var thisController = this;
		var idx = parseInt(w.find("#edEvento").val());
		if (isNaN(idx)) return;
		openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar el evento seleccionado?", onOk:function() {
			thisController.contrato.eventos.splice(idx,1);
			thisController.refrescaEventos(w, initialData);
		}});
	},
	init:function(w, initialData) {
		this.refrescaMetodos(w, initialData);
		this.refrescaEventos(w, initialData);
	},	
	onOk:function(w, initialData) {
		w.dialog("close");
		if (initialData.onOk) initialData.onOk(this.contrato);
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}