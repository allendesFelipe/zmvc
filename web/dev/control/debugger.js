var controller = {
	$this:null,
	parentController:null,
	control:null,
	controlId:guid(),
	esVentana:false,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;

		this.$this.find("#cmdRefrescar").button({icons:{secondary:"ui-icon-refresh"}})
		.click(function() {
			thisController.refresca();
		});
		this.$this.find("#cmdEjecutar").button({icons:{secondary:"ui-icon-play"}})
		.click(function() {
			thisController.ejecutar();
		});
		this.$this.find("#cmdAyuda").button({icons:{secondary:"ui-icon-circle-triangle-s"}})
		.click(function() {
			var menu = thisController.$this.find("#pop-menu");
			var html = thisController.ayuda();
			if (!html) {
				openDialog("common/w-error", {message:"No hay métodos en el contrato del control"});
				return;
			}
			menu.html(html);
			menu.menu("refresh");
			menu.show().position({
				my: "right top",
				at: "right bottom",
				of: this
			});
			$(document).one("click", function() {
				menu.hide();
			});
			return false; // prevent default
		});
		this.$this.find("#pop-menu").menu({
			select:function(e, ui) {
				var item = $(e.originalEvent.target);
				var pasteCode = item.data("paste-code");
				thisController.$this.find("#edComando").text("controller." + pasteCode);
			}
		});
	},
	inicializa:function(parentController, control) {
		var thisController = this;
		this.control = control;
		this.parentController = parentController;
		this.$this.find("#lblTitulo").text(control.path);
		var nombre = control.path.substring(control.path.lastIndexOf("/") + 1);
		this.esVentana = nombre.startsWith("w");
		if (this.esVentana) {
			this.$this.find("#cmdEjecutar").hide();
			this.$this.find("#cmdAyuda").hide();
		}
	},
	fetch:function() {
	},
	refresca:function() {
		var thisController = this;
		this.inicializaLogs();
		var control = this.parentController.fetch();
		var opciones = {};
		if (this.esVentana) {
			var j = this.$this.find("#edComando").val();
			if (!j) j = "{}";
			try {
				opciones = JSON.parse(j);
			} catch(error) {
				openDialog("common/w-error", {message:"Las opciones de inicio no se pueden interpretar como un objeto Json"});
				return;
			}
			this.$this.find("#edComando").val(JSON.stringify(opciones, null, 2));
		}
		httpInvoke("startDebugSession.zdev", {basePath:app.path, controlPath:control.path, control:control, opciones:opciones}, function() {
			registerZDBGListener("?", function() {
				registerZDBGListener(thisController.controlId, function(action, data) {
					thisController.procesaMensaje(action, data);
				});
				thisController.sendMessage("setId", thisController.controlId);
			});
			thisController.$this.find("#iframe").prop("src", "common/empty");
			thisController.$this.find("#iframe").prop("src", "test/z-test.html");
			
		});
	},
	sendMessage:function(action, data) {
		this.$this.find("#iframe")[0].contentWindow.postMessage({action:action, data:data}, "*");
	},
	procesaMensaje:function(action, data) {
		if (action == "log") {
			this.agregaLog("info", data);
		} else if (action == "evento") {
			var args = "";
			$.each(data.argumentos, function(i, a) {
				if (args.length) args += ", ";
				if (typeof a == "object") {
					a = JSON.parse(a);
				}
				args += a;
			});
			this.agregaLog("evento", data.nombre + (args.length?"(" + args + ")":""));
		}
	},
	ejecutar:function() {
		var cmd = this.$this.find("#edComando").val();
		this.sendMessage("ejecuta", cmd);
	},
	ayuda:function() {
		var contrato = this.parentController.getContrato();
		var html = "";
		contrato.metodos.forEach(m => {
			var code = m;
			if (!m.endsWith(")")) m += "()";
			html += "<li data-paste-code='" + m + "'>" + m + "</li>\n";
		});
		return html;
	},
	inicializaLogs:function() {
		this.$this.find("#logContainer").html("<table id='tablaLog' style='width:100%;' class='tabla-log'></table>");
	},
	agregaLog:function(tipo, log) {
		var html = "<tr><td style='width:20px;'><img src='img/16x16/" + tipo + ".png' /></td><td style='width:100%;'>" + log + "</td></tr>";
		this.$this.find("#tablaLog").append(html);
		this.$this.find("#logContainer").scrollTop(this.$this.find("#logContainer")[0].scrollHeight);
	}
}