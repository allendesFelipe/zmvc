var controller = {
	$this:null,
	parentController:null,
	control:null,
	thisId:guid(),
	dom:null,
	controles:null,
	editor:null,
	timer:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		this.$this.find("#arbolControles").prop("id", "arbolControles_" + this.thisId);
		var arbol = this.getArbol().jqGrid({
            datatype: "local",
            gridview: true,
            rowNum: 10000,
            sortname: "id",
            treeGrid: true,            
            treeGridModel: 'adjacency',
            treedatatype: "local",
            ExpandColumn: "nombre",
            ExpandColClick: true,            
            autowidth: true,
            hidegrid: false,
            shrinkToFit: false,
            colNames: ["id", "", "Control", "id", "tipo"],
            colModel: [
              { name: "id", index: "id", key: true, hidden: true },
              { name: "imgClase", index:"imgClase", width:20, align:"center"},
			  { name: "nombre", index: "nombre", width: 280 },
			  { name: "controlId", index: "controlId", hidden: true },
              { name: "tipo", index: "tipo", hidden: true }
            ],
            caption: false,
            onSelectRow:function() {
            	thisController.refrescaDetallesControl();            	
            }
        });
        attachJQGridToContainerPanel(this.$this.find("#arbolControlesContainer"), arbol, true);
        
		var tabs = this.$this.find("#tabs").tabs({
			activate:function(event, ui) {
				if (thisController.editor) thisController.editor.resize();
			}
		});
		attachElementsToTabs(tabs, [thisController.$this.find("#detControles"), thisController.$this.find("#detHTML")], [], true);
		
		this.$this.find("#detallesControl").zPager({
			pages:[
			    {code:"empty", url:"common/empty", initial:true},
			    {code:"properties", url:"dev/control/controles/properties"}
			],
			onReady:function() {
				thisController.inicializaPanelesDetalle();
			}
		});
		
		var cmdAplicar = this.$this.find("#cmdAplicar").button({icons:{secondary:"ui-icon-check"}}).click(function() {
			thisController.aplicar();
		});

		// Wizards
		this.$this.find("#pop-menu").menu({
			select:function(e, ui) {
				var item = $(e.originalEvent.target);
				var codigoWizard = item.data("wizard");
				if (codigoWizard) thisController.runWizard(codigoWizard);
				else if (item.prop("id") == "opcionEliminar") thisController.eliminar();
				else if (item.prop("id") == "opcionSubir") thisController.subir();
				else if (item.prop("id") == "opcionBajar") thisController.bajar();
			}
		});
		
		this.$this.find("#cmdOpciones").button({icons:{secondary:"ui-icon-circle-triangle-s"}})
		.click(function() {
			var menu = thisController.$this.find("#pop-menu");
			menu.menu("refresh");
			var n = thisController.getNodoSeleccionado();
			if (!n) return;
			menu.show().position({
				my: "right top",
				at: "right bottom",
				of: this
			});
			$(document).one("click", function() {
				menu.hide();
			});
			return false; // prevent default
		});

		this.$this.find("#edFiltro").keyup(function() {
        	thisController.callBuscaArchivo();
        });
		this.$this.find("#edFiltro").click(function() {
        	thisController.callBuscaArchivo();
        });
	},
	inicializaPanelesDetalle:function() {
		var thisController = this;
		var pager = this.$this.find("#detallesControl");
		pager.zPager("controller", "properties").inicializa(this);
		var editorId = "ed-html-" + this.thisId;
		this.$this.find("#contenedor-html").html("<div id='" + editorId + "'></div>");
		ace.require("ace/ext/language_tools");
		this.editor = ace.edit(editorId);
		this.editor.$blockScrolling = Infinity;
		this.$this.find("#contenedor-html").zLayout("doLayout");
		this.editor.setTheme("ace/theme/eclipse");
		this.editor.getSession().setMode("ace/mode/html");
		this.editor.setOptions({
	        enableBasicAutocompletion: true,
	        enableSnippets: true,
	        enableLiveAutocompletion: false
	    });
		this.editor.setShowPrintMargin(false);
		this.$this.find("#" + editorId).data("on-layout-resize", function() {
			thisController.editor.resize();				
		});
		if (this.control) {
			this.editor.setValue(this.control.vista);
			this.editor.clearSelection();
		}
	},
	getArbol:function() {
		return this.$this.find("#arbolControles_" + this.thisId);
	},
	inicializa:function(parentController, control) {
		var thisController = this;
		this.control = control;
		this.parentController = parentController;
		this.refresca();
	},
	markChanged:function() {
		this.parentController.markChanged();
	},
	fetch:function() {
		
	},	
	agregaNivelArbolControles:function(parent, level, nodo, arbol) {
		var thisController = this;
		var codigo = analizaTipoControl(nodo);
		var handler = getHandler(codigo);
		var nombreCustom = handler.describe(nodo);
		var nombre = nombreCustom?nombreCustom:handler.nombre;
		if (nodo.id) nombre += " [" + nodo.id + "]";
		var n = {
			id:"" + (arbol.length + 1),
			controlId:(nodo.id?nodo.id:""),
			nombre:nombre,
			tipo:codigo,
			imgClase:"<img src='img/16x16/" + handler.clase + ".png' />",
			nodo:nodo,
			parent:parent,
			level:level,
			isLeaf:true,
			expanded:level < 2,
			loaded:true
		};
		arbol.push(n);
		if (!handler.esUltimoNivel(nodo)) {
			$.each(nodo.children, function(i, h) {
				if (h.nodeType == 1) {
					n.isLeaf = false;
					thisController.agregaNivelArbolControles(n.id, level + 1, h, arbol);
				}
			});
		}
	},
	parseaDOM:function() {
		var thisController = this;
		this.controles = [];
		$.each(this.dom, function(i, nodo) {
			if (nodo.nodeType == 1) {
				thisController.agregaNivelArbolControles(null, 0, nodo, thisController.controles);
			}
		});
	},
	refresca:function(idToSelect, silent) {
		this.dom = $(this.control.vista);		
		this.refrescaControles(idToSelect, silent);
		if (this.editor) {
			this.editor.setValue(this.control.vista);
			this.editor.clearSelection();
		}
	},
	enableMenu:function(id, enabled) {
		var m = this.$this.find("#" + id);
		if (enabled) m.removeClass("ui-state-disabled");
		else m.addClass("ui-state-disabled");
	},
	refrescaControles:function(idToSelect, silent, controlIdToSelect) {
		var thisController = this;
		this.enableMenu("opcionAgregar", false);
		this.enableMenu("opcionEliminar", false);
		this.parseaDOM();
		var arbol = this.getArbol();
		this.parentController.cambioEstructura(this.controles);
		if (!idToSelect) {
			if (controlIdToSelect) {
				$.each(this.controles, function(i, c) {
					if ($(c.nodo).prop("id") == controlIdToSelect) {
						idToSelect = c.id;
						return false;
					}
				});
			} else {
				idToSelect = arbol.jqGrid("getGridParam", "selrow");
			}
		}
		arbol.jqGrid("clearGridData");
		arbol[0].addJSONData(this.controles);
		if (idToSelect) thisController.seleccionaNodo(idToSelect, silent);
	},
	getNodoSeleccionado:function() {
		var arbol = this.getArbol();
		var id = arbol.jqGrid("getGridParam", "selrow");
		if (!id) return null;
		return arbol.jqGrid("getRowData", id);
	},	
	seleccionaNodo:function(idNodo, silent) {
		var thisController = this;
		var arbol = this.getArbol();
		var nodo = arbol.jqGrid("getRowData", idNodo);
		var parent = arbol.jqGrid("getNodeParent", nodo);
		while(parent) {
			arbol.jqGrid("expandRow", parent);
			arbol.jqGrid("expandNode", parent);
			parent = arbol.jqGrid("getNodeParent", parent);
		}
		arbol.jqGrid("setSelection", idNodo, !silent);
		//this.expandeSubArbol(idNodo);
	},
	getControlAnterior:function(id) {
		var idx = id - 1;
		if (idx < 0) return null;
		var nivelInicial = this.controles[idx].level;
		idx--;
		while (idx >= 0) {
			var c = this.controles[idx];
			if (c.level == nivelInicial) return c;
			if (c.level < nivelInicial) return null;
			idx--;
		}
		return null;
	},
	getControlSiguiente:function(id) {
		var idx = id - 1;
		if (idx < 0) return null;
		var nivelInicial = this.controles[idx].level;
		idx++;
		while (idx < (this.controles.length - 1)) {
			var c = this.controles[idx];
			if (c.level == nivelInicial) return c;
			if (c.level < nivelInicial) return null;
			idx++;
		}
		return null;
	},
	refrescaDetallesControl:function() {
		var thisController = this;
		this.enableMenu("opcionAgregar", false);
		this.enableMenu("opcionEliminar", false);
		var n = this.getNodoSeleccionado();
		var pager = this.$this.find("#detallesControl");
		if (!n) {
			pager.zPager("page", "empty");
			return;
		}
		this.enableMenu("opcionEliminar", true);
		pager.zPager("currentPage", "properties");
		pager.zPager("controller").refresca(this.controles[parseInt(n.id) - 1]);
		var h = getHandler(n.tipo);
		if (!h.esUltimoNivel()) {
			this.enableMenu("opcionAgregar", true);
			var html = "<div>Agregar Control</div><ul>";
			var grupos = getPosiblesHijosDe(n.tipo);
			grupos.forEach((g) => {
				html += "<li><div>" + g.grupo + "</div><ul>";
				g.wizards.forEach((w) => {
					html += "<li data-wizard='" + w.codigo + "'><div data-wizard='" + w.codigo + "'>" + w.nombre + "</div></li>";
				});
				html += "</ul></li>";
			});
			html += "</ul>";			
			thisController.$this.find("#opcionAgregar").html(html);
		}
		this.enableMenu("opcionSubir", this.getControlAnterior(n.id)?true:false);
		this.enableMenu("opcionBajar", this.getControlSiguiente(n.id)?true:false);
	},
	cambioControl:function(nodo) {
		this.markChanged();
		this.refrescaControles();
		var html = this.dom[0].outerHTML;
		html = window.html_beautify(html);
		this.control.vista = html;
		this.editor.setValue(this.control.vista);
		this.editor.clearSelection();
	},
	reemplazaVista:function(html) {
		this.dom = $(html);		
		this.markChanged();
		this.refrescaControles();
		var html = this.dom[0].outerHTML;
		html = window.html_beautify(html);
		this.control.vista = html;
	},
	aplicar:function() {
		this.reemplazaVista(this.editor.getValue());
	},
	runWizard:function(codigo) {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var control = this.controles[parseInt(n.id) - 1];
		var w = getWizard(codigo);
		if (w.nombreVentana) {
			openDialog("dev/control/add-wizards/" + w.nombreVentana, {control:control, onOk:function(html) {
				html = $(html);
				var nuevoId = html.prop("id");
				$.each(html, function(i, c) {
					thisController.indentedAppend(control.nodo, c);				
				});
				//$(control.nodo).append(html);
				thisController.refrescaControles(null, false, nuevoId);
				thisController.control.vista = window.html_beautify(thisController.dom[0].outerHTML);
				thisController.editor.setValue(thisController.control.vista);
				thisController.editor.clearSelection();
				thisController.parentController.markChanged();
			}});
		} else {
			var html = $(w.getHTML());
			var nuevoId = html.prop("id");
			$.each(html, function(i, c) {
				thisController.indentedAppend(control.nodo, c);				
			});
			//$(control.nodo).append(html);
			this.refrescaControles(null, false, nuevoId);
			this.control.vista = window.html_beautify(this.dom[0].outerHTML);
			this.editor.setValue(this.control.vista);
			this.editor.clearSelection();
			this.parentController.markChanged();
		}
	},
	indentedAppend:function(parent,child) {
	    var indent = "",
	        elem = parent;

	    while (elem.parentNode && elem !== document.body) {
	        indent += "\t";
	        elem = elem.parentNode;
	    }

	    if (parent.hasChildNodes() && parent.lastChild.nodeType === 3 && /^\s*[\r\n]\s*$/.test(parent.lastChild.textContent)) {
	        parent.insertBefore(document.createTextNode("\n" + indent), parent.lastChild);
	        parent.insertBefore(child, parent.lastChild);
	    } else {
	        parent.appendChild(document.createTextNode("\n" + indent));
	        parent.appendChild(child);
	        parent.appendChild(document.createTextNode("\n" + indent.slice(0,-2)));
	    }
	},
	eliminar:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var control = this.controles[parseInt(n.id) - 1];
		openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar el nodo '" + n.nombre + "'?", onOk:function() {
			$(control.nodo).remove();
			thisController.refrescaControles();
			thisController.control.vista = thisController.dom[0].outerHTML;
			thisController.editor.setValue(thisController.control.vista);
			thisController.editor.clearSelection();
			thisController.parentController.markChanged();
		}});
	},
	subir:function() {
		var thisController = this;
  		var n = this.getNodoSeleccionado();
		if (!n) return;
		var control = this.controles[parseInt(n.id) - 1];
		var controlAnterior = this.getControlAnterior(n.id);
		$(control.nodo).remove();
		controlAnterior.nodo.parentNode.insertBefore(control.nodo, controlAnterior.nodo);
		thisController.refrescaControles(controlAnterior.id);
		thisController.control.vista = thisController.dom[0].outerHTML;
		thisController.editor.setValue(thisController.control.vista);
		thisController.editor.clearSelection();
		thisController.parentController.markChanged();
	},
	bajar:function() {
		var thisController = this;
  		var n = this.getNodoSeleccionado();
		if (!n) return;
		var control = this.controles[parseInt(n.id) - 1];
		var controlSiguiente = this.getControlSiguiente(n.id);
		$(control.nodo).remove();
		controlSiguiente.nodo.parentNode.insertBefore(control.nodo, controlSiguiente.nodo.nextSibling);
		thisController.refrescaControles(controlSiguiente.id);
		thisController.control.vista = thisController.dom[0].outerHTML;
		thisController.editor.setValue(thisController.control.vista);
		thisController.editor.clearSelection();
		thisController.parentController.markChanged();
	},
	callBuscaArchivo:function(delay) {
		var thisController = this;
		if (!delay) delay = 300;
		if (this.timer) {
			clearTimeout(this.timer);
			this.timer = null;
		}
		var f = this.$this.find("#edFiltro").val().trim();
		if (!f) return;
		this.timer = setTimeout(function() {
			thisController.timer = null;
			thisController.buscaArchivo();
		}, delay);
	},
	buscaArchivo:function() {
		var arbol = this.controles;
		var f = this.$this.find("#edFiltro").val().trim().toUpperCase();
		if (!f) return;
		var i=1;
		var n = this.getNodoSeleccionado();
		if (n) i = parseInt(n.id) + 1;
		if (i > arbol.length) i=1;
		var nBusquedas = 0;
		do {
			n = arbol[i - 1];
			var idControl = n.controlId;
			if (idControl) {
				if (idControl.toUpperCase().indexOf(f) >= 0) {
					this.seleccionaNodo(n.id);
					return;
				}
			}
			i++;
			if (i > arbol.length) i=1;			
		} while (++nBusquedas < arbol.length);
	}
}