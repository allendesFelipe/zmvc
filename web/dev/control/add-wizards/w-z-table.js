var controller = {
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:"Z-Tabla",
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:240,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		w.keyup(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				thisController.onOk(w, initialData);
				return false;
			}
		});
		
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	init:function(w, initialData) {		
	},
	fetch:function(w, initialData) {
		var id = w.find("#edId").val();
		if (!id) {
			openDialog("common/w-error", {message:"Debe ingresar el Id de la nueva tabla"});
			return null;
		}
		var hayDetalles = w.find("#edDetalle").prop("checked")?true:false;
		var hayPaginador = w.find("#edPaginador").prop("checked")?true:false;
		var dataDetalles = hayDetalles?' data-z-page-details="detalle-' + id + '"':'';
		var dataPaginador = hayPaginador?' data-z-pager="paginador-' + id + '"':'';
		var html = "";
		html += '<div id="contenedor-' + id + '" class="row mt-1">\n';
		html += '\t\t<div class="col">\n';
		html += '\t\t\t<table id="' + id + '" class="table table-sm table-hover table-striped table-bordered"' + dataDetalles + ' ' + dataPaginador + ' data-z-selectable-row-class="table-primary" data-z-edit="true">\n';
		html += '\t\t\t\t<thead>\n';
		html += '\t\t\t\t\t<tr>\n';
		html += '\t\t\t\t\t\t<th data-z-col="codigo" data-z-cell-class="text-center td-tabla-custom" class="th-tabla-custom text-center">Código</th>\n';
		html += '\t\t\t\t\t\t<th data-z-col="nombre" data-z-cell-class="text-center td-tabla-custom" class="th-tabla-custom text-center">Nombre</th>\n';
		html += '\t\t\t\t\t</tr>\n';
		html += '\t\t\t\t</thead>\n';
		html += '\t\t\t\t<tbody></tbody>\n';
		html += '\t\t\t</table>\n';
		html += '\t\t</div>\n';
		html += '\t</div>\n';
		if (hayDetalles || hayPaginador) {
			html += '\t<hr />\n';
			if (hayDetalles) {
				html += '\t<div class="row justify-content-center"><small id="detalle-' + id + '" class="form-text text-muted"></small></div>\n';
			}
			if (hayPaginador) {
				html += '\t<div class="row justify-content-center">\n';
				html += '\t\t<ul id="paginador-' + id + '" class="pagination pagination-sm pagination-centered" data-z-pagination="local" data-z-n-rows="10" data-z-n-pages="10"></ul>\n';
				html += '\t</div>\n';
			}
		}
		return html;
	},
	onOk:function(w, initialData) {
		var html = this.fetch(w, initialData);
		if (html) {
			w.dialog("close");
			if (initialData.onOk) initialData.onOk(html);
		}
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}