var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#cmdAgregarPagina").button({text:false, icons:{primary:"ui-icon-circle-plus"}})
		.click(function() {
			thisController.agregarPagina();
		});
		this.$this.find("#cmdEliminarPagina").button({text:false, icons:{primary:"ui-icon-trash"}})
		.click(function() {
			thisController.eliminarPagina();
		});
		this.$this.find("#edPagina").change(function() {
			thisController.refrescaPagina();
		});
		this.$this.find("#cmdBuscarControl").button({text:false, icons:{primary:"ui-icon-search"}})
		.click(function() {
			thisController.buscarControl();
		});
		this.$this.find("#edCode").change(function() {
			var p = thisController.getPage();
			if (!p) return;
			p.code = $(this).val();
			thisController.setPage(p);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edURL").change(function() {
			var p = thisController.getPage();
			if (!p) return;
			p.url = $(this).val();
			thisController.setPage(p);
			if (p.initial) $(thisController.nodo.nodo).attr("data-z-autoload", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edInitial").change(function() {
			var initial = $(this).prop("checked")?true:false;
			if (!initial) {
				$(this).prop("checked", true);
				return;
			}
			var idx = parseInt(thisController.$this.find("#edPagina").val());
			if (isNaN(idx)) return;
			var pages = thisController.getPages();
			$.each(pages, function(i, p) {
				p.initial = (i == idx);
			});
			thisController.setPages(pages);
			$(thisController.nodo.nodo).attr("data-z-autoload", thisController.$this.find("#edURL").val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		this.refrescaPaginas();
	},
	refrescaPaginas:function() {
		this.$this.find("#editorPagina").hide();
		enableButton(this.$this.find("#cmdEliminarPagina"), false);
		var pages = this.getPages();
		var html = "";
		$.each(pages, function(i, p) {
			html += "<option value='" + i + "'>[" + p.code + "]" + (p.initial?"[initial]":"") + " " + p.url + "</option>";
		});
		this.$this.find("#edPagina").html(html);
		if (!isNaN(app.idxToSelect)) {
			this.$this.find("#edPagina").val(app.idxToSelect);
			app.idxToSelect = null;
			this.refrescaPagina();
		}
	},
	refrescaPagina:function() {
		var page = this.getPage();
		if (!page) return;
		app.idxToSelect = parseInt(this.$this.find("#edPagina").val());
		enableButton(this.$this.find("#cmdEliminarPagina"), true);
		this.$this.find("#editorPagina").show();
		this.$this.find("#edCode").val(page.code);
		this.$this.find("#edURL").val(page.url);
		this.$this.find("#edInitial").prop("checked", page.initial?true:false);
	},	
	getControl:function() {
		return this.parentController.control;
	},
	getPages:function() {
		var n = $(this.nodo.nodo);
		var pages = [];
		try {
			pages = JSON.parse($(n).attr("data-z-ref-pages"));
		} catch(error) {
			var initialURL = $(n).attr("data-z-autoload");
			pages = [{code:"inicial", url:initialURL, initial:true}];
		}
		if (!pages.length) {
			var initialURL = $(n).attr("data-z-autoload");
			pages = [{code:"inicial", url:initialURL, initial:true}];
		}
		return pages;
	},
	setPages:function(pages) {
		var n = $(this.nodo.nodo);
		n.attr("data-z-ref-pages", JSON.stringify(pages));
	},
	getPage:function() {
		var idx = parseInt(this.$this.find("#edPagina").val());
		if (isNaN(idx)) return null;
		return this.getPages()[idx];
	},
	setPage:function(page) {
		var idx = parseInt(this.$this.find("#edPagina").val());
		if (isNaN(idx)) return null;
		var pages = this.getPages();
		pages[idx] = page;
		this.setPages(pages);
	},
	hayPaginaInicial:function() {
		var hay = false;
		var pages = this.getPages();
		$.each(pages, function(i, p) {
			if (p.initial) {
				hay = true;
				return false;
			}
		});
		return hay;
	},
	agregarPagina:function() {
		var thisController = this;		
		openDialog("dev/w-busca-control", {onOk:function(control) {
			var code = control.path.substring(control.path.lastIndexOf("/") + 1);
			var url = control.path.substring(app.path.length + 1);
			var page = {code:code, url:url, initial:!thisController.hayPaginaInicial()};
			var pages = thisController.getPages();
			pages.push(page);
			thisController.setPages(pages);
			app.idxToSelect = pages.length;
			thisController.parentController.cambioControl(thisController.nodo);
		}});
	},
	eliminarPagina:function() {
		var thisController = this;
		var idx = parseInt(this.$this.find("#edPagina").val());
		if (isNaN(idx)) return null;		
		openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar la página seleccionada?", onOk:function() {
			var pages = thisController.getPages();
			pages.splice(idx, 1);
			thisController.setPages(pages);
			thisController.parentController.cambioControl(thisController.nodo);
		}});
	},
	buscarControl:function() {
		var thisController = this;
		var page = this.getPage();
		if (!page) return;
		openDialog("dev/w-busca-control", {onOk:function(control) {
			var url = control.path.substring(app.path.length + 1);
			page.url = url;
			thisController.setPage(page);
			thisController.refrescaPaginas();
			thisController.parentController.cambioControl(thisController.nodo);
		}});
	}
}