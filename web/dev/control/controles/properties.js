var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	paginas:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#loader").zLoader({
			pages:{code:"empty", url:"common/empty", initial:true}
		});
		this.$this.find("#edPagina").change(function() {
			thisController.cambiaPagina();
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {		
		var thisController = this;
		this.nodo = nodo;
		var handler = getHandler(nodo.tipo);
		this.paginas = handler.getPropPages(nodo.nodo);
		var html = "";
		$.each(this.paginas, function(i, p) {
			html += "<option value='" + p.codigo + "'>" + p.nombre + "</option>";
		});
		var paginaAnterior = this.$this.find("#edPagina").val();
		this.$this.find("#edPagina").html(html);
		this.$this.find("#loader").zLoader("dynamicLoad", "common/empty", function() {
			if (thisController.paginas.length) {
				if (!paginaAnterior) paginaAnterior = thisController.paginas[0].codigo;
				thisController.$this.find("#edPagina").val(paginaAnterior);
				if (!thisController.$this.find("#edPagina").val()) {
					thisController.$this.find("#edPagina").val(thisController.paginas[0].codigo);
				}
				thisController.cambiaPagina();
			}
		});
	},
	cambiaPagina:function() {
		var thisController = this;
		var codigo = this.$this.find("#edPagina").val();
		this.$this.find("#loader").zLoader("dynamicLoad", "dev/control/controles/pag-" + codigo, function() {
			var controller = thisController.$this.find("#loader").zLoader("getCurrentController");
			controller.inicializa(thisController.parentController);
			controller.refresca(thisController.nodo);
		});
	}
}