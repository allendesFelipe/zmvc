var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edJustifyContent").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("justify-content-start justify-content-center justify-content-end");
			var align = $(this).val();
			if (align) n.addClass(align);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edAlignItems").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("align-items-start align-items-center align-items-end");
			var align = $(this).val();
			if (align) n.addClass(align);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edForm").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("row form-row");
			var c = $(this).prop("checked")?"form-row":"row";
			n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		
		var edJustifyContent = this.$this.find("#edJustifyContent");
		if (n.hasClass("justify-content-start")) edJustifyContent.val("justify-content-start");
		else if (n.hasClass("justify-content-center")) edJustifyContent.val("justify-content-center");
		else if (n.hasClass("justify-content-end")) edJustifyContent.val("justify-content-end");
		else edJustifyContent.val("");
		
		var edAlignItems = this.$this.find("#edAlignItems");
		if (n.hasClass("align-items-start")) edAlignItems.val("align-items-start");
		else if (n.hasClass("align-items-center")) edAlignItems.val("align-items-center");
		else if (n.hasClass("align-items-end")) edAlignItems.val("align-items-end");
		else edAlignItems.val("");
		
		this.$this.find("#edForm").prop("checked", n.hasClass("form-row")) 
	}
}