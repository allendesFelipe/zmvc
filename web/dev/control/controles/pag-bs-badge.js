var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edEstilo").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("badge badge-primary badge-secondary badge-success badge-danger badge-warning badge-info badge-light badge-dark");
			var style = $(this).val();
			if (style) n.addClass("badge " + style);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		var h = getHandler(nodo.tipo);
		var classes = h.getClasses(n);
		var estilo = "";
		$.each(classes, function(i, c) {
			if (c.startsWith("badge-")) {
				estilo = c;
				return false;
			}
		});
		this.$this.find("#edEstilo").val(estilo);
	}
}