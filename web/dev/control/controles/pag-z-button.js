var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edEstilo").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("btn btn-primary btn-secondary btn-success btn-danger btn-warning btn-info btn-light btn-dark btn-link");
			n.removeClass("btn-outline-primary btn-outline-secondary btn-outline-success btn-outline-danger btn-outline-warning btn-outline-info btn--outlinelight btn-outline-dark");
			var style = $(this).val();
			if (style) n.addClass("btn " + style);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edEtiqueta").change(function() {
			var etiqueta = $(this).val();
			var n = $(thisController.nodo.nodo);
			var child = null;
			var candidato = null;
			$.each(n[0].childNodes, function(i, c) {
				if (c.nodeType == 3) {
					if (c.nodeValue.trim()) {
						child = c;
						return false;						
					} else {
						candidato = c;
					}
				}
			});
			if (!child) child = candidato;
			if (child) {
				child.nodeValue = etiqueta;
			} else {
				n.text(etiqueta);
			}
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edSize").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("btn-lg btn-sm");
			var size = $(this).val();
			if (size) n.addClass(size);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		var h = getHandler(nodo.tipo);
		var classes = h.getClasses(n);
		var estilo = "";
		$.each(classes, function(i, c) {
			if (c.startsWith("btn-") && c != "btn-lg" && c != "btn-sm") {
				estilo = c;
				return false;
			}
		});
		this.$this.find("#edEstilo").val(estilo);
		var etiqueta = "";
		$.each(n[0].childNodes, function(i, c) {
			if (c.nodeType == 3 && c.nodeValue.trim()) {
				etiqueta = c.nodeValue;
				return false;
			}
		});
		this.$this.find("#edEtiqueta").val(etiqueta);
		var size = "";
		if (n.hasClass("btn-lg")) size="btn-lg";
		else if (n.hasClass("btn-sm")) size="btn-sm";
		this.$this.find("#edSize").val(size);
	}
}