var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	editor:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;

		var cmdAplicar = this.$this.find("#cmdAplicar").button({icons:{secondary:"ui-icon-check"}}).click(function() {
			thisController.aplicar();
		});

		var editorId = "ed-html-" + guid();
		this.$this.find("#contenedor-editor").html("<div id='" + editorId + "'></div>");
		ace.require("ace/ext/language_tools");
		this.editor = ace.edit(editorId);
		this.editor.$blockScrolling = Infinity;
		this.$this.find("#contenedor-editor").zLayout("doLayout");
		this.editor.setTheme("ace/theme/eclipse");
		this.editor.getSession().setMode("ace/mode/html");
		this.editor.setOptions({
	        enableBasicAutocompletion: true,
	        enableSnippets: true,
	        enableLiveAutocompletion: false
	    });
		this.editor.setShowPrintMargin(false);
		this.$this.find("#" + editorId).data("on-layout-resize", function() {
			thisController.editor.resize();				
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		var titulo = nodo.nodo.tagName;
		if (nodo.nodo.id) titulo += " [" + nodo.nodo.id + "]";
		this.$this.find("#lblTitulo").text(titulo);
		this.nodo = nodo;
		this.editor.setValue(window.html_beautify(nodo.nodo.outerHTML));
		this.editor.clearSelection();
		
	},
	aplicar:function() {
		try {
			var n = $(this.editor.getValue())[0];
			if (n.nodeType != 1) {
				return;
			}
			var parentNode = this.nodo.nodo.parentNode;	
			if (!parentNode || parentNode.nodeName == "#document-fragment") {
				this.parentController.reemplazaVista(n.outerHTML);
			} else {
				this.nodo.nodo.outerHTML = n.outerHTML;
				this.parentController.cambioControl(this.nodo);
			}			
		} catch(error) {
			console.log(error);
		}
	}
}