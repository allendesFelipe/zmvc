var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#cmdAgregarColumna").button({text:false, icons:{primary:"ui-icon-circle-plus"}})
		.click(function() {
			thisController.agregarColumna();
		});
		this.$this.find("#cmdEliminarColumna").button({text:false, icons:{primary:"ui-icon-trash"}})
		.click(function() {
			thisController.eliminarColumna();
		});
		this.$this.find("#edColumna").change(function() {
			thisController.refrescaColumna();
		});
		this.$this.find("#edCampo").change(function() {
			thisController.getColumna().attr("data-z-col", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edTitulo").change(function() {
			var t = $(this).val();
			if (!t) return;
			thisController.setTituloColumna(t);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edJustificaTitulo").change(function() {
			var n = thisController.getColumna();
			n.removeClass("text-center text-left text-right");
			if ($(this).val()) n.addClass($(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edClasesCustomTitulo").change(function() {
			var classes = $(this).val();
			var justClass = thisController.$this.find("#edJustificaTitulo").val();
			if (justClass) {
				if (classes) classes += " ";
				classes += justClass;
			}
			thisController.getColumna().attr("class", classes);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edAnchoTitulo").change(function() {
			thisController.getColumna().css("width", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edJustificaContenido").change(function() {
			var n = thisController.getColumna();
			var zClasses = thisController.$this.find("#edClasesCustomContenido").val();
			if ($(this).val()) {
				if (zClasses) zClasses += " ";
				zClasses += $(this).val();
			} 
			n.attr("data-z-cell-class", zClasses);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edClasesCustomContenido").change(function() {
			var classes = $(this).val();
			var justClass = thisController.$this.find("#edJustificaContenido").val();
			if (justClass) {
				if (classes) classes += " ";
				classes += justClass;
			}
			thisController.getColumna().attr("data-z-cell-class", classes);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	getHeaderRow:function() {
		var n = $(this.nodo.nodo);
		var thead = null;
		$.each(n.children(), function(i, c) {
			if (c.tagName == "THEAD") {
				thead = $(c);
				return false;
			}
		});
		if (!thead) {
			console.log("head no se encontró, agregando");
			thead = $(n.prepend("<thead></thead>"));
		}
		var headerRow = thead.find("tr");
		if (!headerRow.length) {
			console.log("header row no se encontró, agregando");
			headerRow = $(thead.append("<tr></tr>"));
		}
		return headerRow;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var tr = this.getHeaderRow();
		var html = "";
		$.each(tr.children(), function(i, th) {
			html += "<option value='" + i + "'>" + ($(th).attr("data-z-col")?$(th).attr("data-z-col"):"?") + "</option>";
		});
		this.$this.find("#edColumna").html(html);
		if (!isNaN(app.idxToSelect)) {
			this.$this.find("#edColumna").val(app.idxToSelect);
			app.idxToSelect = null;
		}
		this.refrescaColumna();		
	},
	getColumna:function() {
		var idx = parseInt(this.$this.find("#edColumna").val());
		if (isNaN(idx)) return null;
		var tr = this.getHeaderRow();
		return $(tr.children()[idx]);
	},
	getTituloColumna:function() {
		var c = this.getColumna();
		if (!c) return;
		var titulo = null;
		$.each(c[0].childNodes, function(i, n) {
			if (n.nodeType == 3 && n.nodeValue) {
				titulo = n.nodeValue;
				return false;
			}
		});
		return titulo;
	},
	setTituloColumna:function(titulo) {
		var c = this.getColumna();
		if (!c) return;
		$.each(c[0].childNodes, function(i, n) {
			if (n.nodeType == 3 && n.nodeValue) {
				n.nodeValue = titulo;
				return false;
			}
		});
	},
	refrescaColumna:function() {
		var thisController = this;
		var c = this.getColumna();
		if (!c) {
			this.$this.find("#editorColumna").hide();
			return;
		}
		app.idxToSelect = parseInt(this.$this.find("#edColumna").val());
		this.$this.find("#editorColumna").show();
		this.$this.find("#edCampo").val(c.attr("data-z-col"));
		this.$this.find("#edTitulo").val(this.getTituloColumna());

		// Titulo
		var classAttr = c.attr("class");
		if (!classAttr) classAttr = [];
		var classes = classAttr.split(/\s+/);

		this.$this.find("#edJustificaTitulo").val("");
		var custom = "";
		$.each(classes, function(i, c) {
			if (c.startsWith("text-")) {
				thisController.$this.find("#edJustificaTitulo").val(c);
			} else {
				if (custom) custom += " ";
				custom += c;
			}
		});
		thisController.$this.find("#edClasesCustomTitulo").val(custom);
		thisController.$this.find("#edAnchoTitulo").val(c.css("width") == "0px"?"":c.css("width"));
		
		// Contenido
		var classAttr = c.attr("data-z-cell-class");
		if (!classAttr) classAttr = [];
		var classes = classAttr.split(/\s+/);

		this.$this.find("#edJustificaContenido").val("");
		var custom = "";
		$.each(classes, function(i, c) {
			if (c.startsWith("text-")) {
				thisController.$this.find("#edJustificaContenido").val(c);
			} else {
				if (custom) custom += " ";
				custom += c;
			}
		});
		thisController.$this.find("#edClasesCustomContenido").val(custom);
	},
	agregarColumna:function() {
		var tr = this.getHeaderRow();
		tr.append('<th data-z-col="nombreCampo" data-z-cell-class="td-tabla-custom text-left" class="text-center th-tabla-custom" >Titulo</th>');
		this.refresca(this.nodo);
	},
	eliminarColumna:function() {
		
	}
}