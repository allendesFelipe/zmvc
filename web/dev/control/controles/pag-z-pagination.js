var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	estilos:["pagination-sm", "pagination-centered"],
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPagination").change(function() {
			var n = $(thisController.nodo.nodo);
			n.attr("data-z-pagination", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edFilas").change(function() {
			var n = $(thisController.nodo.nodo);
			var numero = parseInt($(this).val());
			if (isNaN(numero)) return;
			n.attr("data-z-n-rows", numero);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPaginas").change(function() {
			var n = $(thisController.nodo.nodo);
			var numero = parseInt($(this).val());
			if (isNaN(numero)) return;
			n.attr("data-z-n-pages", numero);
			thisController.parentController.cambioControl(thisController.nodo);
		});

		this.$this.find("#edSelectableRowClass").change(function() {
			var n = $(thisController.nodo.nodo);		
			var clase = $(this).val();
			if (!clase) n.removeAttr("data-z-selectable-row-class");
			else n.attr("data-z-selectable-row-class", clase);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		this.$this.find("#edPagination").val(n.attr("data-z-pagination"));
		this.$this.find("#edFilas").val(n.attr("data-z-n-rows"));
		this.$this.find("#edPaginas").val(n.attr("data-z-n-pages"));
		this.refrescaEstilos(nodo);
	},
	refrescaEstilos:function(nodo) {
		var thisController = this;
		var n = $(nodo.nodo);
		var html = "";
		this.estilos.forEach((e) => {
			html += "<li><input class='cambiador-estilo' data-estilo='" + e + "' type='checkbox' " + (n.hasClass(e)?" checked='checked'":"") + " />" + e + "</li>";			
		});
		this.$this.find("#edEstilos").html(html);
		this.$this.find("#edEstilos").find(".cambiador-estilo").change(function() {
			var aplica = $(this).prop("checked");
			var estilo = $(this).data("estilo");
			var n = $(thisController.nodo.nodo);
			if (aplica) n.addClass(estilo);
			else n.removeClass(estilo);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	}
}