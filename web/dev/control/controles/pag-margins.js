var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edML").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("ml-0 ml-1 ml-2 ml-3 ml-4 ml-5 ml-auto");
			var m = $(this).val();
			if (m) n.addClass("ml-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edMR").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("mr-0 mr-1 mr-2 mr-3 mr-4 mr-5 mr-auto");
			var m = $(this).val();
			if (m) n.addClass("mr-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edMT").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("mt-0 mt-1 mt-2 mt-3 mt-4 mt-5 mt-auto");
			var m = $(this).val();
			if (m) n.addClass("mt-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edMB").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("mb-0 mb-1 mb-2 mb-3 mb-4 mb-5 mb-auto");
			var m = $(this).val();
			if (m) n.addClass("mb-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		var thisController = this;
		this.nodo = nodo;
		var n = $(nodo.nodo);
		var h = getHandler(nodo.tipo);
		var classes = h.getClasses(nodo.nodo);
		$.each(classes, function(i, c) {
			if (c.startsWith("ml-")) thisController.$this.find("#edML").val(c.substring(3));
			if (c.startsWith("mr-")) thisController.$this.find("#edMR").val(c.substring(3));
			if (c.startsWith("mt-")) thisController.$this.find("#edMT").val(c.substring(3));
			if (c.startsWith("mb-")) thisController.$this.find("#edMB").val(c.substring(3));
		});
	}
}