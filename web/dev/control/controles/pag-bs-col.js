var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		this.$this.find("#edCol").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("col-", ["col-sm-", "col-md-", "col-lg", "col-xl"]);
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edOffset").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("offset-", ["offset-sm-", "offset-md-", "offset-lg", "offset-xl"]);
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edColSM").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("col-sm-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edOffsetSM").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("offset-sm-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});

		this.$this.find("#edColMD").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("col-md-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edOffsetMD").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("offset-md-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});

		this.$this.find("#edColLG").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("col-lg-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edOffsetLG").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("offset-lg-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});

		this.$this.find("#edColXL").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("col-xl-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edOffsetXL").change(function() {
			var c = $(this).val();
			thisController.eliminaClases("offset-xl-");
			if (c) $(thisController.nodo.nodo).addClass(c);
			thisController.corrigeColAuto();
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	eliminaClases:function(prefix, excluir) {
		if (!excluir) excluir = [];
		var classAttr = $(this.nodo.nodo).attr("class");
		var classes = classAttr?classAttr.split(/\s+/):[];
		var classesToRemove = [];
		classes.forEach(c => {
			var ex = false;
			excluir.forEach(cx => {
				if (c.startsWith(cx)) {
					ex = true;
					return false;
				}
			});
			if (!ex && c.startsWith(prefix)) classesToRemove.push(c);
		});
		$(this.nodo.nodo).removeClass(classesToRemove.join(" "));
	},
	corrigeColAuto:function() {
		var classAttr = $(this.nodo.nodo).attr("class");
		var classes = classAttr?classAttr.split(/\s+/):[];
		var tieneCol = false;
		var tieneOtro = false;
		classes.forEach(c => {
			if (c == "col") tieneCol = true;
			if (c.startsWith("col-") || c.startsWith("offset-")) tieneOtro = true;
		});
		if (!tieneOtro) {
			if (!tieneCol) $(this.nodo.nodo).addClass("col");
		} else {
			if (tieneCol) $(this.nodo.nodo).removeClass("col");
		}
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		var classAttr = n.attr("class");
		var classes = classAttr?classAttr.split(/\s+/):[];
		var col = "", offset = "";
		var colSM = "", offsetSM = "";
		var colMD = "", offsetMD = "";
		var colLG = "", offsetLG = "";
		var colXL = "", offsetXL = "";
		classes.forEach(c => {
			if (c.startsWith("col-")) {
				if (c.startsWith("col-sm-")) colSM = c;
				else if (c.startsWith("col-md-")) colMD = c;
				else if (c.startsWith("col-lg-")) colLG = c;
				else if (c.startsWith("col-xl-")) colXL = c;
				else col = c;
			}
			if (c.startsWith("offset")) {
				if (c.startsWith("offset-sm-")) offsetSM = c;
				else if (c.startsWith("offset-md-")) offsetMD = c;
				else if (c.startsWith("offset-lg-")) offsetLG = c;
				else if (c.startsWith("offset-xl-")) offsetXL = c;
				else offset = c;
			}
		});
		this.$this.find("#edCol").val(col);
		this.$this.find("#edOffset").val(offset);
		this.$this.find("#edColSM").val(colSM);
		this.$this.find("#edOffsetSM").val(offsetSM);
		this.$this.find("#edColMD").val(colMD);
		this.$this.find("#edOffsetMD").val(offsetMD);
		this.$this.find("#edColLG").val(colLG);
		this.$this.find("#edOffsetLG").val(offsetLG);
		this.$this.find("#edColXL").val(colXL);
		this.$this.find("#edOffsetXL").val(offsetXL);
	}
}