var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	claseIcono:"",
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#cmdBuscar").button({icons:{primary:"ui-icon-search"}, text:false})
		.click(function() {
			thisController.buscarIcono();
		});
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edSize").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-xs fa-sm fa-lg fa-2x fa-3x fa-4x fa-5x fa-6x fa-7x fa-8x fa-9x fa-10x");
			var c = $(this).val();
			if (c) n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edBorder").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-border");
			var c = $(this).prop("checked");
			if (c) n.addClass("fa-border");
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edUL").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-ul");
			var c = $(this).prop("checked");
			if (c) n.addClass("fa-ul");
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edLI").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-li");
			var c = $(this).prop("checked");
			if (c) n.addClass("fa-li");
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPull").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-pull-left fa-pull-right");
			var c = $(this).val();
			if (c) n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edRotate").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-rotate-90 fa-rotate-180 fa-rotate-270");
			var c = $(this).val();
			if (c) n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edFlip").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-flip-horizontal fa-flip-vertical");
			var c = $(this).val();
			if (c) n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edAnimar").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-spin fa-pulse");
			var c = $(this).val();
			if (c) n.addClass(c);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edFW").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa-fw");
			var c = $(this).prop("checked");
			if (c) n.addClass("fa-fw");
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edIcono").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass(thisController.claseIcono);
			var icono = $(this).val();
			if (!icono.startsWith("fa-")) icono = "fa-" + icono;
			n.addClass(icono);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edEstilo").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("fa fab fas far fal");
			n.addClass($(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		var classAttr = n.attr("class");
		var classes = classAttr?classAttr.split(/\s+/):[];
		var estilo = "";
		var size="";
		var border = false;
		var ul = false;
		var li = false;
		var rotate = "";
		var pull = "";
		var animar = "";
		var flip = "";
		var fw = false;
		var icono ="";
		classes.forEach(c => {
			if (c == "fa" || c == "fas") estilo = "fas";
			else if (c == "fab") estilo = "fab";
			else if (c == "far") estilo = "far";
			else if (c == "fal") estilo = "fal";
			if (c.startsWith("fa-")) {
				if (["fa-xs", "fa-sm", "fa-lg", "fa-2x", "fa-3x", "fa-4x", "fa-5x", "fa-6x", "fa-7x", "fa-8x", "fa-9x", "fa-10x"].indexOf(c) >= 0) size = c;
				else if (c == "fa-border") border = true;
				else if (c == "fa-ul") ul = true;
				else if (c == "fa-li") li = true;
				else if (c.startsWith("fa-rotate-")) rotate = c;
				else if (c.startsWith("fa-flip-")) flip = c;
				else if (c.startsWith("fa-pull-")) pull = c;
				else if (["fa-spin", "fa-pulse"].indexOf(c) >= 0) animar = c;
				else if (c == "fa-fw") fw = true;
				else icono = c;
			}
		});
		
		this.claseIcono = icono;
		 
		this.$this.find("#edId").val(n.prop("id"));
		this.$this.find("#edSize").val(size);
		this.$this.find("#edBorder").prop("checked", border);
		this.$this.find("#edUL").prop("checked", ul);
		this.$this.find("#edLI").prop("checked", li);
		this.$this.find("#edPull").val(pull);
		this.$this.find("#edRotate").val(rotate);
		this.$this.find("#edAnimar").val(animar);
		this.$this.find("#edFlip").val(flip);
		this.$this.find("#edFW").prop("checked", fw);
		this.$this.find("#edEstilo").val(estilo);
		this.$this.find("#edIcono").val(icono);
		
		this.$this.find("#preview").html(n[0].outerHTML);
	},
	buscarIcono:function() {
		console.log("buscar");
		var loc = "https://fontawesome.com/icons?d=gallery&m=free";
		var estilo = this.$this.find("#edEstilo").val();
		if (estilo == "fab") loc += "&s=brands";
		else if (estilo == "fas") loc += "&s=solid";
		else if (estilo == "far") loc += "&s=regular";
		else if (estilo == "fal") loc += "&s=light";
		window.open(loc, "_blank");
	}
}