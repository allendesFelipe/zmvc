var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edFormControl").change(function() {
			var n = $(thisController.nodo.nodo);
			var isFormControl = $(this).prop("checked");
			if (isFormControl) {
				n.addClass("form-control");
			} else {
				n.removeClass("form-control form-control-sm form-control-lg");
			}
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edSize").change(function() {
			var n = $(thisController.nodo.nodo);
			var isFormControl = $(this).prop("checked");
			n.removeClass("form-control-sm form-control-lg");
			var s = $(this).val();
			if (s) n.addClass(s);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edFormControl").prop("checked", n.hasClass("form-control"));
		if (n.hasClass("form-control")) {
			this.$this.find("#edSize").show();
			if (n.hasClass("form-control-sm")) this.$this.find("#edSize").val("form-control-sm");
			else if (n.hasClass("form-control-lg")) this.$this.find("#edSize").val("form-control-lg");
			else this.$this.find("#edSize").val("");
		} else {
			this.$this.find("#edSize").hide();
		}
	}
}