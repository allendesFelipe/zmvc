var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	/* Referencia de Grupos: https://bootstrapcreative.com/resources/bootstrap-4-css-classes-index/ */
	grupos:[
		{
			nombre:"align-*",
			help:"A set of utility classes that are equivelant to writing the css property vertical-align:middle; You can use this on inline and table cell elements. .align-(baseline, top, middle, bottom)",
			link:"https://getbootstrap.com/docs/4.0/utilities/vertical-align/",
			classes:["align-baseline", "align-top", "align-middle", "align-bottom"]
		},
		{
			nombre:"align-content-*",
			help:"Added to the parent flexbox container to determing how the elements are aligned horizontally. .align-content-(start (browser default), end, center, between, around, or stretch)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#align-content",
			classes:["align-content-start", "align-content-end", "align-content-center", "align-content-between", "align-content-around", "align-content-stretch"]
		},
		{
			nombre:"align-self-*",
			help:"Used on flexbox items to align them vertically in relation to the parent container. If columns are used the items will align verticall. (start, end, center, baseline, or stretch (browser default)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#align-self",
			classes:["align-self-start", "align-self-end", "align-self-center", "align-self-baseline", "align-self-stretch"]
		},
		{
			nombre:"bg-*",
			help:"Background color utility classes: .bg-(light, dark primary, secondary, transparent, white, warning, success, info, danger)",
			link:"https://getbootstrap.com/docs/4.0/utilities/colors/",
			classes:["bg-light", "bg-dark", "bg-primary", "bg-secondary", "bg-transparent", "bg-white", "bg-warning", "bg-success", "bg-info", "bg-danger"]
		},
		{
			nombre:"border (base)",
			multiple:true,
			help:"A versatile border utility class that lets you remove borders on a side or change a border color. .border-(light, dark primary, secondary, transparent, white, warning, success, info, danger, 0, top-0, right-0, bottom-0, left-0)",
			link:"https://getbootstrap.com/docs/4.0/utilities/borders/",
			classes:["border", "border-0", "border-top-0", "border-right-0", "border-bottom-0", "border-left-0"]
		},
		{
			nombre:"border (color)",
			help:"A versatile border utility class that lets you remove borders on a side or change a border color. .border-(light, dark primary, secondary, transparent, white, warning, success, info, danger, 0, top-0, right-0, bottom-0, left-0)",
			link:"https://getbootstrap.com/docs/4.0/utilities/borders/",
			classes:["border-primary", "border-secondary", "border-success", "border-danger", "border-warning", "border-info", "border-light", "border-dark", "border-white"]
		},
		{
			nombre:"float-*",
			help:"Responsive utility to float an element. .float-(sm, md, lg, xl)-(none, left, right)",
			link:"https://getbootstrap.com/docs/4.0/utilities/float/",
			classes:["float-left", "float-right", "float-none"]
		},
		{
			nombre:"float-sm-*",
			help:"Responsive utility to float an element. .float-(sm, md, lg, xl)-(none, left, right)",
			link:"https://getbootstrap.com/docs/4.0/utilities/float/",
			classes:["float-sm-left", "float-sm-right", "float-sm-none"]
		},
		{
			nombre:"float-md-*",
			help:"Responsive utility to float an element. .float-(sm, md, lg, xl)-(none, left, right)",
			link:"https://getbootstrap.com/docs/4.0/utilities/float/",
			classes:["float-md-left", "float-md-right", "float-md-none"]
		},
		{
			nombre:"float-lg-*",
			help:"Responsive utility to float an element. .float-(sm, md, lg, xl)-(none, left, right)",
			link:"https://getbootstrap.com/docs/4.0/utilities/float/",
			classes:["float-lg-left", "float-lg-right", "float-lg-none"]
		},
		{
			nombre:"float-xl-*",
			help:"Responsive utility to float an element. .float-(sm, md, lg, xl)-(none, left, right)",
			link:"https://getbootstrap.com/docs/4.0/utilities/float/",
			classes:["float-xl-left", "float-xl-right", "float-xl-none"]
		},
		{
			nombre:"h-* (height)",
			help:"Height utility class that makes the element a percentage height of its parent element. h-(25,50,75,100)",
			link:"https://getbootstrap.com/docs/4.0/utilities/sizing/#content",
			classes:["h-25", "h-50", "h-75", "h-100"]
		},
		{
			nombre:"invalid-feedback",
			help:"This class can be added with server side form validation to add a feedback message to an invalid field",
			link:"https://getbootstrap.com/docs/4.0/components/forms/#server-side",
			classes:["invalid-feedback"]
		},
		{
			nombre:"justify-content-*",
			help:"Class specifies where the flex items will be positioned inside the container. .justify-content-(sm, md, lg, xl)-(start, end, center, between, around)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#justify-content",
			classes:["justify-content-start", "justify-content-end", "justify-content-center", "justify-content-between", "justify-content-around"]
		},
		{
			nombre:"justify-content-sm-*",
			help:"Class specifies where the flex items will be positioned inside the container. .justify-content-(sm, md, lg, xl)-(start, end, center, between, around)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#justify-content",
			classes:["justify-content-sm-start", "justify-content-sm-end", "justify-content-sm-center", "justify-content-sm-between", "justify-content-sm-around"]
		},
		{
			nombre:"justify-content-md-*",
			help:"Class specifies where the flex items will be positioned inside the container. .justify-content-(sm, md, lg, xl)-(start, end, center, between, around)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#justify-content",
			classes:["justify-content-md-start", "justify-content-md-end", "justify-content-md-center", "justify-content-md-between", "justify-content-md-around"]
		},
		{
			nombre:"justify-content-lg-*",
			help:"Class specifies where the flex items will be positioned inside the container. .justify-content-(sm, md, lg, xl)-(start, end, center, between, around)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#justify-content",
			classes:["justify-content-lg-start", "justify-content-lg-end", "justify-content-lg-center", "justify-content-lg-between", "justify-content-lg-around"]
		},
		{
			nombre:"justify-content-xl-*",
			help:"Class specifies where the flex items will be positioned inside the container. .justify-content-(sm, md, lg, xl)-(start, end, center, between, around)",
			link:"https://getbootstrap.com/docs/4.0/utilities/flex/#justify-content",
			classes:["justify-content-xl-start", "justify-content-xl-end", "justify-content-xl-center", "justify-content-xl-between", "justify-content-xl-around"]
		},
		{
			nombre:"rounded-*",
			help:"The .img-rounded class was renamed to this and is primarily used with images. However, the class just adds a border radius so you could use this on other elements that you would like a radius applied.",
			link:"https://getbootstrap.com/docs/4.0/utilities/borders/#border-radius",
			classes:["rounded", "rounded-top", "rounded-right", "rounded-bottom", "rounded-left", "rounded-circle", "rounded-0"]
		},
		{
			nombre:"w-* (width)",
			help:"Width utility class that makes the element a percentage width of its parent element. w-(25,50,75,100)",
			link:"https://getbootstrap.com/docs/4.0/utilities/sizing/",
			classes:["w-25", "w-50", "w-75", "w-100"]
		},
		{
			nombre:"alert-heading",
			help:"This class is added to headings inside alerts. It applies color:inherit so the colors match.",
			link:"https://getbootstrap.com/docs/4.0/components/alerts/#additional-content",
			classes:["alert-heading"]
		},
		{
			nombre:"text-* (text color)",
			help:"Color del texto",
			link:"https://getbootstrap.com/docs/4.0/utilities/colors/#color",
			classes:["text-primary", "text-secondary", "text-success", "text-danger", "text-warning", "text-info", "text-light", "text-dark", "text-muted", "text-white"]
		}
	],
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		if (!app.idxGrupoEstilos) app.idxGrupoEstilos = 0;
		
		this.$this.find("#edGrupoEstilos").change(function() {
			thisController.refrescaEstilos();			
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;		
		var html = "";
		this.grupos.forEach((g, i) => html += "<option value='" + i + "' " + (app.idxGrupoEstilos == i?" selected":"") + ">" + g.nombre + "</option>");
		this.$this.find("#edGrupoEstilos").html(html);
		this.refrescaEstilos();
	},
	refrescaEstilos:function() {
		var thisController = this;		
		var n = $(this.nodo.nodo);
		var idx = parseInt(this.$this.find("#edGrupoEstilos").val());
		app.idxGrupoEstilos = idx;
		var grupo = this.grupos[idx];
		this.$this.find("#help").text(grupo.help);
		this.$this.find("#link").attr("href", grupo.link);
		
		var classesToRemove = "";
		var html = "";
		grupo.classes.forEach(c => {
			if (classesToRemove.length) classesToRemove += " ";
			classesToRemove += c;
			html += "<li><input class='cambiador-estilo' data-estilo='" + c + "' type='checkbox' " + (n.hasClass(c)?" checked='checked'":"") + " />" + c + "</li>";
		});
		this.$this.find("#edEstilo").html(html);
		this.$this.find("#edEstilo").find(".cambiador-estilo").change(function() {
			var aplica = $(this).prop("checked")?true:false;
			if (!grupo.multiple) {
				n.removeClass(classesToRemove);
				if (aplica) {
					n.addClass($(this).attr("data-estilo"));				
				}
			} else {
				if (aplica) n.addClass($(this).attr("data-estilo"));
				else n.removeClass($(this).attr("data-estilo"));
			}
			thisController.parentController.cambioControl(thisController.nodo);
		});
	}
}