var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edPL").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("pl-0 pl-1 pl-2 pl-3 pl-4 pl-5");
			var m = $(this).val();
			if (m) n.addClass("pl-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPR").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("pr-0 pr-1 pr-2 pr-3 pr-4 pr-5");
			var m = $(this).val();
			if (m) n.addClass("pr-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPT").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("pt-0 pt-1 pt-2 pt-3 pt-4 pt-5");
			var m = $(this).val();
			if (m) n.addClass("pt-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPB").change(function() {			
			var n = $(thisController.nodo.nodo);
			n.removeClass("pb-0 pb-1 pb-2 pb-3 pb-4 pb-5");
			var m = $(this).val();
			if (m) n.addClass("pb-" + m);
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		var thisController = this;
		this.nodo = nodo;
		var n = $(nodo.nodo);
		var h = getHandler(nodo.tipo);
		var classes = h.getClasses(nodo.nodo);
		$.each(classes, function(i, c) {
			if (c.startsWith("pl-")) thisController.$this.find("#edPL").val(c.substring(3));
			if (c.startsWith("pr-")) thisController.$this.find("#edPR").val(c.substring(3));
			if (c.startsWith("pt-")) thisController.$this.find("#edPT").val(c.substring(3));
			if (c.startsWith("pb-")) thisController.$this.find("#edPB").val(c.substring(3));
		});
	}
}