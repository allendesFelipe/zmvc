var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#cmdAgregarPagina").button({text:false, icons:{primary:"ui-icon-circle-plus"}})
		.click(function() {
			thisController.agregarPagina();
		});
		this.$this.find("#cmdEliminarPagina").button({text:false, icons:{primary:"ui-icon-trash"}})
		.click(function() {
			thisController.eliminarPagina();
		});
		this.$this.find("#edPagina").change(function() {
			thisController.refrescaPagina();
		});
		this.$this.find("#edIdPagina").change(function() {
			var l = thisController.getNavLink();
			if (!l) return;
			var id = $(this).val();
			if (!id) $(l).attr("href", "");
			else $(l).attr("href", "#" + id);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edEstilo").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("nav-tabs nav-pills");
			n.addClass($(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edInitial").change(function() {
			var esInicial = $(this).prop("checked");
			if (!esInicial) {
				openDialog("common/w-error", {message:"La página inicial es obligatoria. Para cambiarla, por favor seleccione la nueva página inicial y chequee la casilla de 'Inicial' en esa página"});
				$(this).prop("checked", true);
				return;
			}
			var l = thisController.getNavLink();
			if (!l) return;
			var id = $(l).attr("href");
			if (!id || id.length < 2) {
				openDialog("common/w-error", {message:"Debe seleccionar un Id de página válida"});
				return;
			}
			var p = thisController.getPaginaPorId(id.substring(1));
			if (!p) {
				openDialog("common/w-error", {message:"No se encontró la página destino '" + id + "' en este control"});
				return;
			}
			thisController.limpiaPaginasIniciales();
			$(p).addClass("show active");
			thisController.limpiaNavLinksIniciales();
			$(l).addClass("active");
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edTitulo").change(function() {
			var etiqueta = $(this).val();
			var l = thisController.getNavLink();
			if (!l) return;
			var child = null;
			var candidato = null;
			$.each(l.childNodes, function(i, c) {
				if (c.nodeType == 3) {
					if (c.nodeValue.trim()) {
						child = c;
						return false;						
					} else {
						candidato = c;
					}
				}
			});
			if (!child) child = candidato;
			if (child) {
				child.nodeValue = etiqueta;
			} else {
				$(l).text(etiqueta);
			}
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	limpiaPaginasIniciales:function() {
		var l = this.getNavLink();
		if (!l) return;
		var id = $(l).attr("href");
		if (!id || id.length < 2) return;
		var p = this.getPaginaPorId(id.substring(1));
		if (!p) return;
		var contenedor = $(p).parent();
		if (!contenedor || !contenedor.length) return;
		contenedor.find(".tab-pane").removeClass("show active");
	},
	limpiaNavLinksIniciales:function() {
		var l = this.getNavLink();
		if (!l) return;
		$(l).parent().parent().find(".nav-link").removeClass("active");
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		if (n.hasClass("nav-tabs")) this.$this.find("#edEstilo").val("nav-tabs");
		else this.$this.find("#edEstilo").val("nav-pills")
		var html = "<option value=''>[Sin Página Seleccionada]</option>";
		this.getPosiblesPaginas().forEach(p => html += "<option value='" + p.id + "'>" + p.id + "</option>");
		this.$this.find("#edIdPagina").html(html);
		this.refrescaPaginas();
	},
	getNavLinks:function() {
		var n = $(this.nodo.nodo);
		var ret = [];
		var lnks = null;
		$.each(n.find(".nav-item"), function(i, c) {
			var lnks = $(c).find(".nav-link");
			if (lnks.length) ret.push(lnks[0]);
		});
		return ret;
	},
	getTitulo:function(navLink) {
		var titulo = "";
		$.each(navLink.childNodes, function(i, c) {
			if (c.nodeType == 3) {
				if (c.nodeValue.trim()) {
					titulo = c.nodeValue;
					return false;				
				}
			}
		});
		return titulo;
	},
	refrescaPaginas:function() {	
		var thisController = this;
		var html = "";
		var links = this.getNavLinks();
		links.forEach((l,i) => html += "<option value='" + i + "'>" + thisController.getTitulo(l) + "</option>");
		this.$this.find("#edPagina").html(html);
		if (app.idxToSelect != null) {
			this.$this.find("#edPagina").val(app.idxToSelect);
			app.idxToSelect = null;
		} else if (links.length) {
			this.$this.find("#edPagina").val(0);
		}
		this.refrescaPagina();
	},
	getRootNode:function() {
		var n = $(this.nodo.nodo);
		while(n.parent().length) {
			n = n.parent();
		}
		return n;
	},
	getPosiblesPaginas:function() {
		var ret = [];
		$.each(this.getRootNode().find(".tab-pane"), function(i, p) {
			if (p.id) ret.push(p);
		});
		return ret;
	},
	getPaginaPorId:function(id) {
		var found = this.getRootNode().find("#" + id);
		if (found.length) return found[0];
		else return null;
	},
	getNavLink:function() {
		var idx = parseInt(this.$this.find("#edPagina").val());
		if (isNaN(idx)) return null;
		return this.getNavLinks()[idx];
	},
	refrescaPagina:function() {
		var l = this.getNavLink();
		if (!l) {
			this.$this.find("#editorPagina").hide();
			return;
		}
		app.idxToSelect = parseInt(this.$this.find("#edPagina").val());
		var href = $(l).attr("href");
		var paginaDestino = href?href.substring(1):"";		
		var active = $(l).hasClass("active");
		var titulo = this.getTitulo(l);
		this.$this.find("#edIdPagina").val(paginaDestino);
		if (!this.$this.find("#edIdPagina").val()) this.$this.find("#edIdPagina").val("");
		this.$this.find("#edInitial").prop("checked", active);
		this.$this.find("#edTitulo").val(titulo);
	},
	agregarPagina:function() {
		var l = this.getNavLink();
		if (!l) return;
		var html='\t\t<li class="nav-item">\n\t\t\t<a class="nav-link" data-toggle="tab" role="tab">Nueva Página</a>\n\t\t</li>\n';
		$(l).parent().parent().append(html);
		this.parentController.cambioControl(this.nodo);
	},
	eliminarPagina:function() {
		var thisController = this;
		var l = this.getNavLink();
		if (!l) return;
		openDialog("common/w-confirm", {message:"¿Confirma que desea eliminar la página seleccionada?"}, function() {
			$(l).remove();
			thisController.parentController.cambioControl(thisController.nodo);
		});
	}
}