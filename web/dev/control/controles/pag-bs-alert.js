var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edEstilo").change(function() {
			var n = $(thisController.nodo.nodo);
			n.removeClass("alert alert-primary alert-secondary alert-success alert-danger alert-warning alert-info alert-light alert-dark");
			var style = $(this).val();
			if (style) n.addClass("alert " + style);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		
		this.$this.find("#edVisible").change(function() {
			var val = $(this).val();
			if (val == "invisible") $(thisController.nodo.nodo).css("display", "none");
			else $(thisController.nodo.nodo).css("display", "");
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edId").val(n.prop("id"));
		var h = getHandler(nodo.tipo);
		var classes = h.getClasses(n);
		var estilo = "";
		$.each(classes, function(i, c) {
			if (c.startsWith("alert-")) {
				estilo = c;
				return false;
			}
		});
		this.$this.find("#edEstilo").val(estilo);
		if (n.css("display") == "none") this.$this.find("#edVisible").val("invisible");
		else this.$this.find("#edVisible").val("visible");
	}
}