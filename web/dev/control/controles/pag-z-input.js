var controller = {
	$this:null,
	parentController:null,
	nodo:null,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#edType").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("type", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edId").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("id", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edAutochangeDelay").change(function() {
			var delay = parseInt($(this).val());
			if (isNaN(delay) || delay < 10 || delay > 10000) return;
			var n = $(thisController.nodo.nodo);
			n.attr("data-z-autochange-delay", "" + delay);
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edAutochange").change(function() {
			var delay = parseInt(thisController.$this.find("#edAutochangeDelay").val());
			if (isNaN(delay) || delay < 10 || delay > 10000) delay = 300;
			var n = $(thisController.nodo.nodo);
			if ($(this).prop("checked")) {
				n.attr("data-z-autochange-delay", "" + delay);
			} else {
				n.removeAttr("data-z-autochange-delay");
			}
			thisController.parentController.cambioControl(thisController.nodo);
		});
		this.$this.find("#edPlaceholder").change(function() {
			var n = $(thisController.nodo.nodo);
			n.prop("placeholder", $(this).val());
			thisController.parentController.cambioControl(thisController.nodo);
		});
	},
	inicializa:function(parentController) {
		this.parentController = parentController;
	},
	refresca:function(nodo) {
		this.nodo = nodo;
		var n = $(nodo.nodo);
		this.$this.find("#edType").val(n.prop("type"));
		this.$this.find("#edId").val(n.prop("id"));
		this.$this.find("#edPlaceholder").val(n.prop("placeholder"));
		var h = getHandler(nodo.tipo);
		this.$this.find("#edAutochange").prop("checked", h.isAutochange(n));
		this.$this.find("#edAutochangeDelay").val(n.attr("data-z-autochange-delay")?n.attr("data-z-autochange-delay"):"300");
		if (h.isAutochange(n)) this.$this.find("#edAutochangeDelay").show();
		else this.$this.find("#edAutochangeDelay").hide();
	}
}