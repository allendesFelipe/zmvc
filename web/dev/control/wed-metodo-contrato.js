var controller = {
	createInstance:function(html, initialData) {
		var thisController = this;
		var w = $(html).dialog({
			autoOpen:false,
			title:(initialData.nuevoRegistro?"Nuevo Método":"Editar Método"),
			closeOnEscape:true,
			closeText:"Cerrar",
			width:400,
			height:160,
			modal:true,
			position:{my:"center"},
			buttons:{"Aceptar":{
			  text:"Aceptar",
			  click:function() {thisController.onOk(w, initialData);}
			}, "Cancelar":{
			  text:"Cancelar",
			  click:function() {thisController.onCancel(w, initialData);}			
			}}
		});
		w.keyup(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				thisController.onOk(w, initialData);
				return false;
			}
		});
		
		attachRootLayoutPanelToDialog(w);		
		w.bind("dialogclose", function() {
			thisController.destroyInstance(w, initialData);
		});
		
		return w;
	},
	init:function(w, initialData) {
		this.showRecord(w, initialData.record, initialData);
	},
	showRecord:function(w, r, initialData) {
		if (!initialData.nuevoRegistro) {
			w.find("#edMetodo").val(r.metodo);
		}
	},
	fetchRecord:function(w, initialData) {
		var metodo = w.find("#edMetodo").val();
		if (metodo) metodo = metodo.trim();
		if (!metodo) {
			openDialog("common/w-error", {message:"Debe ingresar la Firma del Método"});
			return null;
		}
		return {
			metodo:metodo
		};
	},
	onOk:function(w, initialData) {
		var r = this.fetchRecord(w, initialData);
		if (r) {
			w.dialog("close");
			if (initialData.onOk) initialData.onOk(r);
		}
	},
	onCancel:function(w, initialData) {
		w.dialog("close");
	},
	destroyInstance:function(w, initialData) {
		w.remove();
	}
}