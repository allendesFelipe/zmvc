var controller = {
	$this:null,
	parentController:null,
	control:null,
	editor:null,
	controlId:guid(),
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		this.$this.find("#conPanel").zSidePanel();
		this.$this.find("#cmdMenu").click(function() {
			thisController.alternaPanelMenu();
		});
		this.$this.find("#cmdCerrarMenu").click(function() {
			thisController.alternaPanelMenu();
		});
		this.$this.find("#contenedor-editor").html("<div></div>");
		this.$this.find("#cmdContrato").button({icons:{secondary:"ui-icon-check"}})
			.click(function() {
				thisController.editarContrato();
			});
	},
	inicializa:function(parentController, control) {
		var thisController = this;
		this.control = control;
		this.parentController = parentController;
		var editorId = "ed-a-" + this.controlId;
		this.$this.find("#contenedor-editor").html("<div id='" + editorId + "'></div>");
		ace.require("ace/ext/language_tools");
		this.editor = ace.edit(editorId);
		this.editor.$blockScrolling = Infinity;
		this.$this.find("#contenedor-editor").zLayout("doLayout");
		this.editor.setTheme("ace/theme/eclipse");
		this.editor.getSession().setMode("ace/mode/javascript");
		this.editor.setOptions({
	        enableBasicAutocompletion: true,
	        enableSnippets: true,
	        enableLiveAutocompletion: false
	    });
		this.editor.insert(this.control.controlador);
		this.editor.setShowPrintMargin(false);
		this.$this.find("#" + editorId).data("on-layout-resize", function() {
			thisController.editor.resize();				
		});
		this.editor.on("change", function() {
			thisController.parentController.markChanged();
		});
		this.refrescaArbolPaste();
	},
	fetch:function() {
		this.control.controlador = this.editor.getValue();
	},	
	alternaPanelMenu:function() {
		var conPanel = this.$this.find("#conPanel");
		if (conPanel.zSidePanel("isOpen")) {
			conPanel.zSidePanel("close");
		} else {
			conPanel.zSidePanel("open");
		}
	},
	refrescaArbolPaste:function() {
		var thisController = this;
		var id = "apc_" + this.controlId;
		this.$this.find("#contenedor-arbol-paste").html("<div id='contenedor-" + id + "'><table id='" + id + "'></table></div>");
		var arbol = this.$this.find("#" + id).jqGrid({
            datatype: "local",
            gridview: true,
            rowNum: 10000,
            sortname: "nombre",
            treeGrid: true,            
            treeGridModel: 'adjacency',
            treedatatype: "local",
            ExpandColumn: "nombre",
            ExpandColClick: true,            
            autowidth: true,
            hidegrid: false,
            shrinkToFit: false,
            colNames: ["id", "tipo", " ", "Objeto / Método / Evento", "help", "pasteCode"],
            colModel: [
			  { name: "id", index: "id", key: true, hidden: true },
              { name: "tipo", index: "tipo", hidden: true },
              { name: "imgTipo", index: "imgTipo", width:20, align:"center" },
              { name: "nombre", index: "nombre", width: 240, sortable: false },
              { name: "help", index: "help", hidden: true },
              { name: "pasteCode", index: "pasteCode", hidden: true }
            ],
            caption: false,
            onSelectRow:function() {
            	var tree = thisController.$this.find("#" + id);
            	var idNodo = tree.jqGrid("getGridParam", "selrow");
            	if (!idNodo) return;
            	var n = tree.jqGrid("getRowData", idNodo);
            	thisController.$this.find("#ayuda").html(n.help);
            },
            ondblClickRow:function(rowId, iRow, iCol, e) {
            	var tree = thisController.$this.find("#" + id);
            	var idNodo = tree.jqGrid("getGridParam", "selrow");
            	if (!idNodo) return;
            	var n = tree.jqGrid("getRowData", idNodo);
            	if (n.tipo == "metodo" || n.tipo == "evento") {
            		thisController.pasteNodo(n);
            		thisController.alternaPanelMenu();
            	}
            }
        });
        attachJQGridToContainerPanel(this.$this.find("#contenedor-" + id), arbol, true);
        
        thisController.$this.find("#ayuda").html("Seleccione un elemento del árbol para mostrar acá su documentación.<hr /><br />Doble click sobre un elemento final para pegarlo en el código.");
        this.$this.find("#contenedor-arbol-paste").zLayout("doLayout");
	},
	pasteNodo:function(n) {
		this.editor.insert(n.pasteCode);
		this.editor.focus();
	},
	cambioEstructura:function(controles) {
		if (!this.control) return;
		var thisController = this;
		var id = "apc_" + this.controlId;
		var pasteTree = this.$this.find("#" + id);
		pasteTree.jqGrid("clearGridData");
		
		var arbol = [
			{id:"this", tipo:"objeto", imgTipo:"<img src='img/16x16/objeto.png' />", nombre:"this", pasteCode:"this", help:"Este controlador", parent:null, level:0, isLeaf:false, expanded:false, loaded:true},
			{id:"trigger", tipo:"metodo", imgTipo:"<img src='img/16x16/metodo.png' />", nombre:"trigger", pasteCode:"this.trigger('nombreEvento', arg1, arg2)", help:"Dispara un evento que puede ser capturado en el controlador padre", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true},
			{id:"onThis_init", tipo:"evento", imgTipo:"<img src='img/16x16/evento.png' />", nombre:"init", pasteCode:"onThis_init:function() {\n\t\t\n\t\}", help:"Disparado una vez que el controlador y todos sus controladores hijos han terminado de cargarse", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true},
			{id:"onThis_activate", tipo:"evento", imgTipo:"<img src='img/16x16/evento.png' />", nombre:"activate", pasteCode:"onThis_activate:function() {\n\t\t\n\t\}", help:"Se dispara cuando el control se hace visible", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true},
			{id:"onThis_deactivate", tipo:"evento", imgTipo:"<img src='img/16x16/evento.png' />", nombre:"dactivate", pasteCode:"onThis_deactivate:function() {\n\t\t\n\t\}", help:"Se dispara cuando el control se hace invisible", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true}
		];
		if (this.control.nombre.startsWith("w")) {
			arbol.push({id:"close", tipo:"metodo", imgTipo:"<img src='img/16x16/metodo.png' />", nombre:"close", pasteCode:"this.close(arg1, arg2, ...)", help:"Cierra el diálogo retornando en el primer callback de openDialog los parámetros enviados como arga, arg2, ...", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true});
			arbol.push({id:"cancel", tipo:"metodo", imgTipo:"<img src='img/16x16/metodo.png' />", nombre:"cancel", pasteCode:"this.cancel()", help:"Cierra el diálogo invocando el segundo callback de openDialog", parent:"this", level:1, isLeaf:true, expanded:false, loaded:true});
		}
		var promesas = [];
		$.each(controles, function(i, c) {
			if (c.tipo && $(c.nodo).prop("id")) {
				var h = getHandler(c.tipo);
				promesas.push(h.getContrato(c.nodo, thisController.control.path));
			}
		});
		Promise.all(promesas)
		.then((contratos) => {
			contratos.forEach((contrato) => {
				if (contrato.metodos.length || contrato.eventos.length) {
					arbol.push({id:contrato.id, tipo:"objeto", imgTipo:"<img src='img/16x16/objeto.png' />", nombre:contrato.id, pasteCode:"this." + contrato.id, help:contrato.nombre, parent:null, level:0, isLeaf:false, expanded:false, loaded:true});
					var prefix = "this." + contrato.id + ".";
					contrato.metodos.forEach((m) => {
						var code = m.pasteCode;
						if (!code.endsWith(")")) code += "()";
						arbol.push({id:prefix + m.pasteCode, tipo:"metodo", imgTipo:"<img src='img/16x16/metodo.png' />", nombre:m.nombre, pasteCode:prefix + code, help:m.help, parent:contrato.id, level:1, isLeaf:true, expanded:false, loaded:true});
					});
					prefix = "on" + contrato.id.substring(0,1).toUpperCase() + contrato.id.substring(1) + "_";
					contrato.eventos.forEach((e) => {
						var p = e.pasteCode.indexOf("(");
						var evtName = p < 0?e.pasteCode:e.pasteCode.substring(0,p);
						var args = p < 0?"()":e.pasteCode.substring(p);
						arbol.push({id:prefix + evtName, tipo:"evento", imgTipo:"<img src='img/16x16/evento.png' />", nombre:e.nombre, pasteCode:prefix + evtName + ":function" + args + " {\n\t\t\n\t\}", help:e.help, parent:contrato.id, level:1, isLeaf:true, expanded:false, loaded:true});
					});
				}
			});
			var t = thisController.$this.find("#apc_" + thisController.controlId);
			t[0].addJSONData(arbol);
		})
		.catch((err) => console.log(err));
	},
	getContrato:function() {
		var contrato = {metodos:[], eventos:[]};
		var code = this.editor.getValue();
		var p = code.indexOf("contrato::{");
		if (p<0) return contrato;
		var p2 = code.indexOf("}", p);
		if (p2<0) return contrato;
		var j = code.substring(p+10, p2+1);
		try {
			contrato = JSON.parse(j);
			if (!contrato.metodos) contrato.metodos = [];
			if (!contrato.eventos) contrato.eventos = [];
		} catch(error) {
			return contrato;
		}
		return contrato;
	},
	insertaContratoInicial:function(j) {
		var code = this.editor.getValue();
		this.editor.setValue("/*\ncontrato::" + j + "\n*/\n" + code);
		this.editor.clearSelection();
	},
	setContrato:function(c) {
		var code = this.editor.getValue();
		var j = JSON.stringify(c, null, 4);
		var p = code.indexOf("contrato::{");
		if (p<0) {
			this.insertaContratoInicial(j);
			return;
		}
		var p2 = code.indexOf("}", p);
		if (p2<0) {
			this.insertaContratoInicial(j);
			return;
		}
		this.editor.setValue(code.substring(0, p) + "contrato::" + j + code.substring(p2+1));
		this.editor.clearSelection();
	},
	editarContrato:function() {
		var thisController = this;
		var contrato = this.getContrato();
		openDialog("dev/control/wed-contrato", {contrato:contrato, onOk:function(c) {
			thisController.setContrato(c);
		}});
	}
}