var controller = {
	$this:null,
	arbol:null,
	contenedorFolders:null,
	init:function(container, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		httpInvoke("getInitialPath.zdev", {}, function(path) {
			app.path = path;
			console.log("initialPath", path);
			var alArbol = thisController.$this.find("#al-arbol");
			whenAutoloadReady(alArbol, function() {
				thisController.arbol = alArbol.data("controller");
				console.log("arbol cargado", alArbol.data("controller"));
				thisController.arbol.onAbrirItem = function(item) {
					thisController.abrirItem(item);
				};
				thisController.arbol.estaArchivoAbierto = function(path) {
					return thisController.estaArchivoAbierto(path);
				};
				thisController.arbol.hayArchivosAbiertos = function() {
					return thisController.hayArchivosAbiertos();
				};
				thisController.refresca();
			});
		});
		var alContenedorFolders = this.$this.find("#alContenedorFolders");
		whenAutoloadReady(alContenedorFolders, function() {
			thisController.contenedorFolders = alContenedorFolders.data("controller");
			thisController.contenedorFolders.onAbrirItem = function(tipo, path) {
				thisController.abrirItem({tipo:tipo, path:path});
			}
		});
	},
	refresca:function() {
		console.log("path", app.path);
		this.$this.find("#lblPath").val(app.path);
		this.arbol.refresca();
	},
	abrirItem:function(item) {
		if (item.tipo == "control") {
			this.contenedorFolders.abreControl(item.path);
		} else if (item.tipo == "js") {
			this.contenedorFolders.abreJavaScript(item.path);
		} else if (item.tipo == "html") {
			this.contenedorFolders.abreHTML(item.path);
		} else if (item.tipo == "css") {
			this.contenedorFolders.abreCSS(item.path);
		} else if (item.tipo == "znav") {
			this.contenedorFolders.abreZNav(item.path);
		}
	},
	estaArchivoAbierto:function(path) {
		return this.contenedorFolders.existeTab(path);
	},
	hayArchivosAbiertos:function() {
		return this.contenedorFolders.hayTabsAbiertos();
	}
}