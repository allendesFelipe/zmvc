var controller = {
	$this:null,
	parentController:null,
	control:null,
	changed:false,
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		var cmdSave = this.$this.find("#cmdSave").button({icons:{secondary:"ui-icon-disk"}}).click(function() {
			thisController.save();
		});
		enableButton(cmdSave, false);
		this.$this.find(".cuadro-opcion-control").click(function() {
			var opcion = $(this).data("codigo-opcion");
			thisController.$this.find(".cuadro-opcion-control").removeClass("opcion-control-seleccionada");
			$(this).addClass("opcion-control-seleccionada");
			thisController.$this.find("#pager").zPager("gotoPage", opcion);
		});
	},
	getPathRelativo:function(path) {
		return path.substring(app.path.length + 1);
	},
	inicializa:function(parentController, control) {
		var thisController = this;
		this.parentController = parentController;
		this.control = control;
		this.changed = false;
		this.$this.find("#lblTitulo").text(this.getPathRelativo(control.path));
		this.$this.find("#pager").zPager({
			pages:[
			    {code:"vista", url:"dev/control/vista", initial:true},
			    {code:"controlador", url:"dev/control/controlador"},
			    {code:"debugger", url:"dev/control/debugger"}
			],
			onReady:function() {
				thisController.refrescaPaneles();
			}
		});
	},
	isChanged:function() {return this.changed;},
	markChanged:function() {
		enableButton(this.$this.find("#cmdSave"), true);
		this.changed = true;
	},
	refrescaPaneles:function() {
		this.$this.find("#pager").zPager("controller", "controlador").inicializa(this, this.control);
		this.$this.find("#pager").zPager("controller", "vista").inicializa(this, this.control);
		this.$this.find("#pager").zPager("controller", "debugger").inicializa(this, this.control);
	},
	fetch:function() {
		this.$this.find("#pager").zPager("controller", "vista").fetch();
		this.$this.find("#pager").zPager("controller", "controlador").fetch();
		this.$this.find("#pager").zPager("controller", "debugger").fetch();
		return this.control;
	},
	save:function(callback) {
		var thisController = this;
		this.fetch();
		httpInvoke("saveControl.zdev", {control:this.control}, function(control) {
			enableButton(thisController.$this.find("#cmdSave"), false);
			thisController.changed = false;
			thisController.control = control;
			thisController.refrescaPaneles();
			if (callback) callback();
		});
	},
	cambioEstructura:function(controles) {
		this.$this.find("#pager").zPager("controller", "controlador").cambioEstructura(controles);
	},
	getContrato:function() {
		return this.$this.find("#pager").zPager("controller", "controlador").getContrato();
	}
}