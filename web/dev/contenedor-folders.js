var controller = {
	$this:null,
	onAbrirItem:null, // function(tipo, path)
	tabControllers:{},
	init:function(container, p, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		// Tabs
		var tabs = this.$this.find("#tabs").tabs({
			activate:function() {thisController.tabChange();}
		});
		this.$this.find(".ui-tabs-nav" ).sortable({axis: "x", stop: function() {tabs.tabs("refresh");}});
		tabs.data("on-layout-resize", function() {thisController.resizeTabContent()});
	},
	
	// Tabs Handling
	resizeTabContent:function() {
		var tabs = this.$this.find("#tabs");
		var activeIndex = tabs.tabs("option", "active");
		var ul = $(tabs.children()[0]);
		var lis = ul.children();
		var li = $(lis[activeIndex]);
		var a = $(li.find("a"))[0];
		if (!a) return;
		var hash = a.hash;
		var content = $(tabs.find(hash));		
		var margins = getChildMargins(content);
		content.css("position", "absolute");
		content.css("left", margins[0]);
		content.css("top", ul.height() + 1 + margins[1]);
		content.width(tabs.width() - margins[0] - margins[2]);
		content.height(tabs.height() - ul.height() - 5 - margins[1] - margins[3]);
		doChildLayout(content);
	},
	getTabIndex:function(id) {
		var tabs = this.$this.find("#tabs");
		var ul = $(tabs.children()[0]);
		var lis = ul.children();
		var index = -1;
		$.each(lis, function(i, li) {
			var a = $($(li).find("a"))[0];
			var hash = a.hash;
			if (hash == ("#" + id)) {
				index = i;
				return false;
			}
		});
		return index;
	},
	getTabId:function(index) {
		var tabs = this.$this.find("#tabs");
		var ul = $(tabs.children()[0]);
		var lis = ul.children();
		var li = $(lis[index]);
		var a = $(li.find("a"))[0];
		if (!a) return null;
		var hash = a.hash;
		var content = $(tabs.find(hash));
		return content.attr("id");
	},
	getSelectedTabId:function() {
		var tabs = this.$this.find("#tabs");
		var activeIndex = tabs.tabs("option", "active");
		if (activeIndex < 0) return null;
		return this.getTabId(activeIndex);
	},
	setSelectedTabId:function(id) {
		var tabs = this.$this.find("#tabs");
		var index = this.getTabIndex(id);
		tabs.tabs("option", "active", index);
	},
	tabChange:function() {	
		this.resizeTabContent();
	},
	generaTabId:function(path) {
		return path.split("/").join("_").split(".").join("_").split(":").join("_").split("\\").join("_");
	},
	existeTab:function(path) {
		var id = this.generaTabId(path);
		return this.getTabIndex(id) >= 0;
	},
	hayTabsAbiertos:function() {
		var tabs = this.$this.find("#tabs");
		var ul = $(tabs.children()[0]);
		return ul.children().length > 0;
	},
	abreControl:function(path) {
		var id = this.generaTabId(path);
		var thisController = this;
		var tabId = id;
		if (this.getTabIndex(tabId) >= 0) {
			this.setSelectedTabId(tabId);
			return;
		}
		httpInvoke("getControl.zdev", {path:path}, function(control) {
			var tabs = thisController.$this.find("#tabs");
			var html = "<div id='" + tabId + "' data-layout='rows(100%)'>";
			html += "<div id='auto-" + tabId + "' data-layout-autoload='dev/control'></div>";
			html += "</div>";
			var ul = $(tabs.children()[0]);
			ul.append("<li><span><a id='tabLabel-" + tabId + "' href='#" + tabId + "'>" + control.nombre + "</a></span><span id='tabClose-" + tabId + "'><img src='img/16x16/cancel.png' width='10' height='10' style='padding-top:4px;padding-right:4px;cursor:pointer;' /></span></li>");
			tabs.append(html);
			var newTab = tabs.find("#auto-" + tabId);
			whenAutoloadReady(newTab, function() {
				var controller = newTab.data("controller");
				thisController.tabControllers[tabId] = controller;
				controller.inicializa(thisController, control);
			});
			tabs.find("#tabClose-" + tabId).click(function() {
				thisController.tabCloseRequest(tabId);
			});
			tabs.tabs("refresh");
			thisController.setSelectedTabId(tabId);
			thisController.resizeTabContent();
		});
	},	
	abreJavaScript:function(path) {
		var id = this.generaTabId(path);
		var thisController = this;
		var tabId = id;
		if (this.getTabIndex(tabId) >= 0) {
			this.setSelectedTabId(tabId);
			return;
		}
		httpInvoke("getArchivo.zdev", {path:path}, function(contenido) {
			var tabs = thisController.$this.find("#tabs");
			var html = "<div id='" + tabId + "' data-layout='rows(100%)'>";
			html += "<div id='auto-" + tabId + "' data-layout-autoload='dev/javascript'></div>";
			html += "</div>";
			var ul = $(tabs.children()[0]);
			var nombre = path.substring(path.lastIndexOf("/") + 1);
			ul.append("<li><span><a id='tabLabel-" + tabId + "' href='#" + tabId + "'>" + nombre + "</a></span><span id='tabClose-" + tabId + "'><img src='img/16x16/cancel.png' width='10' height='10' style='padding-top:4px;padding-right:4px;cursor:pointer;' /></span></li>");
			tabs.append(html);
			var newTab = tabs.find("#auto-" + tabId);
			whenAutoloadReady(newTab, function() {
				var controller = newTab.data("controller");
				thisController.tabControllers[tabId] = controller;
				controller.inicializa(thisController, path, contenido);
			});
			tabs.find("#tabClose-" + tabId).click(function() {
				thisController.tabCloseRequest(tabId);
			});
			tabs.tabs("refresh");
			thisController.setSelectedTabId(tabId);
			thisController.resizeTabContent();
		});
	},	
	abreHTML:function(path) {
		var id = this.generaTabId(path);
		var thisController = this;
		var tabId = id;
		if (this.getTabIndex(tabId) >= 0) {
			this.setSelectedTabId(tabId);
			return;
		}
		httpInvoke("getArchivo.zdev", {path:path}, function(contenido) {
			var tabs = thisController.$this.find("#tabs");
			var html = "<div id='" + tabId + "' data-layout='rows(100%)'>";
			html += "<div id='auto-" + tabId + "' data-layout-autoload='dev/html'></div>";
			html += "</div>";
			var ul = $(tabs.children()[0]);
			var nombre = path.substring(path.lastIndexOf("/") + 1);
			ul.append("<li><span><a id='tabLabel-" + tabId + "' href='#" + tabId + "'>" + nombre + "</a></span><span id='tabClose-" + tabId + "'><img src='img/16x16/cancel.png' width='10' height='10' style='padding-top:4px;padding-right:4px;cursor:pointer;' /></span></li>");
			tabs.append(html);
			var newTab = tabs.find("#auto-" + tabId);
			whenAutoloadReady(newTab, function() {
				var controller = newTab.data("controller");
				thisController.tabControllers[tabId] = controller;
				controller.inicializa(thisController, path, contenido);
			});
			tabs.find("#tabClose-" + tabId).click(function() {
				thisController.tabCloseRequest(tabId);
			});
			tabs.tabs("refresh");
			thisController.setSelectedTabId(tabId);
			thisController.resizeTabContent();
		});
	},	
	abreCSS:function(path) {
		var id = this.generaTabId(path);
		var thisController = this;
		var tabId = id;
		if (this.getTabIndex(tabId) >= 0) {
			this.setSelectedTabId(tabId);
			return;
		}
		httpInvoke("getArchivo.zdev", {path:path}, function(contenido) {
			var tabs = thisController.$this.find("#tabs");
			var html = "<div id='" + tabId + "' data-layout='rows(100%)'>";
			html += "<div id='auto-" + tabId + "' data-layout-autoload='dev/css'></div>";
			html += "</div>";
			var ul = $(tabs.children()[0]);
			var nombre = path.substring(path.lastIndexOf("/") + 1);
			ul.append("<li><span><a id='tabLabel-" + tabId + "' href='#" + tabId + "'>" + nombre + "</a></span><span id='tabClose-" + tabId + "'><img src='img/16x16/cancel.png' width='10' height='10' style='padding-top:4px;padding-right:4px;cursor:pointer;' /></span></li>");
			tabs.append(html);
			var newTab = tabs.find("#auto-" + tabId);
			whenAutoloadReady(newTab, function() {
				var controller = newTab.data("controller");
				thisController.tabControllers[tabId] = controller;
				controller.inicializa(thisController, path, contenido);
			});
			tabs.find("#tabClose-" + tabId).click(function() {
				thisController.tabCloseRequest(tabId);
			});
			tabs.tabs("refresh");
			thisController.setSelectedTabId(tabId);
			thisController.resizeTabContent();
		});
	},
	abreZNav:function(path) {
		var id = this.generaTabId(path);
		var thisController = this;
		var tabId = id;
		if (this.getTabIndex(tabId) >= 0) {
			this.setSelectedTabId(tabId);
			return;
		}
		httpInvoke("getArchivo.zdev", {path:path}, function(contenido) {
			var tabs = thisController.$this.find("#tabs");
			var html = "<div id='" + tabId + "' data-layout='rows(100%)'>";
			html += "<div id='auto-" + tabId + "' data-layout-autoload='dev/navegacion/navegacion'></div>";
			html += "</div>";
			var ul = $(tabs.children()[0]);
			var nombre = path.substring(path.lastIndexOf("/") + 1);
			ul.append("<li><span><a id='tabLabel-" + tabId + "' href='#" + tabId + "'>" + nombre + "</a></span><span id='tabClose-" + tabId + "'><img src='img/16x16/cancel.png' width='10' height='10' style='padding-top:4px;padding-right:4px;cursor:pointer;' /></span></li>");
			tabs.append(html);
			var newTab = tabs.find("#auto-" + tabId);
			whenAutoloadReady(newTab, function() {
				var controller = newTab.data("controller");
				thisController.tabControllers[tabId] = controller;
				controller.inicializa(thisController, path, contenido);
			});
			tabs.find("#tabClose-" + tabId).click(function() {
				thisController.tabCloseRequest(tabId);
			});
			tabs.tabs("refresh");
			thisController.setSelectedTabId(tabId);
			thisController.resizeTabContent();
		});
	},
	tabCloseRequest:function(id) {
		var thisController = this;
		var controller = this.tabControllers[id];
		if (controller.isChanged()) {
			openDialog("common/w-confirm-discard", {message:"El elemento ha sido modificado. ¿Confirma que descarta los cambios?",
				onCancel:function() {
				},
				onSave:function() {
					controller.save(function() {
						thisController.closeTab(id);
					});
				},
				onDiscard:function() {
					thisController.closeTab(id);
				}
			});
		} else {
			this.closeTab(id);
		}
	},
	closeTab:function(id) {
		var tabs = this.$this.find("#tabs");
		var index = this.getTabIndex(id);
		var ul = $(tabs.children()[0]);
		var lis = ul.children();
		var li = $(lis[index]);
		var a = $(li.find("a"))[0];
		var hash = a.hash;
		var content = $(tabs.find(hash));
		li.remove();
		content.remove();
		delete this.tabControllers[id];
		if (Object.keys(this.tabControllers).length > 0) {
			tabs.tabs("refresh");
			if (!this.getSelectedTabId()) {
				tabs.tabs("option", "active", 0);
			}			
		}	
		this.resizeTabContent();
	}
}