var controller = {
	$this:null,
	buscador:null,
	timer:null,
	onAbrirItem:null,
	estaArchivoAbierto:null,
	hayArchivosAbiertos:null,
	init:function(container, options) {
		this.$this = $(container.children()[0]);
		var thisController = this;
		
		this.$this.find("#pop-menu").menu({
			select:function(e, ui) {
				var id = $(e.originalEvent.target).attr("id");
				if (id == "cmdAddControl") {
					thisController.addControl();
				} else if (id == "cmdAddCSS") {
					thisController.addCSS();
				} else if (id == "cmdAddHTML") {
					thisController.addHTML();
				} else if (id == "cmdAddJS") {
					thisController.addJS();
				} else if (id == "cmdAddMockServer") {
					thisController.addMock();
				} else if (id == "cmdAddFolder") {
					thisController.addFolder();
				} else if (id == "cmdRenombrar") {
					thisController.renombrar();
				} else if (id == "cmdEliminar") {
					thisController.eliminar();
				}
			}
		});
		
		this.$this.find("#cmdOpciones").button({icons:{secondary:"ui-icon-circle-triangle-s"}})
		.click(function() {
			var menu = thisController.$this.find("#pop-menu");
			var n = thisController.getNodoSeleccionado();
			if (!n) {
				menu.find("#cmdAddControl").addClass("ui-state-disabled");
				menu.find("#cmdAddCSS").addClass("ui-state-disabled");
				menu.find("#cmdAddHTML").addClass("ui-state-disabled");
				menu.find("#cmdAddJS").addClass("ui-state-disabled");
				menu.find("#cmdAddZNav").addClass("ui-state-disabled");
				menu.find("#cmdAddFolder").addClass("ui-state-disabled");
				menu.find("#cmdRenombrar").addClass("ui-state-disabled");
				menu.find("#cmdEliminar").addClass("ui-state-disabled");
			} else {
				if (n.tipo == "folder") {
					menu.find("#cmdAddControl").removeClass("ui-state-disabled");
					menu.find("#cmdAddCSS").removeClass("ui-state-disabled");
					menu.find("#cmdAddHTML").removeClass("ui-state-disabled");
					menu.find("#cmdAddJS").removeClass("ui-state-disabled");
					menu.find("#cmdAddZNav").removeClass("ui-state-disabled");
					menu.find("#cmdAddFolder").removeClass("ui-state-disabled");
					menu.find("#cmdRenombrar").removeClass("ui-state-disabled");
					menu.find("#cmdEliminar").removeClass("ui-state-disabled");					
				} else {
					menu.find("#cmdAddControl").addClass("ui-state-disabled");
					menu.find("#cmdAddCSS").addClass("ui-state-disabled");
					menu.find("#cmdAddHTML").addClass("ui-state-disabled");
					menu.find("#cmdAddJS").addClass("ui-state-disabled");
					menu.find("#cmdAddZNav").addClass("ui-state-disabled");
					menu.find("#cmdAddFolder").addClass("ui-state-disabled");
					menu.find("#cmdRenombrar").removeClass("ui-state-disabled");
					menu.find("#cmdEliminar").removeClass("ui-state-disabled");
				}
			}
			menu.show().position({
				my: "right top",
				at: "right bottom",
				of: this
			});
			$(document).one("click", function() {
				menu.hide();
			});
			return false; // prevent default
		});
		
		this.$this.find("#cmdRefresh").button({icons:{primary:"ui-icon-refresh"}, text:false}).click(function() {
			thisController.callRefrescaArchivos(10);
		});
		
		// Arbol
		var arbol = this.$this.find("#arbolArchivos").jqGrid({
            datatype: "local",
            gridview: true,
            rowNum: 10000,
            sortname: "id",
            treeGrid: true,            
            treeGridModel: 'adjacency',
            treedatatype: "local",
            ExpandColumn: "nombre",
            ExpandColClick: true,            
            autowidth: true,
            hidegrid: false,
            shrinkToFit: false,
            colNames: ["id", "path", "", "Nombre", "tipo"],
            colModel: [
              { name: "id", index: "id", key: true, hidden: true },
			  { name: "path", index: "path", hidden: true },
			  { name: "imgTipo", index: "imgTipo", width: 30, align:"center" },
			  { name: "nombre", index: "nombre", width: 240 },
              { name: "tipo", index: "tipo", hidden: true }
            ],
            caption: false,
            ondblClickRow:function(rowId, iRow, iCol, e) {
            	var n = thisController.getNodoSeleccionado();
            	if (!n) return;
            	thisController.onAbrirItem(n);
            }
        });
        attachJQGridToContainerPanel(this.$this.find("#arbolArchivosContainer"), arbol, true);
        
        this.$this.find("#edFiltro").keyup(function() {
        	thisController.callBuscaArchivo();
        });
        this.$this.find("#edFiltro").click(function() {
        	thisController.callBuscaArchivo();
        });
               
	},
	refresca:function() {
		 this.callRefrescaArchivos(10);
	},
	getNodoSeleccionado:function() {
		var arbol = this.$this.find("#arbolArchivos");
		var id = arbol.jqGrid("getGridParam", "selrow");
		if (!id) return null;
		return arbol.jqGrid("getRowData", id);
	},	
	seleccionaNodo:function(idNodo) {
		var thisController = this;
		var arbol = this.$this.find("#arbolArchivos");
		var nodo = arbol.jqGrid("getRowData", idNodo);
		var parent = arbol.jqGrid("getNodeParent", nodo);
		while(parent) {
			arbol.jqGrid("expandRow", parent);
			arbol.jqGrid("expandNode", parent);
			parent = arbol.jqGrid("getNodeParent", parent);
		}
		arbol.jqGrid("setSelection", idNodo, true);
		//this.expandeSubArbol(idNodo);
	},
	callBuscaArchivo:function(delay) {
		var thisController = this;
		if (!delay) delay = 300;
		if (this.timer) {
			clearTimeout(this.timer);
			this.timer = null;
		}
		var f = this.$this.find("#edFiltro").val().trim();
		if (!f) return;
		this.timer = setTimeout(function() {
			thisController.timer = null;
			thisController.buscaArchivo();
		}, delay);
	},
	buscaArchivo:function() {
		var arbol = app.arbolProyecto;
		var f = this.$this.find("#edFiltro").val().trim().toUpperCase();
		if (!f) return;
		var i=1;
		var n = this.getNodoSeleccionado();
		if (n) i = parseInt(n.id) + 1;
		if (i > arbol.length) i=1;
		var nBusquedas = 0;
		do {
			n = arbol[i - 1];
			if (!n) console.log("n es nulo para i ", i, arbol.length);
			var path = n.path;
			if (path) {
				var p =path.lastIndexOf("/");
				if (p<0) p = path.lastIndexOf("\\");
				if (p>=0) path = path.substring(p+1);
				if (path.toUpperCase().indexOf(f) >= 0) {
					this.seleccionaNodo(n.id);
					return;
				}
			}
			i++;
			if (i > arbol.length) i=1;			
		} while (++nBusquedas < arbol.length);
	},
	callRefrescaArchivos:function(delay, idToSelect) {
		if (!delay) delay = 300;
		var thisController = this;
		if (this.buscador) {
			this.buscador.abort();
			this.buscador = null;
		}
		if (this.timer) {
			clearTimeout(this.timer);
		}
		this.timer = setTimeout(function() {
			thisController.timer = null;
			thisController.refrescaArchivos(idToSelect);
		}, delay);
	},
	refrescaArchivos:function(idToSelect, pathToSelect, abrir) {
		var thisController = this;
		var filtro = this.$this.find("#edFiltro").val(); 
		var arbol = this.$this.find("#arbolArchivos");
		if (!idToSelect) {
			// Keep selection
			idToSelect = arbol.jqGrid("getGridParam", "selrow");
		}
		arbol.jqGrid("clearGridData");
		this.buscador = httpInvoke("getArbolArchivos.zdev", {path:app.path}, function(nodos) {
			$.each(nodos, function(i, n) {
				n.imgTipo = "<img src='img/16x16/" + n.tipo + ".png' />";
			});
			arbol[0].addJSONData(nodos);
			app.arbolProyecto = nodos;	
			if (pathToSelect) thisController.seleccionaPath(pathToSelect, abrir);
			else if (idToSelect) thisController.seleccionaNodo(idToSelect);			
		});
	},
	seleccionaPath:function(path, abrir) {
		var found = null;
		$.each(app.arbolProyecto, function(i, n) {
			if (n.path == path) {
				found = n;
				return false;
			}
		});
		if (found) {
			this.seleccionaNodo(found.id);
			if (abrir) this.onAbrirItem(found);
		}
	},
	addCSS:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		openDialog("dev/w-get-file-name", {tipo:"css", onOk:function(filename) {
			httpInvoke("creaArchivo.zdev", {path:path + "/" + filename, contenido:".clase {}"}, function() {
				thisController.refrescaArchivos(null, path + "/" + filename, true);
			});
		}});
	},
	addHTML:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		openDialog("dev/w-get-file-name", {tipo:"html", onOk:function(filename) {
			httpInvoke("creaArchivo.zdev", {path:path + "/" + filename, contenido:"<!DOCTYPE html>\n<html>\n</html>"}, function() {
				thisController.refrescaArchivos(null, path + "/" + filename, true);
			});
		}});
	},
	addJS:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		openDialog("dev/w-get-file-name", {tipo:"js", onOk:function(filename) {
			httpInvoke("creaArchivo.zdev", {path:path + "/" + filename, contenido:""}, function() {
				thisController.refrescaArchivos(null, path + "/" + filename, true);
			});
		}});
	},
	addZNav:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		var def = {
			id:guid(),
			type:"znav",
			width:640,
			height:480,
			zoom:100,
			backgroundColor:"white",
			name:"Modelo de Navegación",
			elements:{},
			lines:{}
		}
		openDialog("dev/w-get-file-name", {tipo:"znav", onOk:function(filename) {
			httpInvoke("creaArchivo.zdev", {path:path + "/" + filename, contenido:JSON.stringify(def, null, 4)}, function() {
				thisController.refrescaArchivos(null, path + "/" + filename, true);
			});
		}});
	},
	addMock:function() {
		var thisController = this;
		openDialog("dev/w-get-file-name", {tipo:"js", etiqueta:"Extensión URL", onOk:function(filename) {
			httpInvoke("creaMockServer.zdev", {name:filename}, function() {
				thisController.refrescaArchivos(null, app.path + "/_zmocks/" + filename, true);
			});
		}});
	},
	addFolder:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		openDialog("dev/w-get-file-name", {tipo:"folder", etiqueta:"Nombre Directorio", titulo:"Crear subdirectorio", onOk:function(filename) {
			httpInvoke("creaFolder.zdev", {path:path, name:filename}, function() {
				thisController.refrescaArchivos(null, path + "/" + filename, true);
			});
		}});
	},	
	renombrar:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		if (n.tipo == "folder") {
			if (this.hayArchivosAbiertos()) {
				openDialog("common/w-error", {message:"Debe cerrar todos los archivos abiertos antes de renombrar un directorio"});
				return;
			}
		} else {
			if (this.estaArchivoAbierto(path)) {
				openDialog("common/w-error", {message:"Debe cerrar el archivo antes de renombrarlo"});
				return;
			}
		}
		var nombre = path.substring(path.lastIndexOf("/") + 1);
		var dir = path.substring(0, path.lastIndexOf("/"));
		var ext = nombre.lastIndexOf(".") >= 0?nombre.substring(nombre.lastIndexOf(".") + 1):"control";
		openDialog("dev/w-get-file-name", {tipo:ext, oldFileName:nombre, titulo:"Renombrar", onOk:function(filename) {
			var newPath = dir + "/" + filename;
			if (ext == "control" && n.tipo != "folder") {
				httpInvoke("renameControl.zdev", {path:path, newPath:newPath}, function() {
					thisController.refrescaArchivos(null, newPath, false);
				});
			} else {
				httpInvoke("renameFile.zdev", {path:path, newPath:newPath}, function() {
					thisController.refrescaArchivos(null, newPath, false);
				});
			}
		}});
	},
	eliminar:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		if (n.tipo == "folder") {
			if (this.hayArchivosAbiertos()) {
				openDialog("common/w-error", {message:"Debe cerrar todos los archivos abiertos antes de eliminar un directorio"});
				return;
			}			
		} else {
			if (this.estaArchivoAbierto(path)) {
				openDialog("common/w-error", {message:"Debe cerrar el archivo antes de eliminarlo"});
				return;
			}
		}
		var nombre = path.substring(path.lastIndexOf("/") + 1);
		var msg = "¿Confirma que desea eliminar el " + (n.tipo == "folder"?"directorio":(n.tipo=="control"?"control":"archivo")) + " '" + nombre + "'?";
		openDialog("common/w-confirm", {message:msg, onOk:function() {
			var servicio = "delete" + (n.tipo == "folder"?"Folder":(n.tipo=="control"?"Control":"File"));
			httpInvoke(servicio + ".zdev", {path:path}, function() {
				thisController.refrescaArchivos();
			});
		}});
	},
	ejecutaWizardMVC:function(wizard, path, nombre, options, onReady) {
		var thisController = this;
		var pathRelativo = path.substring(app.path.length + 1);
		var promesas = [];
		if (!wizard.usaPlantillas) {
			promesas.push(new Promise((onOk) => {		
				var url = "dev/mvc-wizards/" + wizard.codigo + "/wizard.js";
				jQuery.get(url, function(data) {
					eval(data);
					var controles = run(options, nombre, path, pathRelativo);
					controles.forEach((c) => {						
						httpInvoke("creaControl.zdev", {path:path + "/" + c.nombreArchivo, html:c.html, js:c.js}, function() {
							onOk();
						});						
					});
				});
			}));
		} else {
			wizard.usaPlantillas.forEach((p) => {
				promesas.push(new Promise((onOk) => {
					var variables = {nombre:nombre, pathRelativo:pathRelativo};			
					variables.nombreArchivo = thisController.reemplazaVariables(p.nombreArchivo, variables);
					var url = "dev/mvc-wizards/" + wizard.codigo + "/" + p.plantilla + ".js";
					jQuery.get(url, function(data) {					
						var js = thisController.reemplazaVariables(data, variables);
						url = "dev/mvc-wizards/" + wizard.codigo + "/" + p.plantilla + ".html";
						jQuery.get(url, function(data) {
							var html = thisController.reemplazaVariables(data, variables);
							httpInvoke("creaControl.zdev", {path:path + "/" + variables.nombreArchivo, html:html, js:js}, function() {
								onOk();
							});
						});
					});						
				}));
			});			
		}
		Promise.all(promesas).then(() => onReady());
	},
	reemplazaVariables:function(codigo, variables) {
		var txt = codigo + "";	
		var keys = Object.keys(variables);
		for (var i=0; i<keys.length; i++) {
			var v = keys[i];
			txt = txt.split("${" + v + "}").join(variables[v]);
		}
		return txt;
	},
	addControl:function() {
		var thisController = this;
		var n = this.getNodoSeleccionado();
		if (!n) return;
		var path = n.path;
		openDialog("dev/w-mvc-wizard", {onOk:function(wizard, nombre) {
			if (wizard.tieneVentana) {
				openDialog("dev/mvc-wizards/" + wizard.codigo + "/w-" + wizard.codigo, {nombre:nombre, path:path, onOk:function(options) {
					thisController.ejecutaWizardMVC(wizard, path, nombre, options, function() {
						thisController.refrescaArchivos(null, path + "/" + nombre, true);
					});
				}});
			} else {
				thisController.ejecutaWizardMVC(wizard, path, nombre, {}, function() {
					thisController.refrescaArchivos(null, path + "/" + nombre, true);
				});
			}
		}});
	}
}