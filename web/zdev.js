var zDBG = {id:"?"}
if (window.addEventListener) window.addEventListener("message", _zdbgEventListener, false);
else attachEvent("onmessage", _zdbgEventListener);
_zdbgSendMessage("loaded");

function zInitTest(controlId) {
	var controlURL = $(controlId).attr("data-z-autoload");
	var controlName = controlURL;
	var p = controlName.lastIndexOf("/");
	if (p>=0) controlName = controlName.substring(p+1);
	if (controlName.startsWith("w")) {
		var opciones = zDialogOptions?zDialogOptions:{};
		openDialog(controlURL, opciones, function() {
			for (var i=0; i<arguments.length; i++) {
				var a = arguments[i];
				console.log("resultado de close: " + i);
				console.log(a);
			}
		}, function() {
			console.log("dialogo cancelado");
		}, function() {
			console.log("dialogo cerrado");
		}, function(controller) {
			zDBG.controller = controller;
		});
	} else {
		$(controlId).zMVC({
			onReady:function(controller) {
				controller.trigger = function() {
					var args = [];
					var eventName = null;
					for (var i=0; i<arguments.length; i++) {
						if (i==0) eventName = arguments[0];
						else {
							var a = arguments[i];
							if (typeof a == "object") {
								a = JSON.stringify(a);
							}
							args.push(a);
						}
					}
					_zdbgSendMessage("evento", {nombre:eventName, argumentos:args});
				};
				zDBG.controller = controller;
			}
		});
	}
}

function _zdbgSendMessage(action, data) {
	if (parent) {
		try {
			parent.postMessage({id:zDBG.id, action:action, data:data}, "*");
		} catch(error) {
			zDBG.oldConsole.log("No se puede enviar mensaje '" + action + "' con data ", data);
			try {
				data = JSON.stringify(data);
			} catch(error2) {
				data = "" + data;
			}
			zDBG.oldConsole.log("reintentando con data ", "" + data);
			parent.postMessage({id:zDBG.id, action:action, data:data}, "*");
		}
	} else {
		console.log("no hay parent en iframe. No se puede enviar mensaje");
	}
}	
function _zdbgEventListener(e) {
	var action = e.data.action;
	var data = e.data.data;
	if (action == "setId") {
		zDBG.id = data;
		if (!zDBG.oldConsole) zDBG.oldConsole = console;
		console = {				
			log:function() {
				for (var i=0; i<arguments.length; i++) {
					var a = arguments[i];
					if (typeof(a) == "object") {
						try {
							a = JSON.stringify(a);
						} catch(error) {
							a = "" +a;
						}
					}
					_zdbgSendMessage("log", a);
				}
			}	
		};
	} else if (action == "ejecuta") {
		var controller = zDBG.controller;
		var r = eval(data);
		console.log(r);
	}
}