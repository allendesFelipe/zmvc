_zmvcRegisterPreprocessor("button", function(element, controller) {
	element.click(function() {
		controller._processEvent(element.prop("id"), "click");
	});
});
_zmvcRegisterPreprocessor("a", function(element, controller) {
	element.click(function() {
		controller._processEvent(element.prop("id"), "click");
	});
});
_zmvcRegisterPreprocessor("select", function(element, controller) {
	element.change(function() {
		controller._processEvent(element.prop("id"), "change");
	});
});
_zmvcRegisterPreprocessor("input", function(element, controller) {
	element.change(function() {
		controller._processEvent(element.prop("id"), "change");
	});
	element.keyup(function(e) {
		controller._processEvent(element.prop("id"), "keyup", e);
	});
	element.keydown(function(e) {
		controller._processEvent(element.prop("id"), "keydown", e);
	});
	element.keypress(function(e) {
		controller._processEvent(element.prop("id"), "keypress", e);
	});
});