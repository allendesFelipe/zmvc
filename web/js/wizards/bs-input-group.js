registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"bs-input-group",
	nombre:"BS input-group imagen/input",
	grupo:"Bootstrap Formularios",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {
		var html = '<div class="input-group input-group-sm col mb-3">\n';
		html    += '\t<div class="input-group-prepend">\n';
		html    += '\t\t<span class="input-group-text"><i class="far fa-address-card fa-lg"></i></span>\n';
		html    += '\t</div>\n';
		html    += '\t<input id="edCampo" type="text" class="form-control" placeholder="Campo" />\n';
		html    += '</div>';
		return html;
	}
}));