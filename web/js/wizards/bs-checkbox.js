registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"bs-checkbox",
	nombre:"BS form-check input/label",
	grupo:"Bootstrap Formularios",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {
		var html = '<div class="col form-group">\n';
		html    += '\t<div class="form-check">\n';
		html    += '\t\t<input id="edCampo" type="checkbox" class="form-check-input"  />\n';
		html    += '\t\t<label for="edCampo" class="form-check-label">Campo</label>\n';
		html    += '\t</div>';
		html    += '</div>';
		return html;
	}
}));