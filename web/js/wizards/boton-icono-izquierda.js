registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"boton-icono-derecha",
	nombre:"Botón con Ícono -> Derecha",
	grupo:"Font Awsome",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {return '<button id="cmdComando" type="button" class="btn btn-primary">Etiqueta <i class="fa-bug fas fa-sm"></i></button>';}
}));