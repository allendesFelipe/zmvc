registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"boton-badge",
	nombre:"Botón con badge",
	grupo:"Bootstrap",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {return '<button id="cmdComando" type="button" class="btn btn-primary">Etiqueta <span id="badge" class="badge badge-warning">0</span></button>';}
}));