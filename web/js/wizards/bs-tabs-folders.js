registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"bs-tabs-folders",
	nombre:"Tabs Folders",
	grupo:"Bootstrap",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {
		var html = "";
		html += '<ul class="nav nav-tabs" id="tabs" role="tablist">\n';
		html += '\t\t<li class="nav-item">\n'
		html += '\t\t\t<a class="nav-link active" data-toggle="tab" href="#pagina-1" role="tab">Página 1</a>\n';
		html += '\t\t</li>\n'
		html += '\t</ul>';
		return html;
	}
}));