registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"z-pager",
	nombre:"Z-MVC Pager",
	grupo:"Z-MVC",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {return "<div id='pager' data-z-pages='[]' data-z-animation-delay='300'></div>";},	
}));