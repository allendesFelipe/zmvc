registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"boton-icono-izquierda",
	nombre:"Botón con Ícono -> Izquierda",
	grupo:"Font Awsome",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {return '<button id="cmdComando" type="button" class="btn btn-primary"><i class="fa-bug fas fa-sm"></i> Etiqueta</button>';}
}));