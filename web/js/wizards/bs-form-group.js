registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"bs-form-group",
	nombre:"BS form-group label/input",
	grupo:"Bootstrap Formularios",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {
		var html = '<div class="col form-group">\n';
		html    += '\t<label for="edCampo">Campo</label>\n';
		html    += '\t<input id="edCampo" type="text" class="form-control form-control-sm" placeholder="nombre del campo" />\n';
		html    += '</div>';
		return html;
	}
}));