registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"fa-icono",
	nombre:"Ícono Font-Awsome 5",
	grupo:"Font Awsome",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {return '<i class="fa-bug fa-lg fas"></i>';}
}));