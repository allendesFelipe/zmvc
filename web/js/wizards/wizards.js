// Wizards
var defWizards = {}

var WizardAbstracto = {
	codigo:"?",
	nombre:"??",
	grupo:"HTML Básicos",
	getPosiblesPadres:function() {return null;}, // Sin filtro
	nombreVentana:null,
	getHTML:function() {return "<div>?</div>";}
}

function registraWizard(w) {
	defWizards[w.codigo] = w;
}

function getWizard(codigo) {
	return defWizards[codigo];
}

function getWizards() {
	var keys = Object.keys(defWizards);
	var wizards = [];
	for (var i=0; i<keys.length; i++) {
		var codigo = keys[i];
		wizards.push(defWizards[codigo]);
	}
	return wizards;
}

function getPosiblesHijosDe(codigo) {
	var wz = [];
	getWizards().forEach((w) => {
		var posiblesPadres = w.getPosiblesPadres();
		if (!posiblesPadres || posiblesPadres.indexOf(codigo) >= 0) wz.push(w);
	});
	var grupos = {};
	wz.forEach((w) => {
		var grupo = grupos[w.grupo];
		if (!grupo) {
			grupo = [];
			grupos[w.grupo] = grupo;
		}
		grupo.push(w);
	});
	var arbol = [];
	var keys = Object.keys(grupos);
	for (var i=0; i<keys.length; i++) {
		var grupo = keys[i];
		arbol.push({grupo:grupo, wizards:grupos[grupo]});
	}
	return arbol;
}
