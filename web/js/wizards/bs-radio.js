registraWizard($.extend(true, {}, WizardAbstracto, {
	codigo:"bs-radio",
	nombre:"BS form-check radio-button",
	grupo:"Bootstrap Formularios",
	getPosiblesPadres:function() {return null;},
	nombreVentana:null,
	getHTML:function() {
		var html = '<div class="col form-group">\n';
		html    += '\t<div class="form-check">\n';
		html    += '\t\t<input id="edCampo" type="radio" name="grupoDeRadios" class="form-check-input"  />\n';
		html    += '\t\t<label for="edCampo" class="form-check-label">Campo</label>\n';
		html    += '\t</div>';
		html    += '</div>';
		return html;
	}
}));