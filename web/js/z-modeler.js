var RESIZE_SENSIBILITY = 4;
var NORTH = 0;
var EAST = 1;
var SOUTH = 2;
var WEST = 3;
var LINESEL_SENSIBILITY = 5;

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function Model() {
	return {
		id:guid(),
		type:"model",
		width:640,
		height:480,
		zoom:100,
		backgroundColor:"white",
		name:"New Model",
		elements:{},
		lines:{}
	};
}

function Element() {
	return {
		id:guid(),
		type:"element",
		x:0,
		y:0,
		width:100,
		height:100,
		resizable:true,
		minWidth:20,
		minHeight:20
	};
}

function ElementHandler() {
	return {
		paint:function(element, context, modeler) {
	    	context.save();
	    	context.lineWidth = 1;
	    	context.strokeStyle = "black";
	    	context.beginPath();
	    	context.rect(modeler.toCanvas(element.x), modeler.toCanvas(element.y), modeler.toCanvas(element.width), modeler.toCanvas(element.height));
	    	context.stroke();
	    	context.restore();			
		},
		editProperties:function(element, modeler) {			
		},
		cloneForCopy:function(element) {
			return $.extend(true, {}, element);
		},
		canStartLine:function(element) {return true;}
	};
}

function Line(sourceId, targetId) {
	return {
		id:guid(),
		type:"line",
		points:[],
		srcId:sourceId,
		trgId:targetId,
		srcBorder:WEST,
		srcDelta:10, 
		trgBorder:EAST,
		trgDelta:10,
		label:""
	};
}

function LineHandler() {
	return {
		paint:function(line, context, modeler) {
	    	context.save();
	    	context.beginPath();
	    	context.lineWidth = 1;
	    	context.strokeStyle = "black";
	    	var points = modeler.getLinePoints(line);
	    	for (var i=0; i<points.length; i++) {
	    		var p = points[i];
	    		var x = modeler.toCanvas(p.x);
	    		var y = modeler.toCanvas(p.y);
	    		if (i == 0) context.moveTo(x, y);
	    		else context.lineTo(x, y);
	    	}
	    	context.stroke();    	

	    	// Arrow
	    	var x1 = modeler.toCanvas(points[points.length-1].x);
	    	var y1 = modeler.toCanvas(points[points.length-1].y);
	    	var x2 = modeler.toCanvas(points[points.length-2].x);
	    	var y2 = modeler.toCanvas(points[points.length-2].y);
	    	var x3, y3, x4, y4, x5, y5, xm, ym;
	    	var d = 20 * modeler.zoom() / 100;
	    	var dmax = Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
	    	var O, v;
	    	
	    	if (dmax < d) d = dmax;
			if (d > 0) {
				if (x1 == x2) {
					if (y2 > y1) { 
						O = Math.PI / 2.0;
					} else {
						O = -Math.PI / 2.0; 
					}
				} else {
					v = (y2 - y1) / (x2 - x1);
					O = Math.atan(v);
					if (x2 < x1) {
						O = Math.PI + O; 
					}
				}
				xm = x1 + d / 2.0 * Math.cos(O);
				ym = y1 + d / 2.0 * Math.sin(O);
				x4 = xm + d / 4.0 * Math.cos(O + Math.PI / 2.0);
				y4 = ym + d / 4.0 * Math.sin(O + Math.PI / 2.0);
				x5 = xm + d / 4.0 * Math.cos(O - Math.PI / 2.0);
				y5 = ym + d / 4.0 * Math.sin(O - Math.PI / 2.0);
			}
			context.shadowOffsetX = 3*modeler.zoom()/100;
	    	context.shadowOffsetY = 3*modeler.zoom()/100;
	    	context.shadowBlur    = 3;
	    	context.shadowColor   = 'rgba(10, 10, 40, 0.5)';   
			context.lineStyle = "rgb(0,0,0)";
			context.lineWidth = 1;
			context.fillStyle = "rgb(100,100,200)";
			context.beginPath();
			context.moveTo(x1, y1);
			context.lineTo(x4, y4);
			context.lineTo(x5, y5);
			context.closePath();
			context.stroke();
			context.fill();
	    	context.restore();
	    	
	    	// Line Text
	    	if (line.label) {
		    	context.save();
		    	var fontSize = 10 * modeler.zoom() / 100;
		    	var fontStyle = fontSize + "px Arial";
		    	context.fillStyle = "black";
				context.textAlign = "left";
				context.textBaseline = "top";
				context.font = fontStyle;
				var x0 = modeler.toCanvas(points[0].x);
				var y0 = modeler.toCanvas(points[0].y);
				var tx, ty;
				switch(line.srcBorder) {
					case NORTH:
						tx = x0 + fontSize / 2;
						ty = y0 - fontSize * 3 / 2;
						break;
					case SOUTH:
						tx = x0 + fontSize / 2;
						ty = y0 + fontSize / 2;
						break;
					case EAST:
						tx = x0 + fontSize / 2;
						ty = y0 + fontSize / 2;
						break;
					case WEST:
						tx = x0 - context.measureText(line.label).width - fontSize / 2;
						ty = y0 + fontSize / 2;
						break;
				}			
				context.fillText(line.label, tx, ty);
		    	context.restore();
	    	}			
		},
		editProperties:function(line, modeler) {
		},
		cloneForCopy:function(line) {
			return $.extend(true, {}, line);
		}		
	};
}


$.widget("z.zModeler", {
	// editing vars
	sizing:false,
	moving:false,
	canMove:false,
	movingLinePoint:false,
	selectedRectsIds:[],
	mostSelectedRectId:null,
	selectedLinesIds:[],
	mostSelectedLineId:null,
	selectedLinePoints:null,
	linePointIndex0:-1,
	linePointIndex1:-1,
	mostSelectedPoint:-1,
	movingLinePoint:false,
	shouldUnselect:false,
	selectingGroup:false,
	moveX0:-1,
	moveY0:-1,
	showingMoveCursor:false,
	sizeDirection:-1,
	moveMinX:0,
	moveMaxX:0,
	moveMinY:0,
	moveMaxY:0,
	addLineTargetPoint:null,
	changed:false,
	originalRectResize:null,
	oldModelWidth:0,
	oldModelHeight:0,
	
	// Undo Manager
	MAX_DIFFS:200,
	diffs:null,
	redoDiffs:null,
	objA:null,
	
	// Handlers
	defaultElementHandler:new ElementHandler(),
	defaultLineHandler:new LineHandler(),
	
	// Widget Options
	options:{
		model:null,
		elementsHandlers:{},
		linesHandlers:{},
		onLineAddRequest:null,
		onDeleteRequest:null,
		onCopyRequest:null,
		onCutRequest:null,
		onPasteRequest:null,
		onUndoBufferChange:null,
		onSelectionChange:null,
		readOnly:false,
		customEditors:{},
		editListener:null
	},
	_create:function() {
		var thisWidget = this;
		this.diffs = new Array();
		this.redoDiffs = new Array();
		var $canvas = $(this.element);
		var canvas = this.element[0];
		var model = this.options.model;
		canvas.onmousemove = function(e) {thisWidget._onMouseMove(e);};
		canvas.onmousedown = function(e) {thisWidget._onMouseDown(e);};
		canvas.onmouseup = function(e) {thisWidget._onMouseUp(e);};
		canvas.addEventListener("dblclick", function(e) {thisWidget._editSelection();});
		canvas.addEventListener("keydown", function(e) {thisWidget._onKeyDown(e);});
		$canvas.css('background-color', model.backgroundColor);
		canvas.width = this._toCanvas(model.width);
		canvas.height = this._toCanvas(model.height);		
		this.paint();
	},
	
	// Path to work with a reference, not a copy
	setModelReference:function(model) {
		this.options.model = model;
	},
	
	// Custom Editors
	registerCustomEditor:function(type, callback) {
		this.options.customEditors[type] = callback;
	},
	
	// Trigger Events
	_triggerUndoBufferChange:function() {
		if (this.options.onUndoBufferChange) this.options.onUndoBufferChange();
	},
	_triggerSelectionChange:function() {
		if (this.options.onSelectionChange) this.options.onSelectionChange();
	},
	
	// Private functions
	_toCanvas:function(x) {return x * this.options.model.zoom / 100;},   
    _toModel:function(x) {return x * 100 / this.options.model.zoom;},
    _relativeEventCoordsToPoint:function (event){
        var totalOffsetX = 0;
        var totalOffsetY = 0;
        var x = 0;
        var y = 0;
        var currentElement = this.element[0];
        var canvasContainer = ($(this.element).parent())[0];
        do{
            totalOffsetX += currentElement.offsetLeft;
            totalOffsetY += currentElement.offsetTop;
        }
        while(currentElement = currentElement.offsetParent);
        x = event.pageX - totalOffsetX + canvasContainer.scrollLeft;
        y = event.pageY - totalOffsetY + canvasContainer.scrollTop;
        return {x:x, y:y};
    },
    _getBorderPoint:function(element, border, delta) {
		switch(border) {
			case NORTH:	return {x:element.x + delta, y:element.y};
			case EAST:	return {x:element.x + element.width, y:element.y + delta};
			case SOUTH:	return {x:element.x + delta, y:element.y + element.height};
			case WEST:	return {x:element.x ,y:element.y + delta};
		}
		return null;
	},
    _getAllLinePoints:function(line) {
    	var model = this.options.model;
    	var points = [];
    	var srcElement = model.elements[line.srcId];
    	if (!srcElement) $.error("Source line element:" + line.srcId + " not found");
    	points.push(this._getBorderPoint(srcElement, line.srcBorder, line.srcDelta));
    	for (var i=0; i<line.points.length; i++) {
    		points.push({x:line.points[i].x, y:line.points[i].y});
    	}
    	var dstElement = model.elements[line.trgId];
    	if (!dstElement) $.error("Destination line element:" + line.trgId + " not found");
    	points.push(this._getBorderPoint(dstElement, line.trgBorder, line.trgDelta));
    	return points;
    },
    _isRectSelected:function(id) {
		return $.inArray(id, this.selectedRectsIds) >= 0;
	},
	_isLineSelected:function(id) {
		return $.inArray(id, this.selectedLinesIds) >= 0;
	},
	_unselectAll:function() {
		this.selectedRectsIds = [];
		this.mostSelectedRectId = null;
		this.selectedLinesIds = [];
		this.mostSelectedLineId = null;
		this.selectedLinePoints = null;
		this._triggerSelectionChange();
	},
	_selectRect:function(r, silent) {
		this.selectedRectsIds.push(r.id);
		this.mostSelectedRectId = r.id;
		this.mostSelectedLineId = null;
		if (!silent) this._triggerSelectionChange();
	},
	_selectLine:function(l, silent) {
		this.selectedLinesIds.push(l.id);
		this.mostSelectedLineId = l.id;
		this.selectedLinePoints = this._getAllLinePoints(l);
		this.mostSelectedPoint = -1;
		this.mostSelectedRectId = null;
		if (!silent) this._triggerSelectionChange();
	},
	_unselectRect:function(r) {
		var p = $.inArray(r.id, this.selectedRectsIds);
		if (p < 0) {
			return;
		}
		this.selectedRectsIds.splice(p, 1);
		if (r.id == this.mostSelectedRectId) {
			if (this.selectedRectsIds.length == 0) {
				this.mostSelectedRectId = null;
			} else {
				this.mostSelectedRectId = this.selectedRectsIds[this.selectedRectsIds.length - 1];
			}
		}
		this._triggerSelectionChange();
	},
	_unselectLine:function(l) {
		var p = $.inArray(l.id, this.selectedLinesIds);
		if (p < 0) {
			return;
		}
		this.selectedLinesIds.splice(p, 1);
		if (l.id == this.mostSelectedLineId) {
			if (this.selectedLinesIds.length == 0) {
				this.mostSelectedLineId = null;
				this.selectedLinePoints = null;
			}
		}
		this._triggerSelectionChange();
	},
	_showMoveCursor:function(direction) {
    	var cursor = "move";
    	switch(direction) {
    		case 0:cursor = "N-resize";break;
    		case 1:cursor = "NE-resize";break;
    		case 2:cursor = "E-resize";break;
    		case 3:cursor = "SE-resize";break;
    		case 4:cursor = "S-resize";break;
    		case 5:cursor = "SW-resize";break;
    		case 6:cursor = "W-resize";break;
    		case 7:cursor = "NW-resize";break;
    		case 8:cursor = "pointer";break;
    		case 9:cursor = "crosshair";break;
    	}
    	this.showingMoveCursor = true;
    	(this.element)[0].style.cursor = cursor;
    },
    _hideMoveCursor:function() {
    	this.showingMoveCursor = false;
    	(this.element)[0].style.cursor = "default";
    },
    _getLineAtPoint:function(p) {
    	var thisWidget = this;
    	var xm = p.x, ym = p.y;
    	var found = null;
    	$.each(this.options.model.lines, function(id, line) {    		
    		var points = thisWidget._getAllLinePoints(line);
    		for (var j=0; j<points.length - 1; j++) {
    			thisWidget.linePointIndex0 = j;
    			thisWidget.linePointIndex1 = j+1;
    			var p0 = points[j];
    			var p1 = points[j+1];
    			var x0 = p0.x, y0 = p0.y;
    			var x1 = p1.x, y1 = p1.y;
    			var dx = x1 - x0;
    			var dy = y1 - y0;
    			var lineSelSens = LINESEL_SENSIBILITY * thisWidget.options.model.zoom / 100;
    			var m;
    			if (Math.abs(dx) < 2) {
					if (((ym >= y0 && ym <= y1) || (ym >= y1 && ym <= y0)) && Math.abs(xm - x0) < lineSelSens) {
						found = line;
						return false;
					}
				} else if (Math.abs(dy) < 2) {
					if (((xm >= x0 && xm <= x1) || (xm >= x1 && xm <= x0)) && Math.abs(ym - y0) < lineSelSens) {
						found = line;
						return false;
					}
				} else {
					m = dy / dx;
					x = xm;
					y = y0 + (x - x0) * m;
					if (((x >= x0 && x <= x1) || (x >= x1 && x <= x0)) && Math.abs(y - ym) < lineSelSens) {
						found = line;
						return false;
					}
					y = ym;
					x = x0 + (y - y0) / m;
					if (((y >= y0 && y <= y1) || (y >= y1 && y <= y0)) && Math.abs(x - xm) < lineSelSens) {
						found = line;
						return false;
					}
				}
    		}
    	});
    	return found;
    },
    _getElementAtPoint:function(p) {
    	var found = null;
    	$.each(this.options.model.elements, function(id, n) {
    		if (p.x >= n.x && (p.x - n.x) <= n.width && p.y >= n.y && (p.y - n.y) <= n.height) {
    			found = n;
    			//return false;    		    		
    		}
    	});
    	return found;
    },
    _calculateMovingLimits:function() {
		for (var i=0; i<this.selectedRectsIds.length; i++) {
			var n = this.options.model.elements[this.selectedRectsIds[i]];
			if (i == 0 || n.x < this.moveMinX) this.moveMinX = n.x;
			if (i == 0 || n.x + n.width > this.moveMaxX) this.moveMaxX = n.x + n.width;
			if (i == 0 || n.y < this.moveMinY) this.moveMinY = n.y;
			if (i == 0 || n.y + n.height > this.moveMaxY) this.moveMaxY = n.y + n.height;
			this.oldModelWidth = this.options.model.width;
			this.oldModelHeight = this.options.model.height;
		} 	
    },
    _getClosestBorderPoint:function(node, x, y) {
		var dn = Math.abs(node.y - y);
		var ds = Math.abs(node.y + node.height - y);
		var de = Math.abs(node.x + node.width - x);
		var dw = Math.abs(node.x - x);
		
		var minBorder = NORTH;
		if (ds < dn || de < dn || dw < dn) {
			minBorder = SOUTH;
			if (de < ds || dw < ds) {
				minBorder = EAST;
				if (dw < de) {
					minBorder = WEST;
				}
			}
		}
		var border = minBorder;
		var delta;
		switch(border) {
			case NORTH:
			case SOUTH:
				delta = x - node.x;
				if (delta < 0) delta = 0;
				if (delta > node.width) delta = node.width;
				break;
			case EAST:
			case WEST:
				delta = y - node.y;
				if (delta < 0) delta = 0;
				if (delta > node.height) delta = node.height;
				break;
		}
		return {border:border, delta:delta};
	},
	_fixAdjacentLines:function(node) {
    	var lines = this._getLinesWithSource(node.id);
		for (var i=0; i<lines.length; i++) {
			var line = lines[i];
			if ((line.srcBorder == NORTH || line.srcBorder == SOUTH) && line.srcDelta > node.width) {
				line.srcDelta = node.width;
			} 
			if ((line.srcBorder == EAST || line.srcBorder == WEST) && line.srcDelta > node.height) {
				line.srcDelta = node.height;
			} 
		}
		lines = this._getLinesWithTarget(node.id);
		for (var i=0; i<lines.length; i++) {
			line = lines[i];
			if ((line.trgBorder == NORTH || line.trgBorder == SOUTH) && line.trgDelta > node.width) {
				line.trgDelta = node.width;
			} 
			if ((line.trgBorder == EAST || line.trgBorder == WEST) && line.trgDelta > node.height) {
				line.trgDelta = node.height;
			} 
		}    
    },
    _intersectBorder:function (node, x0, y0, x1, y1) {
		// x0,y0 should be the closest point
		var dx = x1 - x0;
		var dy = y1 - y0;
		if (Math.abs(dx) < 2) {
			// Vertical
			if (y1 > y0) return {border:SOUTH, delta:(x0 - node.x)};
			else return {border:NORTH, delta:(x0 - node.x)};
		}
		if (Math.abs(dy) < 2) {
			// Horizontal
			if (x1 > x0) return {border:EAST, delta:(y0 - node.y)};
			else return {border:WEST, delta:(y0 - node.y)};
		}
		var m = dy/dx;
		var x,y;
		// Test EAST
		x = node.x + node.width;
		if (x >= x0 && x <= x1) { 
			y = y0 + m*(x-x0);
			if (y >= node.y && y <= node.y + node.height) return {border:EAST, delta:(y - node.y)};
		}
		// Test WEST
		x = node.x;
		if (x >= x1 && x <= x0) { 
			y = y0 + m*(x-x0);
			if (y >= node.y && y <= node.y + node.height) return {border:WEST, delta:(y - node.y)};
		}
		// Test NORTH
		y = node.y;
		if (y >= y1 && y <= y0) {
			x = x0 + (y-y0) / m;
			if (x >= node.x && x <= node.x + node.width) return {border:NORTH, delta:(x - node.x)};
		}
		// Test SOUTH
		y = node.y + node.height;
		if (y >= y0 && y <= y1) {
			x = x0 + (y-y0) / m;
			if (x >= node.x && x <= node.x + node.width) return {border:SOUTH, delta:(x - node.x)};
		}
		// Really don't know
		return {border:0, delta:10};
	},
    _finishMovingLinePoint:function() {
    	var l = this.options.model.lines[this.mostSelectedLineId];
    	this._updateAllLinePoints(l, this.selectedLinePoints);
    	this.selectedLinePoints = this._getAllLinePoints(l);
		this.mostSelectedPoint = -1;
    },
    _finishSelectingGroup:function(mx, my) {
    	var thisWidget = this;
    	var x0, y0, x1, y1
		if (mx > this.moveX0) {
			x0 = this.moveX0;
			x1 = mx;
		} else {
			x0 = mx;
			x1 = this.moveX0;				
		} 
		if (my > this.moveY0) {
			y0 = this.moveY0;
			y1 = my;
		} else {
			y0 = my;
			y1 = this.moveY0;				
		}
		$.each(this.options.model.lines, function(id, l) {
			if (!thisWidget._isLineSelected(l.id)) {
				var points = thisWidget._getAllLinePoints(l);
				var included = true;
				for (var j=0; j<points.length; j++) {
					p = points[j];
					if (p.x < x0 || p.x > x1 || p.y < y0 || p.y > y1) {
						included = false;
						break;
					}
				}
				if (included) thisWidget._selectLine(l);
			}			
		});
		$.each(this.options.model.elements, function(id, r) {
			if (!thisWidget._isRectSelected(r.id)) {
				if (r.x >= x0 && r.x + r.width <= x1 && r.y >= y0 && r.y + r.height <= y1) {
					thisWidget._selectRect(r);
				}
			}
		});
		this.selectingGroup = false;
    },
    _getLinesWithSource:function(id) {
    	var ret = [];
    	$.each(this.options.model.lines, function(lineId, l) {
    		if (l.srcId == id) ret.push(l);
    	});
    	return ret;
    },
    _getLinesWithTarget:function(id) {
    	var ret = [];
    	$.each(this.options.model.lines, function(lineId, l) {
    		if (l.trgId == id) ret.push(l);
    	});
    	return ret;
    },
    _updateAllLinePoints:function(l, pts) {
    	var srcRect = this.options.model.elements[l.srcId];
		var tgtRect = this.options.model.elements[l.trgId];
		var srcBorder = this._getClosestBorderPoint(srcRect, pts[0].x, pts[0].y);
		var tgtBorder = this._getClosestBorderPoint(tgtRect, pts[pts.length - 1].x, pts[pts.length - 1].y);
		l.srcBorder = srcBorder.border;
		l.srcDelta = srcBorder.delta;
		l.trgBorder = tgtBorder.border;
		l.trgDelta = tgtBorder.delta;
		
		// Adjust first and last point to calculate auto removals
		var points = [];
		for (var i=0; i<pts.length; i++) {
			var p = pts[i];
			if (i == 0) {
				p = this._getBorderPoint(srcRect, l.srcBorder, l.srcDelta);
			} else if (i == pts.length - 1) {
				p = this._getBorderPoint(tgtRect, l.trgBorder, l.trgDelta);
			}
			points.push(p);
		}
		var pointIndexToDelete = -1;
		for (var i=1; i<points.length - 1; i++) {
			var p0 = points[i-1];
			var p1 = points[i];
			var p2 = points[i+1];
			
			if (((p1.x >= p0.x && p1.x <= p2.x) || (p1.x >= p2.x && p1.x <= p0.x)) && ((p1.y >= p0.y && p1.y <= p2.y) || (p1.y >= p2.y && p1.y <= p0.y))) { 
				var dx = p2.x - p0.x;
				var dy = p2.y - p0.y;
				if (Math.abs(dx) < 2 || Math.abs(dy) < 2) {
					pointIndexToDelete = i;
					break;
				} 
				var m = dy / dx;
				var x;
				var y;
				x = p1.x;
				y = p0.y + (x - p0.x) * m;
				if (Math.abs(p1.y - y) < 3) {
					pointIndexToDelete = i;
					break;
				}
				y = p1.y;
				x = p0.x + (y - p0.y) / m;
				if (Math.abs(p1.x - x) < 3) {
					pointIndexToDelete = i;
					break;
				}
			}
		}
		if (pointIndexToDelete != -1) {
			points.splice(pointIndexToDelete, 1);
		}
		var newPoints = [];
		if (points.length > 2) {
			for (i=1; i<points.length - 1; i++) {
				p = points[i];
				newPoints.push(p);
			}
		}
		l.points = newPoints;
    },    
    _getElementHandler:function(element) {
    	var h = this.options.elementsHandlers[element.type];
    	if (!h) {
    		return this.defaultElementHandler;
    	}
    	return h;
    },
    _getLineHandler:function(line) {
    	var h = this.options.linesHandlers[line.type];
    	if (!h) return this.defaultLineHandler;
    	return h;
    },
	
    // Editing painting functions
    _paintElement:function(element, context) {
    	this._getElementHandler(element).paint(element, context, this);
    	// Paint extra if most selected
    	if (element.id == this.mostSelectedRectId && !this.readOnly()) {
	    	context.save();
	    	context.lineWidth = 1;
	    	context.strokeStyle = "blue";
	    	context.beginPath();
	    	context.rect(this._toCanvas(element.x), this._toCanvas(element.y), this._toCanvas(element.width), this._toCanvas(element.height));
	    	context.stroke();
	    	if (this._getElementHandler(element).canStartLine(element)) {
	    		context.beginPath();
	        	var x0 = this._toCanvas(element.x + element.width / 2);
	        	var y0 = this._toCanvas(element.y + element.height / 2);
	        	var r = this._toCanvas(3);
	        	context.arc(x0, y0, r, 0, 2 * Math.PI, false);
	        	context.fillStyle = "blue";
	        	context.fill();
	    	}
        	context.restore();
    	}
    },
    _paintLine:function(line, context) {
    	this._getLineHandler(line).paint(line, context, this);
    	if (this.mostSelectedLineId == line.id && !this.readOnly()) {
	    	context.save();
	    	context.beginPath();
	    	context.lineWidth = 2;
	    	context.strokeStyle = "blue";
	    	var points = this._getAllLinePoints(line);
	    	for (var i=0; i<points.length; i++) {
	    		var p = points[i];
	    		var x = this._toCanvas(p.x);
	    		var y = this._toCanvas(p.y);
	    		if (i == 0) context.moveTo(x, y);
	    		else context.lineTo(x, y);
	    	}
	    	context.stroke();    	
    		for (var i=0; i<points.length; i++) {
        		var p = points[i];
        		var x = this._toCanvas(p.x);
        		var y = this._toCanvas(p.y);        		        		
        		context.beginPath();
        		context.lineWidth = 1;
        		context.arc(x, y, 4 * this.options.model.zoom / 100, 0, 2 * Math.PI, false);
        		context.fillStyle = (i == this.mostSelectedPoint?"blue":"white");
        		context.fill();
        		context.strokeStyle = "blue";
        		context.stroke();
    		}
	    	context.restore();
    	}
    },
    _paintSelectedElement:function(element, context, deltaX, deltaY) {
    	if (!deltaX) deltaX = 0;
    	if (!deltaY) deltaY = 0;
    	context.save();
    	context.lineWidth = 1;
    	context.strokeStyle = "rgb(100,100,100)";
    	context.fillStyle = "rgba(150, 150, 170, 0.5)";
    	context.beginPath();
    	context.rect(this._toCanvas(element.x + deltaX), this._toCanvas(element.y + deltaY), this._toCanvas(element.width), this._toCanvas(element.height));
    	context.fillRect(this._toCanvas(element.x + deltaX), this._toCanvas(element.y + deltaY), this._toCanvas(element.width), this._toCanvas(element.height));
    	context.stroke();
    	context.restore();
    },
    _paintSelectedLine:function(line, context, deltaX, deltaY) {
    	if (!deltaX) deltaX = 0;
    	if (!deltaY) deltaY = 0;
    	context.save();
    	context.beginPath();
    	context.lineWidth = 3;
    	context.strokeStyle = "rgba(50, 50, 50, 0.5)";
    	var points = this._getAllLinePoints(line);
    	for (var i=0; i<points.length; i++) {
    		var p = points[i];
    		var x = this._toCanvas(p.x);
    		var y = this._toCanvas(p.y);
    		if (i == 0) {
    			if (this._isRectSelected(line.srcId)) {
    				x += this._toCanvas(deltaX);
    				y += this._toCanvas(deltaY);
    			}
    		} else if (i == points.length - 1) {
    			if (this._isRectSelected(line.trgId)) {
    				x += this._toCanvas(deltaX);
    				y += this._toCanvas(deltaY);
    			}
    		} else {
    			x += this._toCanvas(deltaX);
				y += this._toCanvas(deltaY);
    		}
    		if (i == 0) context.moveTo(x, y);
    		else context.lineTo(x, y);
    	}
    	context.stroke(); 
    	context.restore();
    },
    _paintMovingLinePoint:function(p) {
    	var context = (this.element)[0].getContext("2d");
    	var mouseX = p.x;
		var mouseY = p.y;
		// Adjust to adyacent points by horizontal or vertical coordinates
		if (this.mostSelectedPoint > 0) {
			var p0 = this.selectedLinePoints[this.mostSelectedPoint-1];
			if (Math.abs(p0.x - mouseX) < 4) mouseX = p0.x;
			if (Math.abs(p0.y - mouseY) < 4) mouseY = p0.y;
		}
		if (this.mostSelectedPoint < this.selectedLinePoints.length - 1) {
			var p0 = this.selectedLinePoints[this.mostSelectedPoint+1];
			if (Math.abs(p0.x - mouseX) < 4) mouseX = p0.x;
			if (Math.abs(p0.y - mouseY) < 4) mouseY = p0.y;
		} 
		var l = this.options.model.lines[this.mostSelectedLineId];
		if (this.mostSelectedPoint == 0) {
			var srcRect = this.options.model.elements[l.srcId];
			var border = this._getClosestBorderPoint(srcRect, mouseX, mouseY);
			var srcPoint = this._getBorderPoint(srcRect, border.border, border.delta);
			mouseX = srcPoint.x;
			mouseY = srcPoint.y;
		} else if (this.mostSelectedPoint == this.selectedLinePoints.length - 1) {
			var tgtRect = this.options.model.elements[l.trgId];
			var border = this._getClosestBorderPoint(tgtRect, mouseX, mouseY);
			var tgtPoint = this._getBorderPoint(tgtRect, border.border, border.delta);
			mouseX = tgtPoint.x;
			mouseY = tgtPoint.y;
		}
		context.beginPath();
		context.strokeStyle = "rgb(50, 50, 50)";
		for (var i=0; i<this.selectedLinePoints.length; i++) {
			var sp = this.selectedLinePoints[i];
			var x, y;
			if (i == this.mostSelectedPoint) {
				x = this._toCanvas(mouseX);
				y = this._toCanvas(mouseY);
				this.selectedLinePoints[i] = {x:mouseX, y:mouseY};
			} else {
				x = this._toCanvas(sp.x);
				y = this._toCanvas(sp.y);
			}
			if (i == 0) context.moveTo(x, y);
			else context.lineTo(x, y);
		}
		context.stroke();
	},
    _paintMovingShape:function(point) {
    	if (!this.moving) {
    		$.error("paintMovingShape called when no moving");
    		return;
    	}
		var mouseX = point.x;
		var mouseY = point.y;
		if (this.moveMinX - this.moveX0 + mouseX < 1) mouseX = 1 + this.moveX0 - this.moveMinX;
		if (this.moveMinY - this.moveY0 + mouseY < 1) mouseY = 1 + this.moveY0 - this.moveMinY;
		if (this.moveMaxX + mouseX - this.moveX0 > this.options.model.width - 3) {
			//mouseX = this.options.model.width - 3 - this.moveMaxX + this.moveX0;
			var canvas = this.element[0];
			canvas.width = parseInt(canvas.width * 1.1);
			this.options.model.width = this._toModel(canvas.width);
		}
		if (this.moveMaxY + mouseY -this. moveY0 > this.options.model.height - 3) {
			//mouseY = this.options.model.height - 3 - this.moveMaxY + this.moveY0;
			var canvas = this.element[0];
			canvas.height = parseInt(canvas.height * 1.1);
			this.options.model.height = this._toModel(canvas.height);
		}
		var deltaX = mouseX - this.moveX0;
		var deltaY = mouseY - this.moveY0;
    	var context = (this.element)[0].getContext("2d");
		for (var i=0; i<this.selectedRectsIds.length; i++) {
			this._paintSelectedElement(this.options.model.elements[this.selectedRectsIds[i]], context, deltaX, deltaY);
		}
		for (var i=0; i<this.selectedLinesIds.length; i++) {
			this._paintSelectedLine(this.options.model.lines[this.selectedLinesIds[i]], context, deltaX, deltaY);
		}
    },
    _paintAddingLine:function(p) {
		var n = this.options.model.elements[this.mostSelectedRectId];
		if (n == null) return null;
		var context = (this.element)[0].getContext("2d");
		context.save();
		context.beginPath();
		context.strokeStyle = "black";
		context.lineWidth = 1;
		context.moveTo(this._toCanvas(n.x + n.width / 2), this._toCanvas(n.y + n.height / 2));
		context.lineTo(this._toCanvas(p.x), this._toCanvas(p.y));
		context.stroke();
		var t = this._getElementAtPoint(p);
		if (t != null) {
			var x0 = this._toCanvas(t.x);var y0 = this._toCanvas(t.y);
			var w = this._toCanvas(t.width);
			var h = this._toCanvas(t.height);
			context.lineWith = 1;
			context.strokeStyle = "rgb(0,0,255)";
	    	context.fillStyle = "rgba(50,50,200,0.1)";
	    	context.rect(x0, y0, w,h);
	    	context.fill();
	    	context.stroke();
		}
		context.restore();
	},
	_paintSelectingGroup:function(p) {
		var context = (this.element)[0].getContext("2d");
		context.save();
    	context.beginPath();
    	context.lineWidth = 1;
    	context.strokeStyle = "rgb(80,80,80)";
    	context.fillStyle = "rgba(200, 200, 240, 0.2)";
    	var x0 = this.moveX0, y0 = this.moveY0, x1 = p.x, y1 = p.y;
    	if (x1 < x0) {
    		var tmp = x0;
    		x0 = x1;
    		x1 = tmp;
    	}
    	if (y1 < y0) {
    		var tmp = y0;
    		y0 = y1;
    		y1 = tmp;
    	}
    	context.rect(this._toCanvas(x0), this._toCanvas(y0), this._toCanvas(x1 - x0), this._toCanvas(y1 - y0));
    	context.fillRect(this._toCanvas(x0), this._toCanvas(y0), this._toCanvas(x1 - x0), this._toCanvas(y1 - y0));    	
    	context.stroke();
    	context.restore();
	},
	_buildDeleteGroup:function() {
		var thisWidget = this;
		var delGroup = [];
		$.each(this.selectedRectsIds, function(i, id) {
			delGroup.push(thisWidget.options.model.elements[id]);
		});
		$.each(this.selectedLinesIds, function(i, id) {
			delGroup.push(thisWidget.options.model.lines[id]);
		});
		// Add lines connected to rects in group
		$.each(this.selectedRectsIds, function(i, id) {
			var lines = thisWidget._getLinesWithSource(id);
			$.each(lines, function(j, line) {
				if (delGroup.indexOf(line) < 0) delGroup.push(line);					
			});
			lines = thisWidget._getLinesWithTarget(id);
			$.each(lines, function(j, line) {
				if (delGroup.indexOf(line) < 0) delGroup.push(line);					
			});
		});
		return delGroup;
	},
	_buildCopyGroup:function() {
		var thisWidget = this;
		var copyGroup = {elements:[], lines:[]};
		$.each(this.selectedRectsIds, function(i, id) {
			copyGroup.elements.push(thisWidget.options.model.elements[id]);
		});
		$.each(this.selectedLinesIds, function(i, id) {
			// Add only lines fully connected to rects in group
			var line = thisWidget.options.model.lines[id];
			var srcElement = thisWidget.getElement(line.srcId);
			var trgElement = thisWidget.getElement(line.trgId);
			if (copyGroup.elements.indexOf(srcElement) >= 0 && copyGroup.elements.indexOf(trgElement) >= 0) {
				copyGroup.lines.push(line);
			}
		});
		return copyGroup;
	},
	_addNewLinePoints:function(line) {
		var srcNode = this.options.model.elements[line.srcId];
		var dstNode = this.options.model.elements[line.trgId];
		var p = this.addLineTargetPoint;
		if (srcNode.id != dstNode.id) {
			var x0 = srcNode.x + srcNode.width / 2;
			var y0 = srcNode.y + srcNode.height / 2;
			var srcBorder = this._intersectBorder(srcNode, x0, y0, p.x, p.y);
			line.srcBorder = srcBorder.border;
			line.srcDelta = srcBorder.delta;
			var trgBorder = this._intersectBorder(dstNode, p.x, p.y, x0, y0);
			line.trgBorder = trgBorder.border;
			line.trgDelta = trgBorder.delta;
		} else {
			line.srcBorder = EAST;
			line.srcDelta = 10;
			line.trgBorder = NORTH;
			line.trgDelta = srcNode.width - 10;
			line.points.push({x:srcNode.x + srcNode.width + 30, y:srcNode.y + 10});
			line.points.push({x:srcNode.x + srcNode.width + 30, y:srcNode.y + 10 - 30});
			line.points.push({x:srcNode.x + srcNode.width - 10, y:srcNode.y + 10 - 30});
		}
	},
	_editSelection:function(e) {
		var selectedElement = null;
		if (this.mostSelectedRectId != null) {
			selectedElement = this.options.model.elements[this.mostSelectedRectId];
			if (this.options.customEditors[selectedElement.type]) {
				this.options.customEditors[selectedElement.type](selectedElement);
			} else {
				this._getElementHandler(selectedElement).editProperties(selectedElement, this.element);
				if (this.options.editListener) this.options.editListener(selectedElement); 
			}
		} else if (this.mostSelectedLineId != null) {
			selectedElement = this.options.model.lines[this.mostSelectedLineId];
			this._getLineHandler(selectedElement).editProperties(selectedElement, this.element);
			if (this.options.editListener) this.options.editListener(selectedElement);
		}
	},
	// Private event handlers
	_onKeyDown:function(e) {
		if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 46) { // del
			e.preventDefault();
			if (this.readOnly()) return;
			var delGroup = this._buildDeleteGroup();
			if (delGroup && delGroup.length > 0) {
				if (this.options.onDeleteRequest) {
					this.options.onDeleteRequest(delGroup);
				}
			}
		} else if (e.keyCode == 90 && (e.ctrlKey || e.metaKey)) { // undo
			if (this.readOnly()) return;
			this.undo();
		} else if (e.keyCode == 89 && (e.ctrlKey || e.metaKey)) { // redo
			if (this.readOnly()) return;
			this.redo();
		} else if (e.keyCode == 67 && (e.ctrlKey || e.metaKey)) { // ctrl-C
			if (this.options.onCopyRequest) this.options.onCopyRequest();
		} else if (e.keyCode == 86 && (e.ctrlKey || e.metaKey)) { // ctrl-V
			if (this.readOnly()) return;
			if (this.options.onPasteRequest) this.options.onPasteRequest();
		} else if (e.keyCode == 88 && (e.ctrlKey || e.metaKey)) { // ctrl-X
			if (this.readOnly()) return;
			if (this.options.onCutRequest) this.options.onCutRequest();
		} else if (e.keyCode == 190 && (e.ctrlKey || e.metaKey)) { // ctrl-.
			console.log("Elements:" + Object.keys(this.options.model.elements).length);
			console.log("Lines:" + Object.keys(this.options.model.lines).length);
			var t0 = (new Date()).getTime();
			for (var i=0; i < 100; i++) {
				this.paint();
			}
			var t1 = (new Date()).getTime();
			console.log("100 frames in " + (t1 - t0) + " [ms] => " + (100000 / (t1 -t0)) + " [f/s]");
		}
	},
	_onMouseMove:function(e) {
    	var newDirection = -1;
    	var p = this._relativeEventCoordsToPoint(e);
    	p.x = this._toModel(p.x);
    	p.y = this._toModel(p.y);
    	if (!this.moving) {
    		if (this.canMove) {
    			var lineUnderCursor = this._getLineAtPoint(p);
    			if (this.mostSelectedLineId && lineUnderCursor && lineUnderCursor.id == this.mostSelectedLineId) {
					this.canMove = false;
					var newLinePoints = [];
					for (var i=0; i<this.selectedLinePoints.length; i++) {
						newLinePoints.push(this.selectedLinePoints[i]);;
						if (i == this.linePointIndex0) {
							newLinePoints.push(p);
							this.mostSelectedPoint = i+1;
						}
					}
					this.selectedLinePoints = newLinePoints;
					this.movingLinePoint = true;
					this.paint();
					this._paintMovingLinePoint(p);
				} else {
					this.moving = true;
					this._calculateMovingLimits();
					this.paint();
					this._paintMovingShape(p);
				}
	    	} else if (this.shouldUnselect) {
	    		this.shouldUnselect = false;
				this.selectingGroup = true;
				this.moveX0 = p.x;
				this.moveY0 = p.y;
				this.paint();
				this._paintSelectingGroup(p);
	    	} else if (this.selectingGroup) {
	    		if (e.buttons == 0) {
	    			// Reentering without button pressed
	    			this._unselectAll();
	    			this.selectingGroup = false;
	    			this.paint();
	    			return;
	    		}
	    		this.paint();
	    		this._paintSelectingGroup(p);
	    	} else if (!this.sizing && !this.movingLinePoint && !this.readOnly()) {
		    	if (this.mostSelectedRectId) {
			    	var r = this.options.model.elements[this.mostSelectedRectId];
			    	this.originalRectResize = {x:r.x, y:r.y, w:r.width, h:r.height};
			    	var x = p.x;
			    	var y = p.y;
			    	var xc = r.x + r.width / 2;
			    	var yc = r.y + r.height / 2;
			    	var resizeSens = RESIZE_SENSIBILITY;
			    	if (Math.abs(x - r.x) <= resizeSens) {
						if (Math.abs(y - r.y) <= resizeSens) {
							newDirection = 7;
						} else if (Math.abs(y - (r.y + r.height)) <= resizeSens) {
							newDirection = 5;
						} else if (y >= r.y && y <= (r.y + r.height)) {
							newDirection = 6;
						}
					} else if (Math.abs(x - (r.x + r.width)) <= resizeSens) {
						if (Math.abs(y - r.y) <= resizeSens) {
							newDirection = 1;
						} else if (Math.abs(y - (r.y + r.height)) <= resizeSens) {
							newDirection = 3;
						} else if (y >= r.y && y <= (r.y + r.height)) {
							newDirection = 2;
						}
					} else if (Math.abs(y - r.y) <= resizeSens) {
						if (x >= r.x && x <= (r.x + r.width)) {
							newDirection = 0;
						}
					} else if (Math.abs(y - (r.y + r.height)) <= resizeSens) {
						if (x >= r.x && x <= (r.x + r.width)) {
							newDirection = 4;
						}
					} else if (Math.abs(x - xc) <= resizeSens && Math.abs(y - yc) <= resizeSens) {
						var canStartLine = this._getElementHandler(r).canStartLine(r);
						if (canStartLine) {
							newDirection = 8; // Center -> Start relation
						}
					}
			    	if (newDirection != -1 && newDirection != 8 && !this.options.model.elements[this.mostSelectedRectId].resizable) {
			    		newDirection = -1;
			    	}
			    	if (newDirection != this.sizeDirection) {
						if (this.showingMoveCursor) {
							this._hideMoveCursor();
						}
						if (newDirection != -1) {
							this._showMoveCursor(newDirection);							
						}
						this.sizeDirection = newDirection;
					}
		    	} else if (this.mostSelectedLineId) {
		    		var resizeSens = RESIZE_SENSIBILITY * this.options.model.zoom / 100;
		    		newDirection = -1; 
					x = p.x
					y = p.y;
					for (var i=0; i<this.selectedLinePoints.length; i++) {
						var point = this.selectedLinePoints[i];
						if (Math.abs(point.x - x) < resizeSens && Math.abs(point.y - y) < resizeSens) {
							newDirection = 9;
							this.mostSelectedPoint = i;
							this.paint();
							break;
						}
					}
					if (newDirection != this.sizeDirection) {
						if (this.showingMoveCursor) {
							this._hideMoveCursor();
						}
						if (newDirection != -1) {
							this._showMoveCursor(newDirection);							
						}
						this.sizeDirection = newDirection;
					}
		    	} else {
		    		if (this.showingMoveCursor) {
						this._hideMoveCursor();
					}
		    		this.sizeDirection = -1;
		    	}
	    	} else if (this.sizing) {
	    		var mx = p.x;
				var my = p.y;
				var r = this.options.model.elements[this.mostSelectedRectId];
				if (this.sizeDirection != 8) {
					var minW = r.minWidth;
					var minH = r.minHeight;
					switch(this.sizeDirection) {
						case 0:			
							if (r.height + r.y - my < minH) {
								my = r.height + r.y - minH;
							}				
							r.height = r.height + r.y - my;
							r.y = my;
							break;
						case 1:
							if (mx - r.x < minW) {
								mx = minW + r.x;
							}
							if (r.height + r.y - my < minH) {
								my = r.height + r.y - minH;
							}
							r.width = mx - r.x; 
							r.height = r.height + r.y - my;
							r.y = my;
							break;
						case 2:
							if (mx - r.x < minW) {
								mx = minW + r.x;
							}
							r.width = mx - r.x; 
							break;
						case 3:
							if (mx - r.x < minW) {
								mx = minW + r.x;
							}
							if (my - r.y < minH) {
								my = minH + r.y;
							}
							r.width = mx - r.x;
							r.height = my - r.y; 
							break;
						case 4:
							if (my - r.y < minH) {
								my = minH + r.y;
							}
							r.height = my - r.y; 
							break;
						case 5:
							if (r.width + r.x - mx < minW) {
								mx = r.width + r.x - minW;
							}
							if (my - r.y < minH) {
								my = minH + r.y;
							}
							r.width = r.width + r.x - mx;
							r.x = mx; 
							r.height = my - r.y; 
							break;
						case 6:
							if (r.width + r.x - mx < minW) {
								mx = r.width + r.x - minW;
							}
							r.width = r.width + r.x - mx;
							r.x = mx; 
							break;
						case 7:
							if (r.width + r.x - mx < minW) {
								mx = r.width + r.x - minW;
							}
							if (r.height + r.y - my < minH) {
								my = r.height + r.y - minH;
							}
							r.width = r.width + r.x - mx;
							r.x = mx;
							r.height = r.height + r.y - my;
							r.y = my;
							break;
					}
					this.paint();
				} else {
					this.paint();
					this._paintAddingLine(p);					
				}
	    	} else if (this.movingLinePoint) {
	    		this.paint();
	    		this._paintMovingLinePoint(p);	    	
	    	}
    	} else {
    		// moving
    		this.paint();
    		this._paintMovingShape(p);
    	}
    	return false;
	},
	_onMouseDown:function(e) {
    	var p = this._relativeEventCoordsToPoint(e);
    	p.x = this._toModel(p.x);
    	p.y = this._toModel(p.y);
    	this.shouldUnselect = false;
    	if (this.sizeDirection == -1) {
			var r = this._getElementAtPoint(p);
			if (!r) {
				var l = this._getLineAtPoint(p);
				if (!l) {
					this.canMove = false;
					if (!e.ctrlKey) {
						this._unselectAll();
					}
					this.shouldUnselect = true;
				} else {
					// l != null
					if (!this._isLineSelected(l.id)) {
						this.canMove = true;
						if (!e.ctrlKey) {
							this._unselectAll();
							this._selectLine(l);
						} else {
							this._selectLine(l);
						}
					} else {
						// isSelected								
						if (e.ctrlKey) {
							this._unselectLine(l);
							this.canMove = false;
						} else {
							if (l.id != this.mostSelectedLineId) {
								this.mostSelectedLineId = l.id;
								this.selectedLinePoints = this._getAllLinePoints(l);
								this.mostSelectedRectId = null;
							}
							this.canMove = true;
						}								
					}
				}
			} else {
				if (!this._isRectSelected(r.id)) {
					this.canMove = true;
					if (!e.ctrlKey) {
						this._unselectAll();
						this._selectRect(r);
					} else {
						this._selectRect(r);
					}
				} else {
					if (e.ctrlKey) {
						this._unselectRect(r);
						this.canMove = false;
					} else {
						if (r.id != this.mostSelectedRectId) {
							this.mostSelectedRectId = r.id;
							this.mostSelectedLineId = null;
						}
						this.canMove = true;
					}
				}
			}
			if (this.canMove) {
				this.moveX0 = p.x;
				this.moveY0 = p.y;
			}
		} else if (this.sizeDirection == 9) {
			this.movingLinePoint = true;
			this._paintMovingLinePoint(p);
		} else {
			this.sizing = true;
		}
    	this.paint();
    	if (this.readOnly()) {
    		this.canMove = false;
    		this.sizing = false;
    	}
    	return false;
	},
	_onMouseUp:function(e) {
    	var p = this._relativeEventCoordsToPoint(e);
    	this.addLineTargetPoint = p;
    	p.x = this._toModel(p.x);
    	p.y = this._toModel(p.y);
    	this.canMove = false;
    	if (this.moving) {
    		// Restore size to reflect changes in undo manager
    		this.options.model.width = this.oldModelWidth;
    		this.options.model.height = this.oldModelHeight;
    		this.startOperation();
    		var mouseX = p.x;
    		var mouseY = p.y;
    		if (this.moveMinX - this.moveX0 + mouseX < 1) mouseX = 1 + this.moveX0 - this.moveMinX;
			if (this.moveMinY - this.moveY0 + mouseY < 1) mouseY = 1 + this.moveY0 - this.moveMinY;
			var deltaX = mouseX - this.moveX0;
			var deltaY = mouseY - this.moveY0;
			for (var i=0; i<this.selectedRectsIds.length; i++) {
				var id = this.selectedRectsIds[i];
				var r = this.options.model.elements[id];
				r.x += deltaX;
				r.y += deltaY;
			}
			for (var i=0; i<this.selectedLinesIds.length; i++) {
				var l = this.options.model.lines[this.selectedLinesIds[i]];
				var points = l.points;
				for (var j=0; j<points.length; j++) {
					points[j].x += deltaX;
					points[j].y += deltaY;
				}
			}
			// Resize model to fit all elements + 10%
			var maxX = 10; maxY = 10;
			$.each(this.options.model.elements, function(id, e) {
				if (e.x + e.width > maxX) maxX = e.x + e.width;
				if (e.y + e.height > maxY) maxY = e.y + e.height;
			});
			this.options.model.width = parseInt(maxX * 1.1);
			this.options.model.height = parseInt(maxY * 1.1);
			if (this.options.model.width < 640) this.options.model.width = 640;
			if (this.options.model.height < 480) this.options.model.height = 480;
			var canvas = this.element[0];
			this.completeOperation();
			this.moving = false;
			canvas.width = this._toCanvas(this.options.model.width);
			canvas.height = this._toCanvas(this.options.model.height);
			this.paint();
    	} else if (this.sizing) {
			var mx = p.x;
			var my = p.y;
			var r = this.options.model.elements[this.mostSelectedRectId];
			if (this.sizeDirection != 8) {
				var minW = r.minWidth;
				var minH = r.minHeight;
				switch(this.sizeDirection) {
					case 0:			
						if (r.height + r.y - my < minH) {
							my = r.height + r.y - minH;
						}				
						r.height = r.height + r.y - my;
						r.y = my;
						break;
					case 1:
						if (mx - r.x < minW) {
							mx = minW + r.x;
						}
						if (r.height + r.y - my < minH) {
							my = r.height + r.y - minH;
						}
						r.width = mx - r.x; 
						r.height = r.height + r.y - my;
						r.y = my;
						break;
					case 2:
						if (mx - r.x < minW) {
							mx = minW + r.x;
						}
						r.width = mx - r.x; 
						break;
					case 3:
						if (mx - r.x < minW) {
							mx = minW + r.x;
						}
						if (my - r.y < minH) {
							my = minH + r.y;
						}
						r.width = mx - r.x;
						r.height = my - r.y; 
						break;
					case 4:
						if (my - r.y < minH) {
							my = minH + r.y;
						}
						r.height = my - r.y; 
						break;
					case 5:
						if (r.width + r.x - mx < minW) {
							mx = r.width + r.x - minW;
						}
						if (my - r.y < minH) {
							my = minH + r.y;
						}
						r.width = r.width + r.x - mx;
						r.x = mx; 
						r.height = my - r.y; 
						break;
					case 6:
						if (r.width + r.x - mx < minW) {
							mx = r.width + r.x - minW;
						}
						r.width = r.width + r.x - mx;
						r.x = mx; 
						break;
					case 7:
						if (r.width + r.x - mx < minW) {
							mx = r.width + r.x - minW;
						}
						if (r.height + r.y - my < minH) {
							my = r.height + r.y - minH;
						}
						r.width = r.width + r.x - mx;
						r.x = mx;
						r.height = r.height + r.y - my;
						r.y = my;
						break;					
				}
				var swap = {x:r.x, y:r.y, w:r.width, h:r.height};
				r.x = this.originalRectResize.x;
				r.y = this.originalRectResize.y;
				r.width = this.originalRectResize.w;
				r.height = this.originalRectResize.h;
				this.startOperation();
				r.x = swap.x;
				r.y = swap.y;
				r.width = swap.w;
				r.height = swap.h;
				this._fixAdjacentLines(r);
				this.completeOperation();				
			} else {
				// sizeDirection == 8
				var dstNode = this._getElementAtPoint(p); 
				if (dstNode != null) {
					if (dstNode.id == this.mostSelectedRectId) {
						var xc = dstNode.x + dstNode.width / 2;
						var yc = dstNode.y + dstNode.height / 2;
						if (Math.abs(p.x - xc) < 4 && Math.abs(p.y - yc) < 4) {
							// Double click just on the center of mostSelectedRect
							this.moving = false;
							this.sizing = false;
							return; 
						}
					}
					var srcNode = this.options.model.elements[this.mostSelectedRectId];
					if (this.options.onLineAddRequest) {
						this.options.onLineAddRequest(srcNode, dstNode);
					} else {
						$.error("onLineAddRequest not passed in options");
					}
					this.sizing = false;
				}				
			}
			this.sizing = false;
			this.paint();
		} else if (this.movingLinePoint) {
			this.startOperation();
			this._finishMovingLinePoint();
			this.movingLinePoint = false;
			this.paint();
			this.completeOperation();
		} else if (this.selectingGroup) {
			this._finishSelectingGroup(p.x, p.y);
			this.selectingGroup = false;
			this.paint();
		} else {
			if (this.shouldUnselect) {
				this.shouldUnselect = false;
				this._unselectAll();
				this.paint();
			}
		}
	},
	
	// Public Methods
	model:function(newModel) {
		if (newModel) {
			this.options.model = newModel;
		}
		return this.options.model;
	},
	startOperation:function() {
		if (this.objA) $.error("Last operation was not correctly canceled");
		this.objA = $.extend(true, {}, this.options.model); // clone
	},
	completeOperation:function() {
		var diff = jsonpatch.compare(this.options.model, this.objA);
		this.objA = null;
		if (!diff || diff.length == 0) return;
		if (this.diffs.length >= this.MAX_DIFFS) {
			this.diffs.splice(0, (this.diffs.length - this.MAX_DIFFS + 1));
		}
		this.diffs.push(diff);
		this.redoDiffs = [];
		this.changed = true;
		this._triggerUndoBufferChange();
	},
	cancelOperation:function() {
		this.objA = null;
	},
	canUndo:function() {
		return this.diffs.length > 0;
	},
	undo:function() {
		var diffIndex = this.diffs.length - 1;
		if (diffIndex < 0) return;
		var diff = this.diffs[diffIndex];
		var objB = $.extend(true, {}, this.options.model); // clone
		jsonpatch.apply(this.options.model, diff);	
		// Save to redoManager
		this.redoDiffs.push(jsonpatch.compare(this.options.model, objB));
		// Remove diff on top
		this.diffs.splice(this.diffs.length-1, 1);
		this._unselectAll();
		// Restore canvas size
		var canvas = this.element[0];
		canvas.width = this._toCanvas(this.options.model.width);
		canvas.height = this._toCanvas(this.options.model.height);
		this.paint();
		this._triggerUndoBufferChange();
	},
	resetUndoBuffer:function() {
		this.diffs = new Array();
		this.redoDiffs = new Array();
	},
	canRedo:function() {
		return this.redoDiffs != null && this.redoDiffs.length > 0;
	},
	redo:function() {
		if (!this.canRedo()) return;
		var objA = $.extend(true, {}, this.options.model); // clone
		var redoDiff = this.redoDiffs[this.redoDiffs.length - 1];
		jsonpatch.apply(this.options.model, redoDiff);
		this.redoDiffs.splice(this.redoDiffs.length - 1, 1);
		this.diffs.push(jsonpatch.compare(this.options.model, objA));
		this._unselectAll();
		this.paint();
		this._triggerUndoBufferChange();
	},
	modelChanged:function() {
		return this.changed;
	},
	markUnchanged:function() {
		this.changed = false;
	},
	paint:function() {
		var thisWidget = this;
		var canvas = this.element[0];
		var model = this.options.model;
    	var context = canvas.getContext("2d");    	
    	context.clearRect(0, 0, canvas.width, canvas.height);
    	context.beginPath();
    	context.rect(0,0, this._toCanvas(model.width), this._toCanvas(model.height));
    	context.stroke();
    	$.each(model.lines, function(id, line) {
    		thisWidget._paintLine(line, context);
    	});
    	$.each(model.elements, function(id, element) {
    		thisWidget._paintElement(element, context);
    	});
		// Paint selection
    	$.each(this.selectedRectsIds, function(i, elementId) {
    		thisWidget._paintSelectedElement(model.elements[elementId], context);
    	});
    	$.each(this.selectedLinesIds, function(i, lineId) {
    		thisWidget._paintSelectedLine(model.lines[lineId], context);
    	});
    },
    toCanvas:function(x) {return this._toCanvas(x);},
    toModel:function(x) {return this._toModel(x);},
    canvasCoordFromEvent:function(event) {return this._relativeEventCoordsToPoint(event);},
    getLinePoints:function(line) {
    	return this._getAllLinePoints(line);
    },
    zoom:function(z) {
    	if (!z) return this.options.model.zoom;
    	this.options.model.zoom = z;
    	var canvas = (this.element)[0];
    	canvas.width = this._toCanvas(this.options.model.width);
		canvas.height = this._toCanvas(this.options.model.height);
    	this.paint();
    },
    addElement:function(e) {
    	this.startOperation();
    	this.options.model.elements[e.id] = e;
    	this.paint();
    	this.completeOperation();
    	return e;
    },
    addLine:function(l) {
		this.startOperation();
    	this.options.model.lines[l.id] = l;
    	this._addNewLinePoints(l);
    	this.paint();
		this.completeOperation();
    	return l;
    },
    deleteGroup:function(delGroup) {
    	this.startOperation();
    	var m = this.options.model;
    	$.each(delGroup, function(i, e) {
    		if (m.elements[e.id]) delete m.elements[e.id];
    		if (m.lines[e.id]) delete m.lines[e.id];
    	});
    	this._unselectAll();
    	this.paint();
    	this.completeOperation();
    },
    buildDeleteGroup:function() {
    	return this._buildDeleteGroup();
    },
    hasSelection:function() {
    	return this.selectedLinesIds.length > 0 || this.selectedRectsIds.length > 0;
    },
    selectedElements:function() {
    	var thisWidget = this;
    	var r = [];
    	$.each(this.selectedRectsIds, function(i, id) {
    		r.push(thisWidget.options.model.elements[id]);
    	});
    	return r;
    },
    selectedLines:function() {
    	var thisWidget = this;
    	var r = [];
    	$.each(this.selectedLinesIds, function(i, id) {
    		r.push(thisWidget.options.model.lines[id]);
    	});
    	return r;
    },
    getElement:function(id) {
    	return this.options.model.elements[id];
    },
    getLine:function(id) {
    	return this.options.model.lines[id];
    },
    adjustModelSize:function() {
    	var maxX = 10;
    	var maxY = 10;
    	$.each(this.options.model.elements, function(i, e) {
    		if (e.x + e.width > maxX) maxX = e.x + e.width;
    		if (e.y + e.height > maxX) maxY = e.y + e.height;
    	});
    	if (this.options.model.width < maxX) this.options.model.width = maxX + 5;
    	if (this.options.model.height < maxY) this.options.model.height = maxY + 5;
    },
    getCopyGroup:function() {
    	return this._buildCopyGroup();
    },
    copy:function() {
    	var thisWidget = this;
    	var copyGroup = this._buildCopyGroup();
    	var clipboard = {elements:[], lines:[]};
    	var newIds = {};
    	$.each(copyGroup.elements, function(i, element) {
			var elementCopy = thisWidget._getElementHandler(element).cloneForCopy(element);
			elementCopy.id = guid();
			newIds[element.id] = elementCopy.id;
			clipboard.elements.push(elementCopy);
    	});
    	$.each(copyGroup.lines, function(i, line) {
			var lineCopy = thisWidget._getLineHandler(line).cloneForCopy(line);
			lineCopy.id = guid();
			lineCopy.srcId = newIds[lineCopy.srcId];
			lineCopy.trgId = newIds[lineCopy.trgId];
			clipboard.lines.push(lineCopy);
    	});
    	return clipboard;
    },
    cut:function() {
    	var thisWidget = this;
    	var delGroup = this._buildDeleteGroup();
    	var copyGroup = this._buildCopyGroup();
    	var clipboard = {elements:[], lines:[]};
    	var newIds = {};
    	$.each(copyGroup.elements, function(i, element) {
			var elementCopy = thisWidget._getElementHandler(element).cloneForCopy(element);
			elementCopy.id = guid();
			newIds[element.id] = elementCopy.id;
			clipboard.elements.push(elementCopy);
    	});
    	$.each(copyGroup.lines, function(i, line) {
			var lineCopy = thisWidget._getLineHandler(line).cloneForCopy(line);
			lineCopy.id = guid();
			lineCopy.srcId = newIds[lineCopy.srcId];
			lineCopy.trgId = newIds[lineCopy.trgId];
			clipboard.lines.push(lineCopy);
    	});
    	this._unselectAll();
    	this.deleteGroup(delGroup);    	
    	this.paint();
    	return clipboard;
    },
    paste:function(clipboard, x0OrDelta, y0) {
    	var thisWidget = this;
    	var delta=null, x0=null;    	
    	if (!x0OrDelta) {
    		delta = 0;
    	} else {
    		if (y0) {
    			x0 = x0OrDelta;
    		} else {
    			delta = x0OrDelta;
    		}
    	}
    	// TODO: Recalculate delta i x0 && y0 (context menu paste)
    	// calculate min x and min y from elements to obtain new delta
    	var newIds = {};
    	var group = {elements:[], lines:[]};
    	$.each(clipboard.elements, function(i, element) {
			var elementCopy = thisWidget._getElementHandler(element).cloneForCopy(element);
			elementCopy.id = guid();
			elementCopy.x += delta;
			elementCopy.y += delta;
			newIds[element.id] = elementCopy.id;
			group.elements.push(elementCopy);
    	});
    	$.each(clipboard.lines, function(i, line) {
			var lineCopy = thisWidget._getLineHandler(line).cloneForCopy(line);
			lineCopy.id = guid();
			lineCopy.srcId = newIds[lineCopy.srcId];
			lineCopy.trgId = newIds[lineCopy.trgId];
			$.each(lineCopy.points, function(j, p) {
				p.x += delta;
				p.y += delta;
			});
			group.lines.push(lineCopy);
    	});
    	this._unselectAll();
    	this.startOperation();
    	$.each(group.elements, function(i, element) {
    		thisWidget.options.model.elements[element.id] = element;
    		thisWidget._selectRect(element);
    	});
    	$.each(group.lines, function(i, line) {
    		thisWidget.options.model.lines[line.id] = line;
    		thisWidget._selectLine(line);
    	});
    	this.adjustModelSize();
    	this.completeOperation();
    	this.paint();
    },
    readOnly:function(ro) {
    	if (ro != undefined) {
    		this.options.readOnly = ro;
    	} else {
    		return this.options.readOnly;
    	}
    },
    selectElement:function(element) {
    	this._unselectAll();
    	this._selectRect(element);
    },
    getSelectedElementId:function() {
    	return this.mostSelectedRectId;
    },
    getSelectedLineId:function() {
    	return this.mostSelectedLineId;
    },
    unselectAll:function() {
    	this._unselectAll();
    },
    selectGroup:function(elements, lines) {
    	this._unselectAll();
    	if (elements) elements.forEach(e => this._selectRect(e, true));
    	if (lines) lines.forEach(l => this._selectLine(l, true));
    	this._triggerSelectionChange();
    },
    getElementHandler:function(element) {
    	return this._getElementHandler(element);
    },
    getLineHandler:function(line) {
    	return this._getLineHandler(line);
    }
});