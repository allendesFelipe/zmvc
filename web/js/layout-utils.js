function attachElementsToTabs(tabs, divs, grids, nomargin) {
	if (divs) {
		for (var i=0; i<divs.length; i++) {
			var d = divs[i];
			d.css("position", "absolute");
		}
	}
	tabs.data("on-layout-resize", function() {
		var w = tabs.width();
		var h = tabs.height();
		if (grids) {
			for (var i=0; i<grids.length; i++) {
				grids[i].jqGrid("setGridWidth", w - 30);
				grids[i].jqGrid("setGridHeight", h - 96);
			}
		}
		if (divs) {
			for (var i=0; i<divs.length; i++) {
				var d = divs[i];
				if (nomargin) {
					d.width(w - 2);
					d.height(h - 32);					
				} else {
					d.width(w - 30);
					d.height(h - 56); // 56
				}
				doChildLayout(d);
			}
		}
	});
}

function attachJQGridToContainerPanel(container, grid, noCaption) {
	container.data("on-layout-resize", function() {
		grid.jqGrid("setGridWidth", container.width());
		if (noCaption) {
			grid.jqGrid("setGridHeight", (container.height() - 24));
		} else {
			grid.jqGrid("setGridHeight", (container.height() - 46));
		}
		return false;
	});
}

function ajustaJQGrid(grid) {
	grid.jqGrid("setGridWidth", grid.parent().width());
}
