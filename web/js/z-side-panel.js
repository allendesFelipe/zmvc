$.widget(
	"z.zSidePanel", {
		options:{
			panelWidth:300,
			fromOpposite:false,
			widthAsPercent:false,
			panelPosition:"left",
			isOpen:false
		},
		_create:function() {
			var thisWidget = this;
			if (this.element.data("side-panel-width")) {
				var panelWidth = this.element.data("side-panel-width");
				if (panelWidth.length >= 1 && panelWidth.substr(panelWidth.length - 1) == "%") {
					this.options.widthAsPercent = true;
					panelWidth = panelWidth.substring(0, panelWidth.length - 1);
				}
				this.options.panelWidth = parseInt(this.element.data("side-panel-width"));				
			}
			if (this.element.data("side-panel-position")) {
				this.options.panelPosition = this.element.data("side-panel-position");				
			}
			if (this.element.data("from-opposite")) {
				this.options.fromOpposite = true;
			}
			if (this.element.children().length != 2) $.error("Side Panel must have two children, but " + this.element.children().length + " found. In container " + this.element.html());
			//this.element.css("overflow", "hidden");
			var mainContent = $(this.element.children()[0]);
			var sidePanel = $(this.element.children()[1]);
			sidePanel.css("position", "absolute");
			mainContent.css("position", "absolute");
			sidePanel.hide();
			doChildLayout(sidePanel);
			markLayoutInitialized(this.element);
		},
		isOpen:function() {
			return this.options.isOpen;
		},
		open:function() {
			var sidePanel = $(this.element.children()[1]);				
			var h = this.element.height();
			var w = this.element.width();
			var panelWidth;
			if (this.options.widthAsPercent) {
				panelWidth = this.options.panelWidth * w / 100;
			} else if (this.options.fromOpposite) {
				panelWidth = w - this.options.panelWidth;
			} else {
				panelWidth = this.options.panelWidth;
			}
			if (this.options.panelPosition == "left") {
				sidePanel.height(h);
				sidePanel.width(panelWidth);
				sidePanel.css({left:-panelWidth, top:0});
				sidePanel.animate({left:0}, 400);
			} else if (this.options.panelPosition == "right") {
				sidePanel.height(h);
				sidePanel.width(panelWidth);
				sidePanel.css({left:w, top:0});
				sidePanel.animate({left:w - panelWidth}, 400);
			}
			sidePanel.show();
			doChildLayout(sidePanel);
			this.options.isOpen = true;
		},
		close:function() {
			var sidePanel = $(this.element.children()[1]);
			var h = this.element.height();
			var w = this.element.width();
			var panelWidth;
			if (this.options.widthAsPercent) {
				panelWidth = this.options.panelWidth * w / 100;
			} else if (this.options.fromOpposite) {
				panelWidth = w - this.options.panelWidth;
			} else {
				panelWidth = this.options.panelWidth;
			}
			if (this.options.panelPosition == "left") {
				sidePanel.animate({left:-panelWidth}, {
					duration:400,
					complete:function() {sidePanel.hide();}
				});
			} else if (this.options.panelPosition == "right") {
				sidePanel.animate({left:w}, {
					duration:400,
					complete:function() {sidePanel.hide();}
				});
			}
			this.options.isOpen = false;
		},
		doLayout:function() {
			var mainContent = $(this.element.children()[0]);
			var sidePanel = $(this.element.children()[1]);				
			var w = this.element.width();
			var h = this.element.height();
			var panelWidth;
			if (this.options.widthAsPercent) {
				panelWidth = this.options.panelWidth * w / 100;
			} else if (this.options.fromOpposite) {
				panelWidth = w - this.options.panelWidth;
			} else {
				panelWidth = this.options.panelWidth;
			}
			var options = this.options;
			var isOpen = options.isOpen;
			mainContent.height(h);
			mainContent.width(w);
			doChildLayout(mainContent);
			if (isOpen) {
				if (options.panelPosition == "left") {
					sidePanel.height(h);
				} else if (options.panelPosition == "right") {
					sidePanel.height(h);
					sidePanel.width(panelWidth);
					sidePanel.css({left:w - panelWidth});
				}
				doChildLayout(sidePanel);
			}
		}
	}
);