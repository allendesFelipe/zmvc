function ZDEVNavegacionElementHandler() {
	return $.extend(true, new ElementHandler(), {
		paintBorder:function(element, context, modeler, fillStyle, strokeStyle, cornerRadius) {
			// Border
			context.save();
	    	context.shadowOffsetX = 3*modeler.zoom()/100;
	    	context.shadowOffsetY = 3*modeler.zoom()/100;
	    	context.shadowBlur    = 3;
	    	context.shadowColor   = 'rgba(10, 10, 40, 0.5)';    	
	    	var x0 = modeler.toCanvas(element.x);
	    	var y0 = modeler.toCanvas(element.y);
	    	var x1 = modeler.toCanvas(element.x + element.width);
	    	var y1 = modeler.toCanvas(element.y + element.height);
	    	
	    	cornerRadius = modeler.toCanvas(cornerRadius !== undefined?cornerRadius:10);
	    	
	    	context.fillStyle = fillStyle?fillStyle:"rgba(255, 255, 255, 1.0)";
	    	context.strokeStyle = strokeStyle?strokeStyle:"black";
	    	context.lineWidth = 3;
	    	context.beginPath();
	    	
	    	context.moveTo(x0 + cornerRadius, y0);
	    	context.lineTo(x1 - cornerRadius, y0);
	    	context.arcTo(x1, y0, x1, y0+cornerRadius, cornerRadius);
	    	context.lineTo(x1, y1 - cornerRadius);
	    	context.arcTo(x1, y1, x1 - cornerRadius, y1, cornerRadius);
	    	context.lineTo(x0 + cornerRadius, y1);
	    	context.arcTo(x0, y1, x0, y1 - cornerRadius, cornerRadius);
	    	context.lineTo(x0, y0 + cornerRadius);
	    	context.arcTo(x0, y0, x0 + cornerRadius, y0, cornerRadius);
	    	context.closePath();
	    	
	    	context.stroke();
	    	context.fill();

	    	context.restore();			
		},
		editProperties:function(element, modeler) {
		},
		// zModeler -> ModelHandler override
		canStartLine:function(element) {return false;},
		
		paint:function(element, context, modeler) {
	    	this.paintBorder(element, context, modeler);
		}
	});
}

function ZDEVNavegacionLineHandler(colorFondoFlecha, colorLabel) {
	return $.extend(true, new LineHandler(), {
		colorFlecha:colorFondoFlecha?colorFondoFlecha:"blue",
		colorTexto:colorLabel?colorLabel:"black",
		angleBetweenPoints:function(x1,y1,x2,y2) {
			var O,v;
			if (x1 == x2) {
				if (y2 > y1) { 
					O = Math.PI / 2.0;
				} else {
					O = -Math.PI / 2.0; 
				}
			} else {
				v = (y2 - y1) / (x2 - x1);
				O = Math.atan(v);
				if (x2 < x1) {
					O = Math.PI + O; 
				}
			}
			return O;
		},
		distanceBetweenPoints:function(x1,y1,x2,y2) {
			return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
		},
		paint:function(line, context, modeler) {
			context.save();
	    	context.beginPath();
	    	context.lineWidth = 1;
	    	var points = modeler.getLinePoints(line);
	    	for (var i=0; i<points.length; i++) {
	    		var p = points[i];
	    		var x = modeler.toCanvas(p.x);
	    		var y = modeler.toCanvas(p.y);
	    		if (i == 0) context.moveTo(x, y);
	    		else context.lineTo(x, y);
	    	}
	    	context.stroke();    	
	
	    	// Arrow
	    	var x1 = modeler.toCanvas(points[points.length-1].x);
	    	var y1 = modeler.toCanvas(points[points.length-1].y);
	    	var x2 = modeler.toCanvas(points[points.length-2].x);
	    	var y2 = modeler.toCanvas(points[points.length-2].y);
	    	var x3, y3, x4, y4, x5, y5, xm, ym;
	    	var d = 20 * modeler.zoom() / 100;
	    	var dmax = Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
	    	var O = this.angleBetweenPoints(x1,y1,x2,y2);
	    	
	    	if (dmax < d) d = dmax;
			if (d > 0) {
				xm = x1 + d / 2.0 * Math.cos(O);
				ym = y1 + d / 2.0 * Math.sin(O);
				x4 = xm + d / 4.0 * Math.cos(O + Math.PI / 2.0);
				y4 = ym + d / 4.0 * Math.sin(O + Math.PI / 2.0);
				x5 = xm + d / 4.0 * Math.cos(O - Math.PI / 2.0);
				y5 = ym + d / 4.0 * Math.sin(O - Math.PI / 2.0);
			}
			context.shadowOffsetX = 3*modeler.zoom()/100;
	    	context.shadowOffsetY = 3*modeler.zoom()/100;
	    	context.shadowBlur    = 3;
	    	context.shadowColor   = 'rgba(10, 10, 40, 0.5)';
			context.lineStyle = "rgb(0,0,0)";
			context.lineWidth = 1;
			context.fillStyle = this.colorFlecha;
			context.beginPath();
			context.moveTo(x1, y1);
			context.lineTo(x4, y4);
			context.lineTo(x5, y5);
			context.closePath();
			context.stroke();
			context.fill();
	    	context.restore();
	    	
	    	// Line Text
	    	/*
	    	var lineText1 = line.label;
	    	if (lineText1.length > 0) {
	    		var label = lineText1;
		    	context.save();
		    	var fontSize = 10 * modeler.zoom() / 100;
		    	var fontStyle = fontSize + "px Arial";
				context.textAlign = "center";
				context.textBaseline = "middle";
				context.fillStyle = this.colorTexto;
				context.font = fontStyle;
				var x1 = modeler.toCanvas(points[0].x);
				var y1 = modeler.toCanvas(points[0].y);
				var x2 = modeler.toCanvas(points[1].x);
				var y2 = modeler.toCanvas(points[1].y);
				var textWidth = this.distanceBetweenPoints(x1,y1,x2,y2) - d;
				var textHeight = context.measureText(label).height;
				var tx, ty;
				var O;
				if (x1 == x2) {
					O = -Math.PI / 2.0;					
					tx = x1 - fontSize;
					if (y2 > y1) { 
						ty = y1 + fontSize / 2 + textWidth / 2;
					} else {
						ty = y1 - fontSize / 2 - textWidth / 2; 
					}
				} else {
					O = this.angleBetweenPoints(x1,y1,x2,y2);
					var xm = x1 + (fontSize + textWidth / 2) * Math.cos(O);
					var ym = y1 + (fontSize + textWidth / 2) * Math.sin(O);
					if (O >= -Math.PI / 2 && O <= Math.PI / 2) {
						var O2 = O - Math.PI / 2;
						tx = xm + fontSize * Math.cos(O2);
						ty = ym + fontSize * Math.sin(O2);
					} else {
						var O2 = O + Math.PI / 2;
						tx = xm + fontSize * Math.cos(O2);
						ty = ym + fontSize * Math.sin(O2);
						O = O + Math.PI; 
						if (O > 2 * Math.PI) O = O - 2 * Math.PI;
					}
				}
				context.translate(tx, ty);
				context.rotate(O);
				context.textAlign = "center";
				context.fillText(label, 0, 0);
		    	context.restore();
	    	}
	    	*/   	
		},
		editProperties:function(line, modeler) {
		},
	});
}

var ZDEVNavegacion = {
	elementHandlers:{},
	lineHandlers:{referencia:new ZDEVNavegacionLineHandler("rgb(222, 242, 229)", "black"), contiene:new ZDEVNavegacionLineHandler("rgb(19, 20, 22)", "gray")},
	imagesCache:{},
	getLinesFromElement:function(element, model) {
		var lines = new Array();
		$.each(model.lines, function(i, line) {
			if (line.srcId == element.id) lines.push(line);
		});
		return lines;
	},
	getLinesToElement:function(element, model) {
		var lines = new Array();
		$.each(model.lines, function(i, line) {
			if (line.trgId == element.id) lines.push(line);
		});
		return lines;
	},
	addElement:function(model, element) {
		model.elements[element.id] = element;
		return element;
	},
	addLine:function(model, line) {
		model.lines[line.id] = line;
		return line;
	},
	getIncommingLine:function(thing, model) {
		var lines = this.getLinesToElement(thing, model);
		if (!lines || lines.length == 0) return null;
		return lines[0];
	},
	getPriorThing:function(thing, model) {
		var lines = this.getLinesToElement(thing, model);
		if (!lines || lines.length == 0) return null;
		return model.elements[lines[0].srcId];
	},
	getNextThings:function(thing, model) {
		var lines = this.getLinesFromElement(thing, model);
		if (!lines || lines.length == 0) return [];
		var things = new Array();
		$.each(lines, function(i, line) {
			things.push(model.elements[line.trgId]);
		});
		return things;
	},
	deleteElement:function(model, thingId) {
		var thisObject = this;
		var thing = model.elements[thingId];
		if (!thing) console.log("No se encontro la cosa " + thingId);
		var linesFrom = this.getLinesFromElement(thing, model);
		$.each(linesFrom, function(i, line) {
			delete model.lines[line.id];
		});
		// Delete lines to thing
		var linesTo = this.getLinesToElement(thing, model);
		$.each(linesTo, function(i, line) {
			delete model.lines[line.id];
		});
		// Delete thing
		delete model.elements[thingId];
	},	
	deleteLine:function(model, lineId) {
		var thisObject = this;		
		delete model.lines[lineId];
	},	
	findElementsByType:function(model, type) {
		var thisObject = this;
		var ret = new Array();
		$.each(model.elements, function(id, e) {
			if (e.type == type) ret.push(e);
		});
		return ret;
	},
	getHandler:function(element) {
		if (!element) return null;
		return this.elementHandlers[element.type];
	}
}

// Paneles
ZDEVNavegacion.elementHandlers["panel"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "rgb(247, 252, 248)";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 0);
		
		context.save();
    	
    	// Nombre
    	var x0 = modeler.toCanvas(element.x);
    	var y0 = modeler.toCanvas(element.y);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + element.height);    	
    	var fontStyle = (10 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "black";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
		
    	context.restore();	
	}
});

//Ventana
ZDEVNavegacion.elementHandlers["ventana"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "rgb(255, 251, 247)";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 10);
		
		context.save();
    	
    	// Nombre
    	var x0 = modeler.toCanvas(element.x);
    	var y0 = modeler.toCanvas(element.y);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + element.height);    	
    	var fontStyle = (10 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "black";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
		
    	context.restore();	
	}
});

//Página
var imgPagina = new Image();
imgPagina.src = "img/svg/pagina.svg";
ZDEVNavegacion.imagesCache["pagina"] = imgPagina;
ZDEVNavegacion.elementHandlers["pagina"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "rgb(172, 193, 229)";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 10);
		
		context.save();
		// Icono
		var x0 = modeler.toCanvas(element.x + 10);
    	var y0 = modeler.toCanvas(element.y + 30);
    	var width = modeler.toCanvas(element.width - 20);
    	var height = modeler.toCanvas(element.height - 30 - 10);
    	context.drawImage(ZDEVNavegacion.imagesCache["pagina"], x0, y0, width, height);
    	
    	// Nombre
    	x0 = modeler.toCanvas(element.x);
    	y0 = modeler.toCanvas(element.y + 15);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + 18);    	
    	var fontStyle = (12 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "black";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
		
    	context.restore();	
	}
});

//Loader
ZDEVNavegacion.elementHandlers["loader"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "rgb(12, 84, 153)";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 10);
		context.save();		

    	// Nombre
    	x0 = modeler.toCanvas(element.x);
    	y0 = modeler.toCanvas(element.y);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + element.height);    	
    	var fontStyle = (10 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "white";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
    	
    	context.restore();	
	}
});

//Pager
ZDEVNavegacion.elementHandlers["pager"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "rgb(8, 86, 39)";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 10);
		context.save();		

    	// Nombre
    	x0 = modeler.toCanvas(element.x);
    	y0 = modeler.toCanvas(element.y);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + element.height);    	
    	var fontStyle = (10 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "white";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
    	
    	context.restore();	
	}
});

//Fantasma
var imgFantasma = new Image();
imgFantasma.src = "img/svg/fantasma.svg";
ZDEVNavegacion.imagesCache["fantasma"] = imgFantasma;
ZDEVNavegacion.elementHandlers["fantasma"] = $.extend(true, new ZDEVNavegacionElementHandler(), {
	paint:function(element, context, modeler) {
		var fillStyle = "white";
		var strokeStyle = "black";
		this.paintBorder(element, context, modeler, fillStyle, strokeStyle, 10);
		context.save();		

		// Icono
		var x0 = modeler.toCanvas(element.x + 5);
    	var y0 = modeler.toCanvas(element.y + 5);
    	var width = modeler.toCanvas(element.height - 10);
    	var height = modeler.toCanvas(element.height - 10);
    	context.drawImage(ZDEVNavegacion.imagesCache["fantasma"], x0, y0, width, height);
    	
    	// Nombre
    	x0 = modeler.toCanvas(element.x + element.height);
    	y0 = modeler.toCanvas(element.y);
    	var x1 = modeler.toCanvas(element.x + element.width);
    	var y1 = modeler.toCanvas(element.y + element.height);    	
    	var fontStyle = (10 * modeler.zoom() / 100) + "px Arial";
    	context.fillStyle = "gray";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = fontStyle;
		context.fillText(element.name, (x0 + x1) / 2, (y0 + y1) / 2);
    	
    	context.restore();	
	}
});


