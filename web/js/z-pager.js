$.widget("z.zPager", {
	options:{pages:[], animate:false, duration:400},
	_create:function() {
		var $this = this.element;
		var thisWidget = this;
		if (!$this.attr("id")) {
			$.error("zPager must have a unique id");
		}
		this.showLoading();
		$this.data("zPager", {pages:{}, toLoad:0, stack:[] });
		$this.data("on-layout-resize", function() {thisWidget.doLayout();});
		$.each(this.options.pages, function(i, p) {
			$this.data("zPager").pages[p.code] = {};
			// Read html
			$this.data("zPager").toLoad++;
			$.ajax({
				url:p.url + ".html",
				data:null,
				success:function(html) {
					$this.data("zPager").pages[p.code].html = html;
					$this.data("zPager").toLoad--;
					if ($this.data("zPager").toLoad == 0) {
						thisWidget.initPages();
					}
				},
				error:function(xhr, errorText, errorThrown) {
					$.error("Error:" + errorThrown);
				},
				cache:false,
				dataType:"html"
			});
			// Read js
			$this.data("zPager").toLoad++;
			$.ajax({
				url:p.url + ".js",
				data:null,
				success:function() {
					$this.data("zPager").pages[p.code].controller = controller;
					$this.data("zPager").toLoad--;
					if ($this.data("zPager").toLoad == 0) {
						thisWidget.initPages();
					}
				},
				error:function(xhr, errorText, errorThrown) {
					$.error("Error:" + errorThrown);
				},
				dataType:"script"
			});
		});
		return this;
	},
	initPages:function() {
		var $this = this.element;
		var thisWidget = this;
		var opts = this.options;
		var pages = $this.data("zPager").pages;
		var initial = null;
		var id = $this.attr("id");
		$this.html("");
		$.each(opts.pages, function(i, p) {
			if (p.initial) initial = p.code;
			$this.append("<div id='h-" + p.code + "' style='position:absolute;left:0;top:0;display:none;'>" + pages[p.code].html + "</div>");
			initChildLayout($this.find("#h-" + p.code).children()[0]);
			if (pages[p.code].controller.init) {
				pages[p.code].controller.init($this.find("#h-" + p.code), p.options);
			}
		});
		if (!initial) {
			$.error("No initial page found in zPager");
		}
		this.gotoPage(initial);
		$.unblockUI();
		if (this.options.onReady) this.options.onReady();
	},
	showLoading:function() {
		$.blockUI({
			message:"Cargando ...",
			css: { 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	        }
		});
	},
	_gotoPageWithNoAnimation:function(code) {
		var $this =this.element;
		var actual = $this.data("zPager").currentPage;
		var pages = $this.data("zPager").pages;
		if (!pages[code]) {
			$.error("No page '" + code + "' found in zPager");
		}
		var $this =this.element;
		var id = $this.attr("id");
		var actual = $this.data("zPager").currentPage;
		var pages = $this.data("zPager").pages;
		if (actual) {
			var actualPageHolder = $this.find("#h-" + actual);
			actualPageHolder.css("display", "none");
		}
		var newPageHolder = $this.find("#h-" + code);
		newPageHolder.css("display", "");
		var newPage = $(newPageHolder.children()[0]);
		newPage.css("position", "absolute");
		newPage.css("left", 0);
		newPage.css("top", 0);
		$this.data("zPager").currentPage = code;
		if (actual) {
			if ($this.data("zPager").pages[actual].controller.deactivated) {
				$this.data("zPager").pages[actual].controller.deactivated();
			}
		}
		if ($this.data("zPager").pages[code].controller.activated) {
			$this.data("zPager").pages[code].controller.activated();
		}
		this.doLayout();
	},
	_finishGotoPageWithAnimation:function(code) {
		var $this =this.element;
		var id = $this.attr("id");
		var actual = $this.data("zPager").currentPage;
		var pages = $this.data("zPager").pages;
		if (actual) {
			var actualPageHolder = $this.find("#h-" + actual);
			actualPageHolder.css("display", "none");
		}
		var newPageHolder = $this.find("#h-" + code);
		newPageHolder.css("display", "");
		var newPage = $(newPageHolder.children()[0]);
		newPage.css("position", "absolute");
		newPage.css("left", 0);
		newPage.css("top", 0);
		$this.data("zPager").currentPage = code;
		this.doLayout();		
	},
	gotoPage:function(code, animation, onReady) {
		if (!this.options.animate) {
			this._gotoPageWithNoAnimation(code);
			return;
		}
		var thisWidget = this;
		var $this =this.element;
		var actual = $this.data("zPager").currentPage;
		var pages = $this.data("zPager").pages;
		if (!pages[code]) {
			$.error("No page '" + code + "' found in zPager");
		}
		if (!animation || !actual) {
			this._gotoPageWithNoAnimation(code);
			return;
		}
		if (actual) {
			if ($this.data("zPager").pages[actual].controller.deactivated) {
				$this.data("zPager").pages[actual].controller.deactivated();
			}
		}
		if ($this.data("zPager").pages[code].controller.activated) {
			$this.data("zPager").pages[code].controller.activated();
		}
		$this.css("overflow", "hidden");
		var actualPageHolder = $this.find("#h-" + actual);
		actualPageHolder.css("display", "");
		actualPageHolder.css("position", "absolute");
		actualPageHolder.css("left", 0);
		actualPageHolder.css("top", 0);
		actualPageHolder.width($this.width());
		actualPageHolder.height($this.height());
		var actualPage = $(actualPageHolder.children()[0]);
		actualPage.css("position", "absolute");
		actualPage.width($this.width());
		actualPage.height($this.height());
		doChildLayout(actualPage);
		var newPageHolder = $this.find("#h-" + code);
		newPageHolder.css("display", "");
		newPageHolder.css("position", "absolute");
		newPageHolder.css("left", 0);
		newPageHolder.css("top", 0);
		newPageHolder.width($this.width());
		newPageHolder.height($this.height());
		var newPage = $(newPageHolder.children()[0]);
		newPage.css("position", "absolute");
		newPage.width($this.width());
		newPage.height($this.height());
		doChildLayout(newPage);
		if (animation == "left") {
			actualPageHolder.css("left", 0);
			newPageHolder.css("left", $this.width());
			actualPageHolder.animate({
				left:-1 * $this.width()
			}, {
				duration:thisWidget.options.duration,
				queue:false
			});
			newPageHolder.animate({
				left:0
			}, {
				duration:thisWidget.options.duration,
				queue:false,
				complete:function() {
					thisWidget._finishGotoPageWithAnimation(code);
					if (onReady) onReady();
				}
			});
		} else if (animation == "right") {
			actualPageHolder.css("left", 0);
			newPageHolder.css("left", -1 * $this.width());
			actualPageHolder.animate({
				left:$this.width()
			}, {
				duration:thisWidget.options.duration,
				queue:false
			});
			newPageHolder.animate({
				left:0
			}, {
				duration:thisWidget.options.duration,
				queue:false,
				complete:function() {
					thisWidget._finishGotoPageWithAnimation(code);
					if (onReady) onReady();
				}
			});
		} else {
			$.error("Animation '" + animation + "' not handled in zPager");
		}
	},
	controller:function(code) {
		if (!code) code = this.element.data("zPager").currentPage;
		return this.element.data("zPager").pages[code].controller;
	},
	currentPage:function(code) {
		if (!code) return this.element.data("zPager").currentPage;
		else this.gotoPage(code);
	},
	push:function(code, onReady) {
		var stack = this.element.data("zPager").stack;
		stack.push(this.currentPage());
		this.gotoPage(code, "left", onReady);
	},
	pop:function(onReady) {
		var stack = this.element.data("zPager").stack;
		var newCode = stack[stack.length - 1];
		stack.splice(stack.length - 1, 1);
		this.gotoPage(newCode, "right", onReady);
		return newCode;
	},
	canPop:function() {
		var stack = this.element.data("zPager").stack;
		return stack.length > 0;
	},
	clearStack:function() {
		this.element.data("zPager").stack = [];
	},
	doLayout:function() {
		var $this =this.element;
		var actual = $this.data("zPager").currentPage;
		if (!actual) return;
		var holder = $this.find("#h-" + actual);
		holder.css("left", 0);
		holder.css("top", 0);
		holder.width($this.width());
		holder.height($this.height());		
		var c = $(holder.children()[0]);
		c.width($this.width());
		c.height($this.height());
		doChildLayout(c);
	}
});	
