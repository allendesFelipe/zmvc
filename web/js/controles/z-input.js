registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-input",
	clase:"z",
	nombre:"input",
	aplicaEnNodo:function(n) {return n.tagName == "INPUT" && $(n).attr("id");},
	esUltimoNivel:function(n) {return true;},
	getPropPages:function(n) {
		return [
			{nombre:"Editor Input", codigo:"z-input"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"BS Form Control", codigo:"bs-form-control"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	isAutochange:function(n) {
		return $(n).attr("data-z-autochange-delay")?true:false;
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes())
			.concat(this.getMetodosValidacion())
			.concat([
				{nombre:"val()", pasteCode:"val()", help:"Retorna el valor del editor"},
				{nombre:"val(nuevoValor)", pasteCode:"val(nuevoValor)", help:"Asigna un valor al editor"}
			]);
			if (this.isAutochange(n)) {
				mets.push({nombre:"clear", pasteCode:"clear()", help:"Limpia el contenido del editor, sincronizando autochange"});
			}
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [				
			];
			if (this.isAutochange(n)) {
				evts.push({
					nombre:"change",
					pasteCode:"change()",
					help:"Disparado luego que el usuario ha modificado el contenido del editor y éste ha perdido el foco o se ha cumplido el tiempo de 'autochange' definido desde la última modificación."
				});
			} else {
				evts.push({
					nombre:"change",
					pasteCode:"change()",
					help:"Disparado luego que el usuario ha modificado el contenido del editor y éste ha perdido el foco."
				});
			}
			onOk(evts);
		});
	}
}));