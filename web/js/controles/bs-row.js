registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"bs-row",
	clase:"bs",
	nombre:"Fila BS",
	describe:function(n) {return $(n).hasClass("form-row")?"Fila en Formulario":"Fila BS"},
	aplicaEnNodo:function(n) {return $(n).hasClass("row") || $(n).hasClass("form-row");},
	getPropPages:function(n) {
		return [
			{nombre:"Bootstrap Row", codigo:"bs-row"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	}
}));