registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"bs-col",
	clase:"bs",
	nombre:"Columna BS",
	aplicaEnNodo:function(n) {
		var classes = " " + $(n).attr("class");
		return classes.indexOf(" col-") >= 0 || $(n).hasClass("col");
	},
	describe:function(n) {
		var d = "";
		var classes = this.getClasses(n);
		classes.forEach(c => {
			if (c == "col" || c.startsWith("col-")) d += " " + c;
		});
		if ($(n).hasClass("form-group")) d += " + form-group";
		if ($(n).hasClass("input-group")) d += " + input-group";
		return d;
	},
	getPropPages:function(n) {
		return [
			{nombre:"Bootstrap Column", codigo:"bs-col"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	}
}));