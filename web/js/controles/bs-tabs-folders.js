registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"bs-tabs-folders",
	clase:"bs",
	nombre:"Tabs Folders",
	aplicaEnNodo:function(n) {return n.tagName == "UL" && ($(n).hasClass("nav-tabs") || $(n).hasClass("nav-pills"));},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {
		return [
			{nombre:"Tabs Folders", codigo:"bs-tabs-folders"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = [];
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [];
			onOk(evts);
		});
	}
}));