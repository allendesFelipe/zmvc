registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"bs-tabs-pane",
	clase:"bs",
	nombre:"Página Tab",
	aplicaEnNodo:function(n) {return n.tagName == "DIV" && $(n).hasClass("tab-pane");},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {
		return [
			{nombre:"Tab Pane", codigo:"bs-tabs-pane"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = [];
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [];
			onOk(evts);
		});
	}
}));