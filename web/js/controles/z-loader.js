registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-loader",
	clase:"z",
	nombre:"loader",
	aplicaEnNodo:function(n) {return $(n).data("z-autoload");},
	esUltimoNivel:function(n) {return true;},
	getPropPages:function(n) {
		return [
			{nombre:"Loader Z-MVC", codigo:"z-loader"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getContratoPagina:function(pagina, basePath) {
		return new Promise((onOk, onError) => {
			var fullUrl = "/test/" + pagina.url + ".js";
			jQuery.get(fullUrl, function(data) {
			    var mets = [];
			    var evts = [];
			    var p = data.indexOf("contrato::{");
			    if (p > 0) {
			    	var p2 = data.indexOf("}", p);
			    	var j = data.substring(p+10, p2+1);
					try {
						contrato = JSON.parse(j);
						if (!contrato.metodos) contrato.metodos = [];
						if (!contrato.eventos) contrato.eventos = [];
						mets = mets.concat(contrato.metodos);
						evts = evts.concat(contrato.eventos);
					} catch(error) {
						console.log("error parseando contrato:", error);
						return contrato;
					}
			    }
				onOk({codigo:pagina.code, url:pagina.url, metodos:mets, eventos:evts});
			});	
		});
	},
	getContrato:function(n, basePath) {
		var self = this;
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes())
			.concat([
				{nombre:"load", pasteCode:"zMVC('load', 'url', function(controller) {})", help:"Carga el controlador en 'url' y retorna su controlador en la función de callback"},
				{nombre:"getController()", pasteCode:"zMVC('getController')", help:"Retorna el controlador del componente cargado"}
			]);
			var evts = [];
			
			var pages = [];
			try {
				pages = JSON.parse($(n).attr("data-z-ref-pages"));
			} catch(error) {
				var initialURL = $(n).attr("data-z-autoload");
				pages = [{code:"inicial", url:initialURL, initial:true}];
			}
			
			var promesas = [];
			pages.forEach((p) => {
				promesas.push(self.getContratoPagina(p, basePath));
			});
			Promise.all(promesas)
			.then((contratos) => {
				var id = $(n).prop("id");
				contratos.forEach((contrato) => {
					mets.push({nombre:"load('" + contrato.codigo + "')", pasteCode:"zMVC('load', '" + contrato.url + "')", help:"Cambia la página activa a '" + contrato.url + "'"});
					contrato.metodos.forEach((m) => mets.push({
						nombre:"[" + contrato.codigo + "]." + m,
						pasteCode:"zMVC('getController')." + m,
						help:"Método " + m + " en el controlador '" + contrato.url + "'"
					}));
					contrato.eventos.forEach((e) => evts.push({
						nombre:"[" + contrato.codigo + "]." + e,
						pasteCode:e,
						help:"Evento " + e + " recibido por pager '" + id + "' desde la página '" + contrato.codigo + "'"
					}));
				});
				onOk({id:id, metodos:mets, eventos:evts});
			});
		});
	}	
}));