registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"bs-alert",
	clase:"bs",
	nombre:"alert",
	describe:function(n) {return "alert " + this.getEstilo(n);},
	aplicaEnNodo:function(n) {return n.tagName == "DIV" && $(n).hasClass("alert");},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {
		return [
			{nombre:"Bootstrap alert", codigo:"bs-alert"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getEstilo:function(n) {
		var classes = this.getClasses(n);
		var found = "";
		classes.forEach(c => {
			if (c.startsWith("alert-")) {
				found = c;
				return false;
			}
		});
		return found;
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = [
				{nombre:"text(text)", pasteCode:"text(text)", help:"Asigna el contenido del alert como texto"},
				{nombre:"text()", pasteCode:"text()", help:"Retorna el contenido como texto del alert"},
				{nombre:"html(html)", pasteCode:"html(html)", help:"Asigna el contenido del alert como código html"},
				{nombre:"html()", pasteCode:"html()", help:"Retorna el contenido html del alert"}
			]
			.concat(this.getMetodosComunes());
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [];
			onOk(evts);
		});
	}
}));