registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-table",
	clase:"z",
	nombre:"table",
	aplicaEnNodo:function(n) {return n.tagName == "TABLE" && $(n).attr("id") && $(n).hasClass("table");},
	esUltimoNivel:function(n) {return true;},
	getPropPages:function(n) {
		return [
			{nombre:"Z-Table", codigo:"z-table"},
			{nombre:"Columnas", codigo:"z-table-columns"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	isAutochange:function(n) {
		return $(n).attr("data-z-autochange-delay")?true:false;
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes())			
			.concat([
				{nombre:"refresh()", pasteCode:"refresh()", help:"Indica a la tabla que debe refrescarse. Ésta responde con un getRows o getRowCount, dependiento del tipo de paginación"},
				{nombre:"update()", pasteCode:"update()", help:"Refresca la tabla (invoca a refresh) manteniendo la fila seleccionada"},
				{nombre:"updateRow(rowIndex, row)", pasteCode:"updateRow(rowIndex, row)", help:"Actualiza la fila indicada sin releer todos los datos. Se utiliza sólo en paginación local"},
				{nombre:"updateAllRows(rows)", pasteCode:"updateAllRows(rows)", help:"Actualiza todas las filas de la tabla. Aplica sólo para paginación local."},
				{nombre:"deleteRow(rowIndex)", pasteCode:"deleteRow(rowIndex)", help:"Elimina la fila seleccionada sin tener que releer todos los datos. Aplica sólo para paginación local"},
				{nombre:"getSelectedRowIndex()", pasteCode:"getSelectedRowIndex()", help:"Retorna el índice (cero based) de la fila seleccionada. Aplica sólo si hay selección de filas en la tabla"},
				{nombre:"getSelectedRow()", pasteCode:"getSelectedRow()", help:"Retorna la fila seleccionada. Aplica sólo si hay selección de filas en la tabla"},
				{nombre:"setSelectedRowIndex(rowIndex)", pasteCode:"setSelectedRowIndex(rowIndex, fireEvent)", help:"Selecciona la fila indicada por el índice. Se indica (true/false) si se debe disparar el evento de selección de fila"},
				{nombre:"getRowCount()", pasteCode:"getRowCount()", help:"Retorna el número total de filas. Aplica a paginación local y servidor"},
				{nombre:"showColumn(name)", pasteCode:"showColumn(name)", help:"Muestra la columna inidicada por el nombre"},
				{nombre:"hideColumn(name)", pasteCode:"hideColumn(name)", help:"Esconde la columna inidicada por el nombre"}
			]);
			if (this.isAutochange(n)) {
				mets.push({nombre:"clear", pasteCode:"clear()", help:"Limpia el contenido del editor, sincronizando autochange"});
			}
			onOk(mets);
		});
	},
	getNodoRaiz:function(n) {
		var nodo = n;
		while(nodo.parentNode) nodo = nodo.parentNode;
		return nodo;
	},
	getPaginator:function(n) {
		var pagId = $(n).attr("data-z-pager");
		if (!pagId) return null;		
		return $(this.getNodoRaiz()).find(pagId);
	},
	getPaginacion:function(n) {
		var p = this.getPaginator();
		if (!p) return "local";
		return $(p).attr("data-z-pagination");
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [
				{
					nombre:"rowSelected(rowIndex, row)",
					pasteCode:"rowSelected(rowIndex, row)",
					help:"Aplica sólo a tablas con fila seleccionable. Se dispara al cambiar la fila seleccionada"
				},
				{
					nombre:"editRequest(rowIndex, row)",
					pasteCode:"editRequest(rowIndex, row)",
					help:"Se ha seleccionado el ícono de edición o una columna definida como 'editable'. El evento se puede resolver abriendo una ventana de edición. Al finalizar, si la paginación es local, se puede usar tabla.updateRow para evitar la lectura de todas la filas"
				},
				{
					nombre:"deleteRequest(rowIndex, row)",
					pasteCode:"deleteRequest(rowIndex, row)",
					help:"Se ha seleccionado el ícono de eliminación. El evento se puede resolver abriendo una ventana de confirmación. Al finalizar, si la paginación es local, se puede usar tabla.deleteRow para evitar la lectura de todas la filas"
				},
				{
					nombre:"columnClick(rowIndex, columnName, row)",
					pasteCode:"columnClick(rowIndex, columnName, row)",
					help:"El usuario ha clickeado en una columna definida como 'clickable'"
				},
				{
					nombre:"extraEditControlClick(rowIndex, controlName, row)",
					pasteCode:"extraEditControlClick(rowIndex, controlName, row)",
					help:"El usuario ha clickeado en un ícono de control de edición extra. Cada fila puede incluir un campo llamado '_extraEditControls' con un arreglo de objetos {name:'nombre', icon:'check'}"
				}
			];
			if (this.getPaginacion(n) == "local") {
				evts.push({
					nombre:"getRows(callback)",
					pasteCode:"getRows(callback)",
					help:"Disparado por la tabla cuando requiere refrescarse. Aplica sólo a paginación local. Al obtenerse resultados, se debe invocar de vuelta a callback(rows)"
				});
			} else {
				evts.push({
					nombre:"countRows(callback)",
					pasteCode:"countRows(callback)",
					help:"Disparado por la tabla cuando requiere refrescarse. Aplica sólo a paginación en servidor. Al obtenerse resultados, se debe invocar de vuelta a callback(nRows)"
				});
				evts.push({
					nombre:"getPage(startRow, nRows, callback)",
					pasteCode:"countRows(callback)",
					help:"Disparado por la tabla cuando requiere refrescar una página. El indice startRow comienza en cero para la primera fila. Aplica sólo a paginación en servidor. Al obtenerse resultados, se debe invocar de vuelta a callback(rowsInPage)"
				});
			}
			onOk(evts);
		});
	}
}));