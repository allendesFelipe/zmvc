registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-pager",
	clase:"z",
	nombre:"pager",
	aplicaEnNodo:function(n) {return $(n).data("z-pages");},
	esUltimoNivel:function(n) {return true;},
	isAnimated:function(n) {
		return $(n).attr("data-z-animation-delay")?true:false;
	},
	getPropPages:function(n) {
		return [
			{nombre:"Pager Z-MVC", codigo:"z-pager"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getContratoPagina:function(pagina, basePath) {
		return new Promise((onOk, onError) => {
			var fullUrl = "/test/" + pagina.url + ".js";
			jQuery.get(fullUrl, function(data) {
			    var mets = [];
			    var evts = [];
			    var p = data.indexOf("contrato::{");
			    if (p > 0) {
			    	var p2 = data.indexOf("}", p);
			    	var j = data.substring(p+10, p2+1);
					try {
						contrato = JSON.parse(j);
						if (!contrato.metodos) contrato.metodos = [];
						if (!contrato.eventos) contrato.eventos = [];
						mets = mets.concat(contrato.metodos);
						evts = evts.concat(contrato.eventos);
					} catch(error) {
						console.log("error parseando contrato:", error);
						return contrato;
					}
			    }
				onOk({codigo:pagina.code, url:pagina.url, metodos:mets, eventos:evts});
			}).fail((err) => {
				console.log("error en get de página", err);
				// Se finaliza la promesa como resuelta para incluir contratos de otras páginas
				onOk({codigo:pagina.code, url:pagina.url, metodos:[], eventos:[]});
			});	
		});
	},
	getContrato:function(n, basePath) {
		var self = this;
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes())
			.concat([
				{nombre:"page", pasteCode:"zMVC('page')", help:"Retorna el código de la página activa"},
				{nombre:"page(codigo)", pasteCode:"zMVC('page', 'codigoPagina')", help:"Cambia la página activa por su código"},
				{nombre:"getPageController", pasteCode:"zMVC('getPageController')", help:"Retorna el controlador de la página activa"},
				{nombre:"getPageController(codigo)", pasteCode:"zMVC('getPageController', 'codigoPagina')", help:"Retorna el controlador de la página indicada por el código"},
				{nombre:"push(codigo)", pasteCode:"zMVC('push', 'codigoPagina', true)", help:"Cambia la página colocándola en el Stack"},
				{nombre:"pop", pasteCode:"zMVC('pop', true)", help:"Elimina la página activa del stack, volviendo a la anterior"}
			]);
			var evts = [];
			
			var pages = JSON.parse($(n).attr("data-z-pages"));
			
			var promesas = [];
			pages.forEach((p) => {
				promesas.push(self.getContratoPagina(p, basePath));
			});
			Promise.all(promesas)
			.then((contratos) => {
				var id = $(n).prop("id");
				contratos.forEach((contrato) => {
					mets.push({nombre:"page('" + contrato.codigo + "')", pasteCode:"zMVC('page', '" + contrato.codigo + "')", help:"Cambia la página activa a '" + contrato.url + "'"});
			    	mets.push({nombre:"push('" + contrato.codigo + "')", pasteCode:"zMVC('push', '" + contrato.codigo + "', true)", help:"Cambia la página activa a '" + contrato.url + "' colocándola en el stack"});
			    	mets.push({nombre:"getPageController('" + contrato.codigo + "')", pasteCode:"zMVC('getPageController', '" + contrato.codigo + "')", help:"Retorna el controlador de la página '" + contrato.url + "'"});
					contrato.metodos.forEach((m) => mets.push({
						nombre:"[" + contrato.codigo + "]." + m,
						pasteCode:"zMVC('getPageController', '" + contrato.codigo + "')." + m,
						help:"Método " + m + " en el controlador '" + contrato.url + "'"
					}));
					contrato.eventos.forEach((e) => evts.push({
						nombre:"[" + contrato.codigo + "]." + e,
						pasteCode:e,
						help:"Evento " + e + " recibido por pager '" + id + "' desde la página '" + contrato.codigo + "'"
					}));
				});
				onOk({id:id, metodos:mets, eventos:evts});
			})
			.catch((err) => onError(err));
		});
	}	
}));