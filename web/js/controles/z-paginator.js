registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-paginator",
	clase:"z",
	nombre:"z-paginator",
	aplicaEnNodo:function(n) {return n.tagName == "UL" && $(n).attr("id") && $(n).hasClass("pagination") && $(n).attr("data-z-pagination");},
	esUltimoNivel:function(n) {return true;},
	getPropPages:function(n) {
		return [
			{nombre:"Z-Pagination", codigo:"z-pagination"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = [];			
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [];			
			onOk(evts);
		});
	}
}));