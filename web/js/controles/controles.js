var defControles = {}

var controlAbstracto = {
	codigo:"?",
	clase:"?",
	nombre:"Indefinido",
	describe:function(n) {return null},
	aplicaEnNodo:function(n) {$.error("Debe sobreescribir aplicaEnNodo para '" + this.codigo + "'")},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {return [];},
	getMetodosComunes:function(n) {
		return [
			{nombre:"show()", pasteCode:"show()", help:"Muestra el Control"},
			{nombre:"hide()", pasteCode:"hide()", help:"Oculta el Control"}
		]
	},
	getMetodosValidacion:function(n) {
		return [
			{nombre:"clearValidation()", pasteCode:"clearValidation()", help:"Quita estilos y texto de validación"},
			{nombre:"setValidationOk()", pasteCode:"setValidationOk()", help:"Coloca estilo success y texto (opcional)"},
			{nombre:"setValidationError()", pasteCode:"setValidationError()", help:"Coloca estilo de error y texto (opcional)"}
		]
	},
	getMetodos:function(n, basePath) {
		return new Promise((onOk, onError) => {onOk([]);});
	},
	getEventos:function(n, basePath) {
		return new Promise((onOk, onError) => {onOk([]);});
	},
	getContrato:function(n, basePath) {
		var self = this;
		return new Promise((onOk, onError) => {
			Promise.all([this.getMetodos(n, basePath), this.getEventos(n, basePath)])
			.then((ret) => {
				var contrato = {
					id:$(n).prop("id"),
					nombre:self.nombre,
					metodos:ret[0],
					eventos:ret[1]
				};
				onOk(contrato);
			});			
		});
	},
	getClasses:function(n) {
		var classAttr = $(n).attr("class");
		if (!classAttr) return [];
		return classAttr.split(/\s+/);
	}
}
function registraControl(handler) {
	defControles[handler.codigo] = handler;
}

function getHandler(codigo) {
	return defControles[codigo];
}

function getHandlers() {
	var keys = Object.keys(defControles);
	var handlers = [];
	for (var i=0; i<keys.length; i++) {
		var codigo = keys[i];
		handlers.push(defControles[codigo]);
	}
	return handlers;
}

function analizaTipoControl(nodo) {
	var found = null;
	$.each(getHandlers(), function(i, h) {
		if (h.codigo != "html" && h.aplicaEnNodo(nodo)) {
			found = h.codigo;
			return false;
		}
	});
	if (!found) return "html";
	return found
}
