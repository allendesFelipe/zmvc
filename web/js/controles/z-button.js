registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"z-button",
	clase:"z",
	nombre:"button",
	aplicaEnNodo:function(n) {return n.tagName == "BUTTON" && $(n).attr("id");},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {
		return [
			{nombre:"Botón", codigo:"z-button"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes());
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [
				{
					nombre:"click",
					pasteCode:"click()",
					help:"Disparado luego que el usuario ha clickeado el botón."
				}
			];
			onOk(evts);
		});
	}
}));