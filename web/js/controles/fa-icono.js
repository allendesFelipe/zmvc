registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"fa-icono",
	clase:"fa",
	nombre:"Ícono Font Awsome",
	aplicaEnNodo:function(n) {
		return (n.tagName == "SPAN" || n.tagName == "I") && 
				($(n).hasClass("fa") || $(n).hasClass("fab") || $(n).hasClass("fas") || $(n).hasClass("far") || $(n).hasClass("fal"));
	},
	esUltimoNivel:function(n) {return true;},
	getPropPages:function(n) {
		return [
			{nombre:"Ícono - FA", codigo:"fa-icono"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = []
			.concat(this.getMetodosComunes());
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [
				{
					nombre:"click",
					pasteCode:"click()",
					help:"Disparado luego que el usuario ha clickeado el ícono."
				}
			];
			onOk(evts);
		});
	}
}));