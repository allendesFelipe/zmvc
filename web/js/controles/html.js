registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"html",
	clase:"html",
	nombre:"elemento HTML",
	describe:function(n) {return n.tagName},
	getPropPages:function(n) {
		return [
			{nombre:"HTML", codigo:"html"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"}
		];
	},
	aplicaEnNodo:function(n) {return true;},
	getEventos:function(n, basePath) {
		return new Promise((onOk, onError) => {
			var evts = [];
			if (n.tagName == "A") {
				evts.push({
					nombre:"click",
					pasteCode:"click()",
					help:"Disparado luego que el usuario ha clickeado el link."
				});
			}
			onOk(evts);
		});
	},
}));