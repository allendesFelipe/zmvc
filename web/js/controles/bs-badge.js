registraControl($.extend(true, {}, controlAbstracto, {
	codigo:"badge",
	clase:"bs",
	nombre:"badge",
	describe:function(n) {return "badge " + this.getEstilo(n);},
	aplicaEnNodo:function(n) {return n.tagName == "SPAN" && $(n).hasClass("badge");},
	esUltimoNivel:function(n) {return false;},
	getPropPages:function(n) {
		return [
			{nombre:"Bootstrap badge", codigo:"bs-badge"},
			{nombre:"Márgenes Bootstrap", codigo:"margins"},
			{nombre:"Paddings Bootstrap", codigo:"paddings"},
			{nombre:"Estilos Bootstrap", codigo:"bs-estilos"},
			{nombre:"HTML", codigo:"html"}
		];
	},
	getEstilo:function(n) {
		var classes = this.getClasses(n);
		var found = "";
		classes.forEach(c => {
			if (c.startsWith("badge-")) {
				found = c;
				return false;
			}
		});
		return found;
	},
	getMetodos:function(n) {
		return new Promise((onOk, onError) => {
			var mets = [
				{nombre:"text(text)", pasteCode:"text(text)", help:"Asigna el contenido del badge como texto"},
				{nombre:"text()", pasteCode:"text()", help:"Retorna el contenido como texto del badge"}
			]
			.concat(this.getMetodosComunes());
			onOk(mets);
		});
	},
	getEventos:function(n) {
		return new Promise((onOk, onError) => {
			var evts = [];
			onOk(evts);
		});
	}
}));