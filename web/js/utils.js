function clearSelection() {
  var sel ;
  if(document.selection && document.selection.empty){
    document.selection.empty() ;
  } else if(window.getSelection) {
    sel=window.getSelection();
    if(sel && sel.removeAllRanges)
      sel.removeAllRanges() ;
  }
}

function enableButton(button, enabled) {
	var b = $(button);
	if (enabled) {
		b.removeAttr("disabled"); 
		b.removeClass("ui-state-disabled");
	} else {
		b.attr("disabled", "disabled");
		b.blur();
		b.removeClass("ui-state-hover");		
		b.addClass("ui-state-disabled");
	}
}

function isButtonEnabled(button) {
	var b = $(button);
	if (b.attr("disabled")) return false;
	return true;
}

function formatDateDDMMYYYY(dt, sep) {
	if (sep == undefined) sep = "/";
	var dd = "" + dt.getDate();
	if (dd.length < 2) dd = "0" + dd;
	var mm = "" + (dt.getMonth() + 1);
	if (mm.length < 2) mm = "0" + mm;
	var yyyy = "" + dt.getFullYear();
	return dd + sep + mm + sep + yyyy;
}

function formatDateDDMMMYYYY(dt, sep) {
	var meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
	if (sep == undefined) sep = "/";
	var dd = "" + dt.getDate();
	if (dd.length < 2) dd = "0" + dd;
	var mm = meses[dt.getMonth()];
	var yyyy = "" + dt.getFullYear();
	return dd + sep + mm + sep + yyyy;
}

function formatTimeHHMM(dt) {
	var hh = "" + dt.getHours();
	if (hh.length < 2) hh = "0" + hh;
	var mm = "" + dt.getMinutes();
	if (mm.length < 2) mm = "0" + mm;
	return hh + ":" + mm;
}

function dateTime2Object(dt) {
	return {
		ano:dt.getFullYear(),
		mes:dt.getMonth(),
		dia:dt.getDate(),
		hora:dt.getHours(),
		minutos:dt.getMinutes(),
		segundos:dt.getSeconds()
	};
}
function date2Object(dt) {
	return {
		ano:dt.getFullYear(),
		mes:dt.getMonth(),
		dia:dt.getDate()
	};
}
function object2DateTime(o) {
	return new Date(o.ano, o.mes, o.dia, o.hora, o.minutos, o.segundos);
}
function object2Date(o) {
	return new Date(o.ano, o.mes, o.dia, 0, 0, 0);
}

function nombreMes(dt) {
	var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
	return meses[dt.getMonth()];
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)  {
            return sParameterName[1];
        }
    }
}

function getUrlParameters() {
	var params = {};
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        params[sParameterName[0]] = sParameterName[1];
    }
    return params;
}

function round2(n) {
	return Math.round(n * 100) / 100.0;
}

function formateaMoneda(x) {
	if(x!=null)
		return "$" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	else
		return "";
}

function formateaPorcentaje(x) {
	if(x!=null)
		return (x.toString().length>=5?x.toString().substring(0,5):x.toString()) + "%";
	else
		return "";
}

function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function getRelativePath(source, target) {
	var sep = (source.indexOf("/") !== -1) ? "/" : "\\",
		targetArr = target.split(sep),
		sourceArr = source.split(sep),
		filename = targetArr.pop(),
		targetPath = targetArr.join(sep),
		relativePath = "";
	
	while (targetPath.indexOf(sourceArr.join(sep)) === -1) {
		sourceArr.pop();
		relativePath += ".." + sep;
	}
	
	var relPathArr = targetArr.slice(sourceArr.length);
	relPathArr.length && (relativePath += relPathArr.join(sep) + sep);
	
	return relativePath + filename;
}
