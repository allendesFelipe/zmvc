var createSubMenu = function(parentItem, html) {
	html += "<div id='" + parentItem.code + "' style='display:none'>\n";
	if (parentItem.submenuLabel) {
		html += "<a rel='title'>" + parentItem.submenuLabel + "</a>\n"; 
		html += "<a data-type='separator'> </a>";
	}
	for (var i=0; i<parentItem.items.length; i++) {
		var item = parentItem.items[i];
		var clickHandler = "";
		if (item.code == "-") {
			html += "<a data-type='separator'> </a>";
		} else {
			if (item.items) {
				clickHandler = "";
			} else {
				clickHandler = "action:'app.mainMenuHandler(\\'" + item.code + "\\')'";
			}
			html += "<a id='" + item.code + "-item' href='#' ";
			if (item.icon) html += " img='" + item.icon + "'";
			if (item.items) {
				html += " menu='" + item.code + "'";
			} else {
				html += " action='app.mainMenuHandler(\"" + item.code + "\")'";
			}
			html += " >" + item.label + "</a>\n";
		}
	}
	html += "</div>\n";
	
	for (var i=0; i<parentItem.items.length; i++) {
		var item = parentItem.items[i];
		if (item.items) html = createSubMenu(item, html);
	}
	return html;
};
var paintMenu = function(elementId, options, clickHandler) {
	app.mainMenuHandler = clickHandler;
	var html = "<table class='rootVoices' cellspacing='0' cellpadding='0' border='0'><tr>\n";
	for (var i=0; i<options.length; i++) {
		var op = options[i];
		if (op.items) {
			html += '<td class="rootVoice {menu: \'' + op.code  + '\'}" >' + op.label + '</td>\n';
		} else {
			html += '<td class="rootVoice {menu: \'empty\'}" onclick="app.mainMenuHandler(\'' + op.code + '\')" >' + op.label + '</td>\n';
		}
	}
	html += "</tr></table>\n";
	for (var i=0; i<options.length; i++) {
		var op = options[i];
		if (op.items) html = createSubMenu(op, html);
	}
	$("#" + elementId).html(html);
	$("#" + elementId).buildMenu({
      additionalData:"",
      menuSelector:".menuContainer",
      menuWidth:200,
      openOnRight:false,
      containment:"window",
      iconPath:"img/menu/",
      hasImages:true,
      fadeInTime:100,
      fadeOutTime:200,
      menuTop:0,
      menuLeft:0,
      submenuTop:0,
      submenuLeft:4,
      opacity:1,
      shadow:false,
      shadowColor:"black",
      shadowOpacity:.2,
      openOnClick:false,
      closeOnMouseOut:true,
      closeAfter:500,
      minZindex:"auto",
      hoverIntent:0, 
      submenuHoverIntent:0, 
  	});
};

function enableMenuItem(menuId, itemCode, enabled) {
	var item = $("#" + menuId).find("#" + itemCode + "-item");
	if (enabled) item.removeAttr("isdisable");
	else item.attr("isdisable", "true");
}