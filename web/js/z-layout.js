function _zParseLayout(def, element) {
	if (!def) {
		$.error("No 'data-layout' found in element: " + (element.attr("id")?"id=" + element.attr("id"):element.html()));
	}
	def = def.toLowerCase();
	var layoutType;
	if (def.substring(0,4) == "rows") layoutType="rows";
	else if (def.substring(0,4) == "cols") layoutType="cols";
	else $.error("data-layout must start with 'rows' or 'cols' in element " + element);
	def = def.substring(4);
	if (def.substring(0,1) != "(" || def.substr(def.length-1, 1) != ")") $.error("invalid data-layout syntax. Not delimited by '(' and ')' in element " + element);
	var splitted = def.substr(1).substr(0,def.length - 2).split(",");
	var sizes = [];
	$.each(splitted, function(i, s) {
		var sizeDef, alignDef;
		var splitted2 = s.split(":");
		if (splitted2.length == 1) {
			sizeDef = splitted2[0];
			alignDef = "full";
		} else {
			sizeDef = splitted2[0];
			alignDef = splitted2[1];
		}
		if (sizeDef.substr(sizeDef.length-2) == "px") {
			sizes.push({type:"fixed", value:parseInt(sizeDef), align:alignDef});
		} else if (sizeDef.substr(sizeDef.length-1) == "%") {
			sizes.push({type:"percent", value:parseInt(sizeDef), align:alignDef});
		} else {
			$.error("Can't interpret '" + sizeDef + "' as a size specificaton in " + element);
		}
	});
	return {type:layoutType, sizes:sizes};
}

function getChildMargins(c) {
	var m = $(c).data("layout-mg");
	if (m) return m;
	if ($(c).data("layout-margin")) {
		var n = parseInt($(c).data("layout-margin"));
		if (isNaN(n)) {
			$.error("Invalid 'data-layout-margin'. Value '" + $(c).data("layout-margin") + "' should be an integer");
		}
		m = [n, n, n, n];
		$(c).data("layout-mg", m);
		return m;
	} else if ($(c).data("layout-margins")) {
		var nn = $(c).data("layout-margins").split(",");
		if (!nn || nn.length != 4) {
			$.error("Invalid 'data-layout-margins'. Value '" + $(c).data("layout-margins") + "' should be four integers, in the form 'left, top, right, bottom'");
		}
		m = [];
		$.each(nn, function(i, n) {
			n = parseInt(n);
			if (isNaN(n)) {
				$.error("Invalid 'data-layout-margins'. Value '" + $(c).data("layout-margins") + "' should be four integers, in the form 'left, top, right, bottom'");
			}
			m.push(n);
		});
		$(c).data("layout-mg", m);
		return m;
	} else {
		m = [0,0,0,0];
		$(c).data("layout-mg", m);
		return m;
	}
}

function isDivider(c) {
	return $(c).data("divider-direction") || $(c).data("divider-initial-position") || $(c).data("divider-width") || $(c).data("divider-class");
}
function isSelector(c) {
	return $(c).data("selector-initial");
}
function isLayout(c) {
	return $(c).data("layout");
}
function isMaximizable(c) {
	return $(c).data("maximizable");
}
function isAutoload(c) {
    return $(c).data("layout-autoload");
}
function isScroller(c) {
    return $(c).data("scroller-min-height") || $(c).data("scroller-min-width");
}
function isSidePanel(c) {
	return $(c).data("side-panel-width");
}
function isOverlay(c) {
	return $(c).data("overlay-margin") || $(c).data("overlay-margins") || $(c).data("overlay-width") || $(c).data("overlay-height") || $(c).data("overlay-vertical-margin") || $(c).data("overlay-horizontal-margin");
}

function playsLayout(c) {
	return isLayout(c) || isSelector(c) || isDivider(c) || isMaximizable(c) || isAutoload(c) || isScroller(c) || isSidePanel(c) || isOverlay(c);
}
function isLayoutInitialized(c) {
	return $(c).data("layout-initialized");
}
function markLayoutInitialized(c) {
	$(c).data("layout-initialized", true);
}
function initChildLayout(c) {
	if (isLayoutInitialized(c)) return;
	if (isLayout(c)) $(c).zLayout();
	if (isDivider(c)) $(c).zDivider();
	if (isSelector(c)) $(c).zSelector();
	if (isMaximizable(c)) $(c).zMaximizable();
	if (isAutoload(c)) zAutoload(c);
	if (isScroller(c)) zScroller(c);
	if (isSidePanel(c)) $(c).zSidePanel();
	if (isOverlay(c)) $(c).zOverlay();
}

function whenAutoloadReady(autoloadElement, callback) {
    var c = autoloadElement;
    var autoloadId = $(c).attr("id");
    if (!autoloadId) {
    	$.error("autoload without id", c);
    }
    if (isLayoutInitialized(c)) callback();    
    else $(c).data("al-callback", callback);
}
function zAutoload(c) {
	if ($(c).data("al-mvc")) return;
	$(c).data("al-mvc", {html:null, controller:null});
    var path = $(c).data("layout-autoload");
    $.ajax({
        url: path + ".html",
        data: null,
        success: function (html) {
        	$(c).data("al-mvc").html = html;
        	if ($(c).data("al-mvc").controller) finishAutoload(c);
        },
        error: function (xhr, errorText, errorThrown) {
            $.error("Error:" + errorThrown);
        },
        cache: false,
        dataType: "html"
    });
    $.ajax({
        url: path + ".js",
        data: null,
        success: function () {
        	$(c).data("al-mvc").controller = controller;
        	if ($(c).data("al-mvc").html) finishAutoload(c);
        },
        error: function (xhr, errorText, errorThrown) {
            $.error("Error:" + errorThrown);
        },
        dataType: "script"
    });
}
function finishAutoload(c) {
	var mvc = $(c).data("al-mvc");
    $(c).html(mvc.html);
    $(c).data("controller", mvc.controller);
    markLayoutInitialized(c);
    if (mvc.controller.init) mvc.controller.init($(c), {});
    doChildLayout(c);
    if ($(c).data("al-callback")) {
    	($(c).data("al-callback"))();
    	$(c).data("al-callback", null);
    }
}
function zScroller(c) {
    $(c).css("overflow", "auto");
    var children = $(c).children();
    if (!children || children.length == 0) {
        $.error("Scroller has no children");
        return;
    }
    if (children.length > 1) {
        $.error("Scroller has " + children.length + " children");
        return;
    }
    var c1 = children[0];
    initChildLayout(c1);
}
var zScrollbarWidth = 20;
var zScrollbarHeight = 20;
function doZScrollLayout(c) {
    var c1 = $(c).children()[0];
    $(c1).css("position", "absolute");
    $(c1).css("left", 0);
    $(c1).css("top", 0);

    var h = $(c).height()-3;
    var w = $(c).width()-3;

    var minH = $(c).data("scroller-min-height");
    var vScroll = false;
    if (minH && h < minH) {
        h = minH;
        vScroll = true;
    }
    var minW = $(c).data("scroller-min-width");
    var hScroll = false;
    if (minW && w < minW) {
        w = minW;
        hScroll = true;
    }
    // Add scrollbars
    if (vScroll && !hScroll) {
        w -= zScrollbarWidth;
        if (minW && w < minW) {
            w = minW;
            hScroll = true;
        }
    }
    if (hScroll && !vScroll) {
        h -= zScrollbarHeight;
        if (minH && h < minH) {
            h = minH;
            vScroll = true;
        }
    }
    $(c1).height(h);
    $(c1).width(w);

    doChildLayout(c1);
}
function doChildLayout(c) {
	if ($(c).data("on-layout-resize")) {
		$(c).data("on-layout-resize")($(c).width(), $(c).height());
	}
	var r = false;
	if (playsLayout(c) && !isLayoutInitialized(c)) initChildLayout(c);
	if (isLayout(c)) {r=true; $(c).zLayout("doLayout")};
	if (isDivider(c)) {r=true; $(c).zDivider("doLayout")};
	if (isSelector(c)) {r=true; $(c).zSelector("doLayout")};
	if (isMaximizable(c)) { r = true; $(c).zMaximizable("doLayout") };
	if (isAutoload(c)) { r = true; doAutoloadLayout(c); }
	if (isScroller(c)) { r = true; doZScrollLayout(c);}
	if (isSidePanel(c)) {r=true; $(c).zSidePanel("doLayout");};
	if (isOverlay(c)) {r=true; $(c).zOverlay("doLayout");};
	return r;
}
function doAutoloadLayout(c) {
    if (!isLayoutInitialized(c)) return; // not loaded yet
    var children = $(c).children();
    if (!children || children.length == 0) {
        $.error("autoload has no children");
        return;
    }
    if (children.length > 1) {
        $.error("autoload has " + children.length + " children");
        return;
    }
    var c1 = children[0];
    $(c1).css("position", "absolute");
    $(c1).css("left", 0);
    $(c1).css("top", 0);
    $(c1).width($(c).width());
    $(c1).height($(c).height());
    doChildLayout(c1);
}

var isOldIE = document.all && !document.getElementsByClassName;
$.widget(
	"z.zLayout", {
		options:{
			layoutWithWindow:false,
			sizes:null
		},
		_create:function() {
			var thisWidget = this;
			if (this.options.layoutWithWindow) {
				$(window).resize(function(e) {
					if (e.target == window || isOldIE) {
						thisWidget.doLayout();
					}
				});
			}
			this.options.sizes = _zParseLayout(this.element.data("layout"), this.element);
			var children = this.element.children();
			if (children.length != this.options.sizes.sizes.length) {
				$.error("Invalid structure: " + this.options.sizes.sizes.length + " children expected, but " + children.length + " found in element " + (this.element.attr("id")?"id=" + this.element.attr("id"):this.element.html()));
			}
			markLayoutInitialized(this.element);
			this.doLayout();
			return this;
		},
		doLayout:function() {
			var thisW = this;
			var w = this.element.width();
			var h = this.element.height();
			var layoutType = this.options.sizes.type;
			var sizes = this.options.sizes.sizes;
			var fixedSum = 0, percentSum = 0;
			$.each(sizes, function(i, s) {
				if (s.type == "fixed") fixedSum += s.value;
				else if (s.type == "percent") percentSum += s.value;
			});
			var size2share = (layoutType=="rows"?h - fixedSum:w - fixedSum);
			var children = this.element.children();
			var acum = 0;
			$.each(children, function(i, c) {
				var s = sizes[i];
				if (!s) {
					console.log("Error in element", thisW.element);
					$.error("no size in controller for i=" + i, thisW.element);
				}
				var $c = $(c);
				$c.css("position", "absolute");
				var newSize;				
				if (i == (children.length - 1)) {
					newSize = layoutType == "cols"?w - acum:h - acum;
				} else {
					newSize = s.type == "fixed"?s.value:size2share * s.value / percentSum;
				}
				var m = getChildMargins($c);
				if (layoutType == "cols") {
					$c.css("left", acum + m[0]);
					$c.width(newSize - m[0] - m[2]);
					switch(s.align) {
						case "full":$c.css("top", m[1]);$c.height(h - m[1] - m[3]);break;
						case "top":$c.css("top", m[1]);break;
						case "bottom":$c.css("top", h - $c.height() - m[3]);break;
						case "center":$c.css("top", m[1] + (h - $c.height() - m[1] - m[3]) / 2);break;
					}
				} else {
					$c.css("top", acum + m[1]);
					$c.height(newSize - m[1] - m[3]);
					switch(s.align) {
						case "full":$c.css("left", m[0]);$c.width(w - m[0] - m[2]);break;
						case "left":$c.css("left", m[0]);break;
						case "right":$c.css("left", w - $c.width() - m[2]);break;
						case "center":$c.css("left", m[0] + (w - $c.width() - m[0] - m[2]) / 2);break;
					}
				}
				acum += newSize;
				doChildLayout(c);				
			});
		},
		setLayout:function(index, def) {
			var split = def.split(":");
			var alignDef = split.length==1?"full":split[1];
			var sizeDef = split[0];
			var size;
			if (sizeDef.substr(sizeDef.length-2) == "px") {
				size = {type:"fixed", value:parseInt(sizeDef), align:alignDef};
			} else if (sizeDef.substr(sizeDef.length-1) == "%") {
				size = {type:"percent", value:parseInt(sizeDef), align:alignDef};
			} else {
				$.error("Can't interpret '" + sizeDef + "' as a size specificaton setting layout");
			}
			this.options.sizes.sizes[index] = size;
			this.doLayout();
		}
	}
);