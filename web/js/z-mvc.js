var useZMVCCache = false;
var _zmvcPreprocessors = {};

function isRendered(domObj) {
	if (!domObj) return false;
    if ((domObj.nodeType != 1) || (domObj == document.body)) {
        return true;
    }
    if (domObj.currentStyle && domObj.currentStyle["display"] != "none" && domObj.currentStyle["visibility"] != "hidden") {
        return isRendered(domObj.parentNode);
    } else if (window.getComputedStyle) {
        var cs = document.defaultView.getComputedStyle(domObj, null);
        if (cs.getPropertyValue("display") != "none" && cs.getPropertyValue("visibility") != "hidden") {
            return isRendered(domObj.parentNode);
        }
    }
    return false;
}

function _zmvcProcessSyntaxError(scriptURL, error) {
	if (error instanceof SyntaxError) {
		alert("Syntax Error in '" + scriptURL + "'");
		var scriptTag = document.createElement('script');
		scriptTag.setAttribute('src', scriptURL);
		document.head.appendChild(scriptTag);
	} else {
		$.error("Error reading " + scriptURL + "\n" + error);
	}
}

function _zmvcRegisterPreprocessor(tagName, processor) {
	_zmvcPreprocessors[tagName] = processor;
}

function _zmvcLoadComponent(url, parentController, onReady) {
	var prefix = zUrlPrefix?zUrlPrefix:"";
	var htmlReady = false;
	var controllerReady = false;
	var ret ={html:null, controller:null};
	$.ajax({
		url:prefix + url + ".html",
		data:null,
		success:function(html) {
			ret.html = html;
			htmlReady = true;
			if (controllerReady) onReady(ret);
		},
		error:function(xhr, errorText, errorThrown) {
			$.error("Error:" + errorThrown);
		},
		cache:false,
		dataType:"html"
	});
	$.ajax({
		url:prefix + url + ".js",
		data:null,
		success:function() {
			ret.controller = _zmvcPrepareController(parentController, controller);
			controllerReady = true;
			if (htmlReady) onReady(ret);
		},
		error:function(xhr, errorText, errorThrown) {
			_zmvcProcessSyntaxError(url + ".js", errorThrown);
		},
		dataType:"script"
	});
}

function _zmvcPrepareController(parentController, controller) {
	var base = {
		parentController:parentController,
		_processEvent:function(controlName, eventName, eventData) {			
			var name = "on";
			name += controlName.substring(0,1).toUpperCase() + controlName.substring(1);
			name += "_" + eventName;
			if (this[name]) this[name](eventData);
		},
		isVisible:function() {
			return isRendered(this.view[0]);
		}
	};
	return $.extend(true, base, controller);
}

function _zmvcPlays(element) {
	var types = [];
	if (element.data("z-autoload")) types.push("autoload");
	if (element.data("z-pages")) types.push("pager");
	
	return types;
}

function _zmvcSearchSubcomponents(widget, controller, element) {
	$.each(element.children(), function(i, child) {
		var processor = _zmvcPreprocessors[$(child).prop("tagName").toLowerCase()];
		if (processor) processor($(child), controller);
		var childId = $(child).prop("id");
		var childTypes = _zmvcPlays($(child));
		if (childTypes.length) {
			if (!childId) $.error("Component should have an id:" + child.outerHTML);
			widget._incPendingsCounter();
			controller[childId] = $(child).zMVC({
				parentController:controller,
				onReady:function() {
					widget._decPendingsCounter();
				}
			});
		} else {
			if (childId) {
				controller[childId] = $(child);
			}
			_zmvcSearchSubcomponents(widget, controller, $(child));
		}
	});
}
function _zmvcAnalize(widget) {
	var types = _zmvcPlays(widget.element);
	widget.types = types;
	if (widget.hasType("autoload")) {
		var url = widget.element.data("z-autoload");
		widget._incPendingsCounter();
		_zmvcLoadComponent(url, widget.options.parentController, function(info) {
			widget.controller = info.controller;
			var view = $(info.html);
			widget.element.html(view);
			widget.view = view;
			widget.controller.view = widget.view;
			_zmvcSearchSubcomponents(widget, widget.controller, widget.element);
			widget._decPendingsCounter();
		});		
	}
	if (widget.hasType("pager")) {
		var pages = widget.element.data("z-pages");
		widget.pages = {};
		$.each(pages, function(i, page) {
			widget._incPendingsCounter();
			_zmvcLoadComponent(page.url, widget.options.parentController, function(info) {
				var view = $(info.html);
				var pageId = "_page_" + page.code;
				widget.element.append("<div id='" + pageId + "' class='page-holder'></div>");
				widget.element.find("#" + pageId).html(view);
				if (!page.initial) {
					widget.element.find("#" + pageId).hide();
				}
				info.controller.view = view;
				widget.pages[page.code] = {code:page.code, controller:info.controller, view:view, initial:page.initial?true:false};
				_zmvcSearchSubcomponents(widget, info.controller, view);
				widget._decPendingsCounter();
			});
		});
	}
	if (widget.options.isDialog) {
		widget._incPendingsCounter();
		widget.controller = widget.options.dialogInfo.controller;
		widget.view = widget.options.dialogInfo.view;
		widget.controller.view = widget.view;
		_zmvcSearchSubcomponents(widget, widget.controller, widget.element);
		widget._decPendingsCounter();
	}
}

function _zmvcInit(widget, onReady) {
	if (widget.hasType("autoload")) {
		widget.controller._processEvent("this", "init");
	} 
	if (widget.hasType("pager")) {
		var initial = null;
		$.each(widget.pages, function(i, p) {
			if (p.initial) {
				initial = p;
				return false;
			}
		});
		if (!initial) $.error("Pager '" + widget.element.prop("id") + "' does not define an initial page");
		widget.controller = initial.controller;
		widget.currentPage = initial.code;
		widget.view = initial.view;
		$.each(widget.pages, function(i, p) {
			p.controller._processEvent("this", "init");
		});
	}
	if (widget.options.isDialog) {
		widget.controller.close = function() {
			if (widget.options.dialogOnClose) {
				widget.options.dialogOnClose.apply(this, arguments);
			}
			widget.element.modal("hide");
		};
		widget.controller.cancel = function() {
			if (widget.options.dialogOnCancel) {
				widget.options.dialogOnCancel();
			}
			widget.element.modal("hide");
		};
		widget.controller._processEvent("this", "init", widget.options.dialogOptions);
		widget.view.modal({
			show:false,
			keyboard:true,
			backdrop:"static"
		});
		widget.view.modal("handleUpdate");
		widget.view.modal("show");
	}
	if (onReady) onReady();
}

function openDialog(url, options, onClose, onCancel) {	
	_zmvcLoadComponent(url, null, function(info) {
		var view = $(info.html);
		if (!view.prop("id")) view.prop("id", "dialog");
		info.view = view;		
		view.zMVC({isDialog:true, dialogInfo:info, dialogOptions:options, dialogOnClose:onClose, dialogOnCancel:onCancel});
	});	
}

$.widget(
	"z.zMVC", {
		types:[],
		controller:null,
		view:null,
		pendingsCounter:0,
		pendingsEndCallback:null,
		pages:null, 		// {code:{controller:controller, view:view, initial:bool}}
		currentPage:null, 	// code
		pagesStack:[],
		options:{
			isDialog:false,
			onReady:null,
			parentController:null,
			isDialog:false,
			dialogInfo:null,
			dialogOptions:null,
			dialogOnClose:null,
			dialogOnCancel:null
		},
		_create:function() {
			var thisWidget = this;
			this.pendingsEndCallback = this._initControllers;
			_zmvcAnalize(this);
		},
		_incPendingsCounter:function() {
			this.pendingsCounter++;
		},
		_decPendingsCounter:function() {
			this.pendingsCounter--;
			if (!this.pendingsCounter && this.pendingsEndCallback) this.pendingsEndCallback();
		},
		_initControllers:function() {			
			var thisWidget = this;
			_zmvcInit(this, function() {
				if (thisWidget.options.onReady) thisWidget.options.onReady(thisWidget.controller);
			});
		},
		hasType:function(type) {
			return this.types.indexOf(type) >= 0;
		},
		getController:function() {
			return this.controller;
		},
		getView:function() {
			return this.view;
		},
		
		// autoload
		load:function(url, onReady) {
			var thisWidget = this;
			this.pendingsEndCallback = function() {
				thisWidget.controller._processEvent("this", "init");
				if (onReady) onReady(thisWidget.controller);
			}
			this._incPendingsCounter();
			_zmvcLoadComponent(url, this.options.parentController, function(info) {
				thisWidget.controller = info.controller;
				var view = $(info.html);
				thisWidget.element.html(view);
				thisWidget.view = view;
				thisWidget.controller.view = view;
				_zmvcSearchSubcomponents(thisWidget, thisWidget.controller, thisWidget.element);
				thisWidget._decPendingsCounter();
			});
		},
		
		// pager
		page:function(code) {
			if (code === undefined) return this.currentPage;
			this.pagesStack = [];
			var p = this.pages[code];
			if (!p) $.error("page '" + code + "' not found in pager '" + this.element.prop("id") + "'");
			this.controller = p.controller;
			this.view = p.view;
			var pageId = "_page_" + code;
			this.element.find(".page-holder").hide();
			this.element.find("#" + pageId).show();
			this.currentPage = code;
			return this.controller;
		},
		getPageController:function(code) {
			if (code && !this.pages[code]) $.error("Page '" + code + "' not found in this pager");
			var p = this.pages[code?code:this.currentPage];
			return p.controller;
		},
		getPageView:function(code) {
			var p = this.pages[code?code:this.currentPage];
			return p.view;
		},
		push:function(code) {
			var p = this.pages[code];
			if (!p) $.error("page '" + code + "' not found in pager '" + this.element.prop("id") + "'");
			this.controller = p.controller;
			this.view = p.view;
			var pageId = "_page_" + code;
			this.element.find(".page-holder").hide();
			this.element.find("#" + pageId).show();
			this.currentPage = code;
			this.pagesStack.push(code);
		},
		pop:function() {
			this.pagesStack.splice(this.pagesStack.length - 1, 1);
			var code = this.pagesStack[this.pagesStack.length - 1];
			var p = this.pages[code];
			this.controller = p.controller;
			this.view = p.view;
			var pageId = "_page_" + code;
			this.element.find(".page-holder").hide();
			this.element.find("#" + pageId).show();
			this.currentPage = code;
		},
		
		// Overlays
		
	}
);
