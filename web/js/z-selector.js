$.widget(
	"z.zSelector", {
		options:{
			selected:null,
			childrenByCode:{}
		},
		_create:function() {
			var thisWidget = this;
			this.options.selected = this.element.data("selector-initial");
			if (!this.options.selected) {
				$.error("No 'data-selector-initial' in selector:" + this.element.html());
			}
			var children = this.element.children();
			$.each(children, function(i, c) {
				var code = $(c).data("selector-code");
				if (!code) {
					$.error("Element child of selector has no 'data-selector-code':" + $(c).html());
				}
				$(c).css("position", "absolute");
				thisWidget.options.childrenByCode[code] = $(c);
			});
			markLayoutInitialized(this.element);
		},
		doLayout:function() {
			var thisWidget = this;
			$.each(Object.keys(this.options.childrenByCode), function(i, code) {
				var c = thisWidget.options.childrenByCode[code];
				if (code == thisWidget.options.selected) {
					c.show();
					var margins = getChildMargins(c);
					c.css("left", margins[0]);
					c.css("top", margins[1]);
					c.width(thisWidget.element.width() - margins[0] - margins[2]);
					c.height(thisWidget.element.height() - margins[1] - margins[3]);
					doChildLayout(c);
				} else {
					thisWidget.options.childrenByCode[code].hide();
				}
			});
		},
		select:function(code) {
			var c = this.options.childrenByCode[code];
			if (!c) {
				$.error("No child of selector found with code '" + code + "'");
			}
			this.options.selected = code;
			this.doLayout();
		},
		getSelected:function() {
			return this.options.selected;
		},
		setSelected:function(code) {
			this.select(code);
		}
	}
)