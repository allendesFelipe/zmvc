var useZMVCCache = false;
var _zmvcPreprocessors = {};
var zMVCUrlPrefix = null;

function isRendered(domObj) {
    if (!domObj) return false;
    if ((domObj.nodeType != 1) || (domObj == document.body)) {
        return true;
    }
    if (domObj.currentStyle && domObj.currentStyle["display"] != "none" && domObj.currentStyle["visibility"] != "hidden") {
        return isRendered(domObj.parentNode);
    } else if (window.getComputedStyle) {
        var cs = document.defaultView.getComputedStyle(domObj, null);
        if (cs.getPropertyValue("display") != "none" && cs.getPropertyValue("visibility") != "hidden") {
            return isRendered(domObj.parentNode);
        }
    }
    return false;
}

function _zmvcProcessSyntaxError(scriptURL, error) {
    if (error instanceof SyntaxError) {
        alert("Syntax Error in '" + scriptURL + "'");
        var scriptTag = document.createElement('script');
        scriptTag.setAttribute('src', scriptURL);
        document.head.appendChild(scriptTag);
    } else {
        $.error("Error reading " + scriptURL + "\n" + error);
    }
}

function _zmvcRegisterPreprocessor(tagName, processor) {
    var preprocessors = _zmvcPreprocessors[tagName];
    if (!preprocessors) preprocessors = [];
    preprocessors.push(processor);
    _zmvcPreprocessors[tagName] = preprocessors;
}

function _zmvcLoadComponent(url, onReady, containerControlName, parentController) {
    var prefix = zMVCUrlPrefix ? zMVCUrlPrefix : "";
    var htmlReady = false;
    var controllerReady = false;
    var ret = { html: null, controller: null };
    $.ajax({
        url: prefix + url + ".html",
        data: null,
        success: function (html) {
            ret.html = html;
            htmlReady = true;
            if (controllerReady) onReady(ret);
        },
        error: function (xhr, errorText, errorThrown) {
            $.error("Error:" + errorThrown);
        },
        cache: false,
        dataType: "html"
    });
    $.ajax({
        url: prefix + url + ".js",
        data: null,
        success: function () {
            ret.controller = _zmvcPrepareController(controller, containerControlName, parentController);
            controllerReady = true;
            if (htmlReady) onReady(ret);
        },
        error: function (xhr, errorText, errorThrown) {
            _zmvcProcessSyntaxError(url + ".js", errorThrown);
        },
        dataType: "script"
    });
}

function _zmvcPrepareController(controller, containerControlName, parentController) {
    var base = {
        parentController: parentController,
        containerControlName: containerControlName, // loader o pager id
        _processEvent: function () {
            var controlName = arguments[0];
            var eventName = arguments[1];
            var args = [];
            for (var i = 2; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            var name = "on";
            name += controlName.substring(0, 1).toUpperCase() + controlName.substring(1);
            name += "_" + eventName;
            if (this[name]) return this[name].apply(this, args);
        },
        isVisible: function () {
            return isRendered(this.view[0]);
        },
        trigger: function () {
            if (!this.parentController) return;
            var eventName = arguments[0];
            var args = [this.containerControlName, eventName];
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            return this.parentController._processEvent.apply(this.parentController, args);
        }
    };
    return $.extend(true, base, controller);
}

function _zmvcPlays(element) {
    var types = [];
    if (element.data("z-autoload")) types.push("autoload");
    if (element.data("z-pages")) types.push("pager");

    return types;
}

var _zNewParentController = null;
function _zmvcSearchSubcomponents(widget, controller, element) {
    controller.widget = widget;
    $.each(element.children(), function (i, child) {
        var $child = $(child);
        var preprocessors = _zmvcPreprocessors[$child.prop("tagName").toLowerCase()];
        if (preprocessors) {
            for (var j = 0; j < preprocessors.length; j++) {
                preprocessors[j]($child, controller);
            }
        }
        var childId = $child.prop("id");
        var childTypes = _zmvcPlays($child);
        if (childTypes.length) {
            if (!childId) $.error("Component should have an id:" + child.outerHTML);
            widget._incPendingsCounter();
            _zNewParentController = controller;
            controller[childId] = $child.zMVC({
                rootCall: false,
                onReady: function () {
                    widget.childMVCs.push(controller[childId]);
                    widget._decPendingsCounter();
                }
            });
        } else {
            if (childId) {
                controller[childId] = $child;
            }
            _zmvcSearchSubcomponents(widget, controller, $child);
        }
    });
}
function _zmvcAnalize(widget) {
    var types = _zmvcPlays(widget.element);
    widget.types = types;
    if (widget.hasType("autoload")) {
        var url = widget.element.data("z-autoload");
        widget._incPendingsCounter();
        _zmvcLoadComponent(url, function (info) {
            widget.controller = info.controller;
            var view = $(info.html);
            widget.element.html(view);
            widget.view = view;
            widget.controller.view = widget.view;
            _zmvcSearchSubcomponents(widget, widget.controller, widget.element);
            widget._decPendingsCounter();
        }, widget.element.prop("id"), widget.options.parentController);
    }
    if (widget.hasType("pager")) {
        var pages = widget.element.data("z-pages");
        widget.pages = {};
        $.each(pages, function (i, page) {
            widget._incPendingsCounter();
            _zmvcLoadComponent(page.url, function (info) {
                var view = $(info.html);
                var pageId = "_page_" + page.code;
                widget.element.append("<div id='" + pageId + "' class='page-holder'></div>");
                widget.element.find("#" + pageId).html(view);
                if (!page.initial) {
                    widget.element.find("#" + pageId).hide();
                }
                info.controller.view = view;
                widget.pages[page.code] = { code: page.code, controller: info.controller, view: view, initial: page.initial ? true : false };
                _zmvcSearchSubcomponents(widget, info.controller, view);
                widget._decPendingsCounter();
            }, widget.element.prop("id"), widget.options.parentController);
        });
    }
    if (widget.options.isDialog) {
        widget._incPendingsCounter();
        widget.controller = widget.options.dialogInfo.controller;
        widget.view = widget.options.dialogInfo.view;
        widget.controller.view = widget.view;
        _zmvcSearchSubcomponents(widget, widget.controller, widget.element);
        widget._decPendingsCounter();
    }
}

function _zmvcInit(widget, onReady) {
    if (widget.hasType("autoload")) {
        widget.controller._processEvent("this", "init");
    }
    if (widget.hasType("pager")) {
        var initial = null;
        $.each(widget.pages, function (i, p) {
            if (p.initial) {
                initial = p;
                return false;
            }
        });
        if (!initial) $.error("Pager '" + widget.element.prop("id") + "' does not define an initial page");
        widget.controller = initial.controller;
        widget.currentPage = initial.code;
        widget.view = initial.view;
        widget.pagesStack = [initial.code];
        widget.animate = widget.element.data("z-animate");
        if (widget.element.data("z-animation-delay")) widget.animationDelay = parseInt(widget.element.data("z-animation-delay"));
        $.each(widget.pages, function (i, p) {
            p.controller._processEvent("this", "init");
        });
    }
    if (widget.options.isDialog) {
        widget.controller.close = function () {
            _zmvcDeactivate(widget);
            if (widget.options.dialogOnClose) {
                widget.options.dialogOnClose.apply(this, arguments);
            }
            widget.element.modal("hide");
        };
        widget.controller.cancel = function () {
            _zmvcDeactivate(widget);
            if (widget.options.dialogOnCancel) {
                widget.options.dialogOnCancel();
            }
            widget.element.modal("hide");
        };
        widget.controller._processEvent("this", "init", widget.options.dialogOptions);
        widget.view.modal({
            show: false,
            keyboard: true,
            backdrop: "static"
        });
        widget.view.modal("handleUpdate");
        widget.view.on("hidden.bs.modal", function () {
            widget.element.remove();
            if (widget.options.dialogOnHidden) {
                widget.options.dialogOnHidden();
            }
        });
        widget.view.modal("show");
        _zmvcActivate(widget, widget.options.dialogOptions);
    }
    if (onReady) onReady();
}

function _zmvcActivate(widget, options) {
    $.each(widget.childMVCs, function (i, subwidget) {
        subwidget.zMVC("internalActivate");
    });
    widget.activated = true;
    widget.controller._processEvent("this", "activate", options);
}
function _zmvcDeactivate(widget) {
    if (!widget.activated) return;
    $.each(widget.childMVCs, function (i, subwidget) {
        subwidget.zMVC("internalDeactivate");
    });
    widget.activated = false;
    widget.controller._processEvent("this", "deactivate");
}

function openDialog(url, options, onClose, onCancel, onHidden, onReady) {
    _zmvcLoadComponent(url, function (info) {
        var view = $(info.html);
        if (!view.prop("id")) view.prop("id", "dialog");
        info.view = view;
        view.zMVC({ isDialog: true, dialogInfo: info, dialogOptions: options, dialogOnClose: onClose, dialogOnCancel: onCancel, dialogOnHidden: onHidden, onReady:function() {
            if (onReady) onReady(view.zMVC("getController"));
        }});
        
    }, null, null);
}

$.widget(
	"z.zMVC", {
	    types: [],
	    controller: null,
	    view: null,
	    pendingsCounter: 0,
	    pendingsEndCallback: null,
	    pages: null, 		// {code:{controller:controller, view:view, initial:bool}}
	    currentPage: null, 	// code
	    animate: false,
	    animationDelay: 300,
	    pagesStack: [],
	    childMVCs: null,
	    options: {
	        isDialog: false,
	        rootCall: true,
	        onReady: null,
	        parentController: null,
	        dialogInfo: null,
	        dialogOptions: null,
	        dialogOnClose: null,
	        dialogOnCancel: null
	    },
	    _create: function () {
	        var thisWidget = this;
	        this.childMVCs = [];
	        this.options.parentController = _zNewParentController;
	        _zNewParentController = null;
	        this.pendingsEndCallback = this._initControllers;
	        _zmvcAnalize(this);
	    },
	    _incPendingsCounter: function () {
	        this.pendingsCounter++;
	    },
	    _decPendingsCounter: function () {
	        this.pendingsCounter--;
	        if (!this.pendingsCounter && this.pendingsEndCallback) this.pendingsEndCallback();
	    },
	    _initControllers: function () {
	        var thisWidget = this;
	        _zmvcInit(this, function () {
	            if (thisWidget.options.rootCall) {
	                _zmvcActivate(thisWidget);
	            }
	            if (thisWidget.options.onReady) thisWidget.options.onReady(thisWidget.controller);
	        });
	    },
	    internalActivate: function () {
	        _zmvcActivate(this);
	    },
	    internalDeactivate: function () {
	        _zmvcDeactivate(this);
	    },
	    hasType: function (type) {
	        return this.types.indexOf(type) >= 0;
	    },
	    getController: function () {
	        return this.controller;
	    },
	    getView: function () {
	        return this.view;
	    },

	    // autoload
	    load: function (url, onReady) {
	        var thisWidget = this;
	        if (this.controller) _zmvcDeactivate(thisWidget);
	        this.childMVCs = [];
	        this.pendingsEndCallback = function () {
	            thisWidget.controller._processEvent("this", "init");
	            _zmvcActivate(thisWidget);
	            if (onReady) onReady(thisWidget.controller);
	        }
	        this._incPendingsCounter();
	        _zmvcLoadComponent(url, function(info) {
	            thisWidget.controller = info.controller;
	            var view = $(info.html);
	            thisWidget.element.html(view);
	            thisWidget.view = view;
	            thisWidget.controller.view = view;
	            _zmvcSearchSubcomponents(thisWidget, thisWidget.controller, thisWidget.element);
	            thisWidget._decPendingsCounter();
	        }, thisWidget.element.prop("id"), thisWidget.options.parentController);
	    },

	    // pager
	    page: function (code) {
	        if (code === undefined) return this.currentPage;
	        this.pagesStack = [code];
	        var p = this.pages[code];
	        if (!p) $.error("page '" + code + "' not found in pager '" + this.element.prop("id") + "'");
	        if (this.controller) _zmvcDeactivate(this);
	        this.controller = p.controller;
	        this.view = p.view;
	        var pageId = "_page_" + code;
	        this.element.find(".page-holder").hide();
	        this.element.find("#" + pageId).show();
	        this.currentPage = code;
	        _zmvcActivate(this);
	        return this.controller;
	    },
	    getPageController: function (code) {
	        if (code && !this.pages[code]) $.error("Page '" + code + "' not found in this pager");
	        var p = this.pages[code ? code : this.currentPage];
	        return p.controller;
	    },
	    getPageView: function (code) {
	        var p = this.pages[code ? code : this.currentPage];
	        return p.view;
	    },
	    buscaHeightEnHijos: function (element) {
	        var self = this;
	        // Si tiene h se le cree. Si no (cero) se busca en hijos recursivamente
	        var h = element.height();
	        var maxH = -1;
	        if (h) return h;
	        $.each(element.children(), function (i, child) {
	            var childH = self.buscaHeightEnHijos($(child));
	            if (i == 0 || childH > maxH) maxH = childH;
	        });
	        return maxH;
	    },
	    _anima: function (oldPage, newPage, direction, duration, complete) {
	        var thisController = this;
	        newPage.show();
	        var oldH = this.buscaHeightEnHijos(oldPage);
	        var newH = this.buscaHeightEnHijos(newPage);
	        var h = Math.max(oldH, newH);
	        this.element.css({ "overflow": "hidden", height: h + "px", position: "relative" });
	        var w = oldPage.width();
	        var oldLeftStart = 0;
	        var oldLeftEnd = direction == "left" ? -w : w;
	        var newLeftStart = direction == "left" ? w : -w;
	        var newLeftEnd = 0;

	        oldPage.css({ position: "absolute", width: w + "px", height: h + "px" });
	        newPage.css({ position: "absolute", left: newLeftStart + "px", top: "0", width: w + "px", height: h + "px" });

	        oldPage.animate({ left: oldLeftEnd + "px" }, { duration: duration, queue: false });
	        newPage.animate({ left: newLeftEnd + "px" }, {
	            duration: duration, queue: false, complete: function () {
	                thisController.element.css({ "overflow": "", height: "", position: "" });
	                oldPage.css({ position: "", left: "", top: "", width: "", height: "" });
	                newPage.css({ position: "", left: "", top: "", width: "", height: "" });
	                oldPage.hide();
	                if (complete) complete();
	            }
	        });
	    },
	    push: function (code, animate) {
	        if (this.controller) _zmvcDeactivate(this);
	        var oldPageId = "_page_" + this.currentPage;
	        var p = this.pages[code];
	        if (!p) $.error("page '" + code + "' not found in pager '" + this.element.prop("id") + "'");
	        if (this.controller) this.controller._processEvent("this", "deactivate");
	        this.controller = p.controller;
	        this.view = p.view;
	        var pageId = "_page_" + code;
	        var oldPage = this.element.find("#" + oldPageId);
	        var newPage = this.element.find("#" + pageId);
	        this.currentPage = code;
	        this.pagesStack.push(code);
	        if (this.animate || animate) {
	            this._anima(oldPage, newPage, "left", this.animationDelay);
	        } else {
	            this.element.find(".page-holder").hide();
	            newPage.show();
	        }
	        _zmvcActivate(this);
	        return this.controller;
	    },
	    pop: function (animate) {
	        var oldPageId = "_page_" + this.currentPage;
	        this.pagesStack.splice(this.pagesStack.length - 1, 1);
	        var code = this.pagesStack[this.pagesStack.length - 1];
	        var p = this.pages[code];
	        if (this.controller) _zmvcDeactivate(this);
	        this.controller = p.controller;
	        this.view = p.view;
	        var pageId = "_page_" + code;
	        var oldPage = this.element.find("#" + oldPageId);
	        var newPage = this.element.find("#" + pageId);
	        this.currentPage = code;
	        if (this.animate || animate) {
	            this._anima(oldPage, newPage, "right", this.animationDelay);
	        } else {
	            this.element.find(".page-holder").hide();
	            this.element.find("#" + pageId).show();
	        }
	        _zmvcActivate(this);
	        return this.controller;
	    }
	}
);
