_zmvcRegisterPreprocessor("button", function(element, controller) {
	element.click(function() {
		controller._processEvent(element.prop("id"), "click");
	});
});
_zmvcRegisterPreprocessor("a", function(element, controller) {
	element.click(function() {
		controller._processEvent(element.prop("id"), "click");
	});
});
_zmvcRegisterPreprocessor("span", function(element, controller) {
	element.click(function() {
		controller._processEvent(element.prop("id"), "click");
	});
});
_zmvcRegisterPreprocessor("select", function(element, controller) {
	element.change(function() {
		controller._processEvent(element.prop("id"), "change");
	});
	addValidationFunctions(element);
});
_zmvcRegisterPreprocessor("input", function(element, controller) {	
	if (element.data("z-autochange-delay")) {
		var delay = parseInt(element.data("z-autochange-delay"));
		if (!isNaN(delay)) {
			element.autoChangeDelay = delay;
			element.clearChangeTimer = function() {
				if (element.changeTimer) {
					clearTimeout(element.changeTimer);
					element.changeTimer = null;
				}
			};
			element.lastVal = "";
			element.scheduleRefresh = function() {
				element.clearChangeTimer();
				element.changeTimer = setTimeout(function() {
				    element.clearChangeTimer();
				    if (element.lastVal != element.val()) {
				        controller._processEvent(element.prop("id"), "change");
				        element.lastVal = element.val();
				    }
				}, element.autoChangeDelay);
			};
			element.clear = function (e) {
			    element.val("");
			    element.scheduleRefresh();
			};
			element.keyup(function (e) {
				element.scheduleRefresh();
			});
			element.change(function(e) {
				element.scheduleRefresh();
			});
		}
	} else {
		element.change(function() {
			controller._processEvent(element.prop("id"), "change");
		});
		element.keyup(function(e) {
			controller._processEvent(element.prop("id"), "keyup", e);
		});
		element.keydown(function(e) {
			controller._processEvent(element.prop("id"), "keydown", e);
		});
		element.keypress(function(e) {
			controller._processEvent(element.prop("id"), "keypress", e);
		});
		if (element.data("z-datepicker")) {
			element.datepicker({language:"es", format:"dd/M/yyyy", autoclose:true});
			element.datepicker("setDate", new Date());
			element.setDate = function(d) {element.datepicker("setDate", d);};
			element.getDate = function() {
				var dt = new Date(element.datepicker("getDate").getTime());
				dt.setHours(0);
				dt.setMinutes(0);
				dt.setSeconds(0);
				dt.setMilliseconds(0);
				return dt;
			};
			element.show = function() {element.datepicker("show");};
		}
	}
	addValidationFunctions(element);
});
_zmvcRegisterPreprocessor("table", function(element, controller) {
	element.rows = [];
	element.cols = [];
	element.hiddenCols = {};
	element.editableCols = {};
	element.clickableCols = {};
	element.cellClasses = {};
	element.find("th").each(function() {
		var th = $(this);
		if (th.data("z-col")) {
			element.cols.push(th.data("z-col"));
			if (th.data("z-editable")) element.editableCols[element.cols.length - 1] = true;
			if (th.data("z-clickable")) element.clickableCols[element.cols.length - 1] = true;
			if (th.data("z-cell-class")) element.cellClasses[element.cols.length - 1] = th.data("z-cell-class");
		} else {
			$.error("No data-z-col in <th> for table '" + element.prop("id") + "'");
		}
	});
	if (element.data("z-edit")) {
		element.cols.push("-edit-");
		element.find("thead").find("tr").append("<th class='text-right'></th>");
	}
	element.getDetailsComponent = function() {
		var id = element.data("z-page-details");
		if (id == null) return null;
		return controller.view.find("#" + id);
	};
	element.getPagerComponent = function() {
		var id = element.data("z-pager");
		if (id == null) return null;
		return controller.view.find("#" + id);
	};
	if (element.getPagerComponent()) {
		var paginator = element.getPagerComponent();
		element.pagination = paginator.data("z-pagination")?paginator.data("z-pagination"):"local";
		if (element.pagination != "local" && element.pagination != "server") $.error("data-z-pagination in " + paginator.prop("id") + " must be 'local' or 'server'");
		var nRows = parseInt(paginator.data("z-n-rows"));
		nRows = isNaN(nRows)?20:nRows;
		var nPages = parseInt(paginator.data("z-n-pages"));
		nPages = isNaN(nPages)?10:nPages;
		element.pagNRows = nRows;
		element.pagNPages = nPages;
	} else {
		element.pagination = false;
	}
	var selectableRowClass = element.data("z-selectable-row-class");
	if (!selectableRowClass) element.selectableRowClass = false;
	else element.selectableRowClass = selectableRowClass;
	element.selectedRowIndex = -1;
	
	element.clearTable = function() {
		element.find("tbody").empty();
		if (element.getDetailsComponent()) element.getDetailsComponent().html("");
		if (element.getPagerComponent()) {
			element.getPagerComponent().html("");
			element.getPagerComponent().hide();
		}
	};
	element.setWorking = function() {
		if (element.getDetailsComponent()) {
			element.getDetailsComponent().html("<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>");
		}
	};
	element.refresh = function(keepSelection, onReady) {
		element.rows = [];		
		element.clearTable();
		element.setWorking();
		element.nRows = 0;
		element.nPages = 0;
		if (!keepSelection) {
			element.currentPage = 0;
			element.startPage = 0;
			element.selectedRowIndex = -1;
		}
		if (!element.pagination || element.pagination == "local") {
			controller._processEvent(element.prop("id"), "getRows", function(rows) {
				element.rows = rows;
				if (!rows || !rows.length) {
					element.nPages = 0;
					element.nRows = 0;
					element.currentPage = 0;
				} else {
					element.nRows = rows.length;
					if (element.pagination) {
						var nPages = rows.length / element.pagNRows;
						if (parseInt(nPages) != nPages) nPages = parseInt(nPages) + 1;
						element.nPages = nPages;
						if (element.currentPage > element.nPages  -1) element.currentPage = element.nPages - 1;
						if (element.startPage > element.nPages  -1) element.startPage = element.nPages - 1;
					}
				}
				element.paint();
				if (onReady) onReady();
			});
		} else {
			controller._processEvent(element.prop("id"), "countRows", function(n) {
				element.nRows = n;
				var nPages = n / element.pagNRows;
				if (parseInt(nPages) != nPages) nPages = parseInt(nPages) + 1;
				element.nPages = nPages;
				if (element.currentPage > element.nPages  -1) element.currentPage = element.nPages - 1;
				if (element.startPage > element.nPages  -1) element.startPage = element.nPages - 1;
				element.refreshPage(onReady);
			});
		}		
	};
	element.update = function() {
		element.refresh(true);
	};
	element.refreshPage = function(onReady) {
		element.setWorking();
		if (!element.nRows) {
			element.paint();
			if (onReady) onReady();
			return;
		}
		var startRow = element.currentPage * element.pagNRows;
		var count = element.pagNRows;
		if (startRow + count > element.nRows) count = element.nRows - startRow;
		controller._processEvent(element.prop("id"), "getPage", startRow, count, function(rows) {
			element.rows = rows;
			element.paint();
			if (onReady) onReady();
		});
	};
	element.updateRow = function(rowIndex, row) {
		element.rows[rowIndex] = row;
		element.clearTable();
		element.paint();
	};
	element.updateAllRows = function (rows) {
	    element.rows = rows;
	    element.clearTable();
	    element.paint();
	},
	element.deleteRow = function(rowIndex) {
		element.rows.splice(rowIndex, 1);
		element.nRows = element.rows.length;
		if (element.pagination) {
			var rows = element.rows;
			if (!rows || !rows.length) {
				element.nPages = 0;
				element.currentPage = 0;
				element.startPage = 0;
				element.selectedRowIndex = -1;
			} else {
				var nPages = rows.length / element.pagNRows;
				if (parseInt(nPages) != nPages) nPages = parseInt(nPages) + 1;
				element.nPages = nPages;
				if (element.currentPage > element.nPages  -1) element.currentPage = element.nPages - 1;
				if (element.startPage > element.nPages  -1) element.startPage = element.nPages - 1;
			}
		}
		element.clearTable();
		element.paint();
	};
	element.getSelectedRowIndex = function() {
		return element.selectedRowIndex;
	};
	element.getSelectedRow = function() {
		var row = null;
		if (element.selectedRowIndex < 0 || element.selectedRowIndex > (element.nRows - 1)) return null;
		if (!element.pagination || element.pagination == "local") {
			row = element.rows[element.selectedRowIndex];
		} else {
			if (element.selectedRowIndex >= element.pagNRows * element.currentPage && element.selectedRowIndex < element.pagNRows * (element.currentPage + 1)) {
				row = element.rows[element.selectedRowIndex - element.pagNRows * element.currentPage];
			}
		}
		return row;
	};
	element.setSelectedRowIndex = function(rowIndex, fireEvent) {
		if (element.selectedRowIndex >= 0) {
			element.find("#r_" + element.selectedRowIndex).removeClass(element.selectableRowClass);
		}
		if (rowIndex >= 0 && rowIndex <= (element.nRows - 1)) {
			element.selectedRowIndex = rowIndex;
			element.find("#r_" + element.selectedRowIndex).addClass(element.selectableRowClass);
			if (fireEvent) {
				var row = null;
				if (!element.pagination || element.pagination == "local") {
					row = element.rows[element.selectedRowIndex];
				} else {
					if (element.selectedRowIndex >= element.pagNRows * element.currentPage && element.selectedRowIndex < element.pagNRows * (element.currentPage + 1)) {
						row = element.rows[element.selectedRowIndex - element.pagNRows * element.currentPage];
					}
				}
				controller._processEvent(element.prop("id"), "rowSelected", element.selectedRowIndex, row);
			}
		} else {
			$.error("Invalid rowIndex " + rowIndex + "/" + element.nRows);
		}
	};
	element.getRowCount = function() {
		return element.nRows;
	};
	element.hideColumn = function(name) {
	    element.hiddenCols[name] = true;
	    element.paint();
	};
	element.showColumn = function (name) {
	    element.hiddenCols[name] = false;
	    element.paint();
	};
	element.paint = function () {
	    if (!element.rows) return;
		var html = "";
		var startRow = 0;
		var endRow = element.rows.length;
		if (element.pagination == "local") {
			startRow = element.currentPage * element.pagNRows;
			endRow = startRow + element.pagNRows;
			if (endRow > element.rows.length) endRow = element.rows.length;
		}
	    // Mostrar esconder columnas en header
		$.each(element.find("th"), function (i, th) {
		    var colName = $(th).data("z-col");
		    if (colName) {
		        if (element.hiddenCols[colName]) $(th).hide();
		        else $(th).show();
		    }
		});

		if (startRow < element.rows.length) {
			for (var i=startRow; i<endRow; i++) {
				var row = element.rows[i];
				html += "<tr id='r_" + i + "' data-index='" + i + "' " + (row._rowClass?"class='" + row._rowClass + "'":"") + ">";
				$.each(element.cols, function (j, col) {
				    if (!element.hiddenCols[col]) {
				        if (col == "-edit-") {
				            html += "<td class='text-right'>";
				            html += "<span class='editador' data-index='" + i + "' style='cursor: pointer;'><i class='fa-sm far fa-edit'></i></span>";
				            html += "<span class='eliminador' data-index='" + i + "' style='cursor: pointer;'><i class='fa-sm far fa-trash-alt'></i></span>";
				            var extra = row._extraEditControls;
				            if (extra) {
				                $.each(extra, function (k, c) {
				                    html += "<span class='extra-edit-control' data-index='" + i + "' data-name='" + c.name + "' style='cursor: pointer;'><i class='" + c.icon + " ml-1' ></i></span>";
				                });
				            }
				            html += "</td>";
				        } else {
				            var val = row[col] ? row[col] : "";
				            var cellClass = element.cellClasses[j];
				            if (element.editableCols[j]) {
				                val = "<a href='#' data-index='" + i + "' class='editador' style='cursor: pointer;'>" + val + "</a>";
				            } else if (element.clickableCols[j]) {
				                val = "<a href='#' data-index='" + i + "' data-col='" + col + "' class='clickador' style='cursor: pointer;'>" + val + "</a>";
				            }
				            html += "<td" + (cellClass ? " class='" + cellClass + "'" : "") + ">" + val + " </td>";
				        }
				    }
				});
				html += "</tr>";
			}
		}
		element.find("tbody").html(html);
		element.find("tbody").find(".editador").click(function(e) {
			e.preventDefault();
			var idx = parseInt($(this).data("index"));
			var row = element.rows[idx];
			controller._processEvent(element.prop("id"), "editRequest", idx, row);
		});
		element.find("tbody").find(".clickador").click(function(e) {
			e.preventDefault();
			var idx = parseInt($(this).data("index"));
			var col = $(this).data("col");
			var row = element.rows[idx];
			controller._processEvent(element.prop("id"), "columnClick", idx, col, row);
		});
		if (element.selectableRowClass) {
			element.find("tbody").find("tr").click(function() {
				var idx = parseInt($(this).data("index"));
				element.setSelectedRowIndex(idx, true);
			});
		}
		element.find("tbody").find(".extra-edit-control").click(function(e) {
			e.preventDefault();
			var idx = parseInt($(this).data("index"));
			var row = element.rows[idx];
			var controlName = $(this).data("name");
			controller._processEvent(element.prop("id"), "extraEditControlClick", idx, controlName, row);
		});
		element.find("tbody").find(".eliminador").click(function(e) {
			e.preventDefault();
			var idx = parseInt($(this).data("index"));
			var row = element.rows[idx];
			controller._processEvent(element.prop("id"), "deleteRequest", idx, row);
		});
		if (element.selectableRowClass && element.selectedRowIndex >= 0) {
			element.setSelectedRowIndex(element.selectedRowIndex, false);
		}
		// Refrescar detalles
		if (element.getDetailsComponent()) {
			if (!element.pagination) {
				if (element.nRows == 1) {
					element.getDetailsComponent().text("Una fila encontrada");
				} else if (element.nRows > 1) {
					element.getDetailsComponent().text(element.nRows + " filas encontradas");
				} else {
					element.getDetailsComponent().text("No se encontraron resultados");
				}
			} else {
				if (element.nPages <= 1) {
					if (element.nRows == 1) {
						element.getDetailsComponent().text("Una fila encontrada");
					} else if (element.nRows > 1) {
						element.getDetailsComponent().text(element.nRows + " filas encontradas");
					} else {
						element.getDetailsComponent().text("No se encontraron resultados");
					}
				} else {
					var msg = "Página " + (element.currentPage + 1) + "/" + element.nPages;
					if (element.pagination == "local") {
						msg += ", mostrando filas " + (startRow + 1) + " - " + endRow + " de " + element.nRows;
					} else {
					    var nn = ((element.currentPage + 1) * element.pagNRows);
					    if (nn > element.nRows) nn = element.nRows;
						msg += ", mostrando filas " + (element.currentPage * element.pagNRows + 1) + " - " + nn + " de " + element.nRows;
					}
					element.getDetailsComponent().text(msg);					
				}
			}
		}
		// Refrescar paginador
		if (element.pagination) {
			var pager = element.getPagerComponent();
			if (element.nPages <= 1) pager.hide();
			else {
				var page0 = element.startPage;
				var page1 = element.startPage + element.pagNPages;
				if (page1 > element.nPages) page1 = element.nPages;
				var prior = page0 > 0;
				var next = page1 < element.nPages;
				var html = "";
				html += "<li class='page-item " + (prior?"":"disabled") + "' >";
				html += "  <a id='pprev' class='page-link' href='#' aria-label='Anterior'>";
				html += "    <span aria-hidden='true'>&laquo;</span>";
				html += "    <span class='sr-only'>Anterior</span>";
				html += "  </a>";
				html += "</li>";
				for (var pag=page0; pag < page1; pag++) {
					html += "<li class='page-item";
					if (pag == element.currentPage) html += " active";
					html += "'><a href='#' class='page-link inter-page' data-page='" + pag + "'>" + (pag + 1) + "</a></li>";
				}
				html += "<li class='page-item " + (next?"":"disabled") + "' >";
				html += "  <a id='pnext' class='page-link' href='#' aria-label='Siguiente' >";
				html += "    <span aria-hidden='true'>&raquo;</span>";
				html += "    <span class='sr-only'>Siguiente</span>";
				html += "  </a>";
				html += "</li>";
				pager.hide();
				pager.html(html);
				pager.show();
				if (prior) {
					pager.find("#pprev").click(function(e) {
						e.preventDefault();
						element.startPage -= element.pagNPages;
						if (element.startPage < 0) element.startPage = 0;
						element.currentPage -= element.pagNPages;
						if (element.currentPage < 0) element.currentPage = 0;
						if (element.pagination == "local") element.paint();
						else element.refreshPage();
					});
				}
				if (next) {
					pager.find("#pnext").click(function(e) {
						e.preventDefault();
						element.startPage += element.pagNPages;
						if (element.startPage > element.nPages - 1) element.startPage = element.nPages - 1;
						element.currentPage += element.pagNPages;						
						if (element.currentPage > element.nPages - 1) element.currentPage = element.nPages - 1;
						if (element.pagination == "local") element.paint();
						else element.refreshPage();
					});
				}
				pager.find(".inter-page").click(function(e) {
					e.preventDefault();
					element.currentPage = parseInt($(this).data("page"));
					if (element.pagination == "local") element.paint();
					else element.refreshPage();
				});
			}
		}
	};
});


// Utilidades de Validación en formularios
// Se agregan a los editores finales
function clearValidation(control) {
	control.removeClass("is-valid");
	control.removeClass("is-invalid");
	var p = control.parent();
	if (p.hasClass("input-group")) p = p.parent();
	var fb = p.find(".invalid-feedback");
	if (fb && fb.length) fb.hide();
}
function setValidationOK(control) {
	clearValidation(control);
	control.addClass("is-valid");
}

function setValidationError(control, error, scroll) {
	var p = control.parent();
	if (p.hasClass("input-group")) p = p.parent();
	var fb = p.find(".invalid-feedback");
	if (!fb || !fb.length) {
	    p.append("<div class='invalid-feedback'></div>");
   		fb = p.find(".invalid-feedback");
	}
	fb.text(error);
	control.addClass("is-invalid");
	fb.show();
	if (scroll) {
		$('html, body').animate({
	        scrollTop: control.offset().top
	    }, 300);
	}
}

function addValidationFunctions(control) {
	control.clearValidation = function() {clearValidation(control);};
	control.setValidationOK = function() {setValidationOK(control);};
	control.setValidationError = function(error, scroll) {setValidationError(control, error, scroll);};
}