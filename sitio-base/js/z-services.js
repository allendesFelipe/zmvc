var securityToken = null;
var zUrlPrefix = "";

function httpInvoke(serviceName, arg, success, error) {
	if (arg == null) arg={};
	if (securityToken != null) arg.token = securityToken;
	return $.ajax({
	    url: zUrlPrefix + serviceName,
	    dataType: "json",
        contentType:"application/json",
        data:JSON.stringify(arg),
	    type: "POST",
	    crossDomain:true, 
	    success: function (ret) {
	        if (ret.status == "OK") {
	            if (success != null) {
	                if (ret.object != null) {
	                    if (ret.basicReturnType) success(ret.object.value);
	                    else success(ret.object);
	                }
	                else if (ret.collection != null) success(ret.collection);
	                else success();
	            }
	        } else {
	            if (error != null) {
	                error(ret.message);
	            } else {
	            	if (defaultZErrorHandler) {
	            		defaultZErrorHandler(ret.message);
	            	} else {
	            		alert(ret.message, "Error");
	            	}
	            }
	        }
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	if (textStatus == "abort") return;
	        if (error != null) {
	            error(textStatus);
	        } else {
	        	if (defaultZErrorHandler) {
            		defaultZErrorHandler(textStatus);
            	} else {
            		alert(textStatus, "Error");
            	}
	        }		        
	    }
	});
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
