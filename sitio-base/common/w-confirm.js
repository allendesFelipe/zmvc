var controller = {
	onThis_init:function(options) {
		if (options.title) this.title.text(options.title);
		this.msg.html(options.message);
	},
	onCmdOK_click:function() {
		this.close();
	}
}