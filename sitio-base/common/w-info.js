var controller = {
	onThis_init:function(options) {
		if (options.title) this.title.text(options.title);
		this.msg.html(options.message);
	},
	onCmdClose_click: function () {
	    this.close();
	},
	onTopClose_click: function () {
	    this.close();
	}
}