var zsModules = {};
exports.registerModule = function(extension, module) {
	zsModules[extension] = module;
};

exports.resolve = function(req, res) {
	var p0 = req.url.lastIndexOf("/");
	var p1 = req.url.lastIndexOf(".");
	var ext = req.url.substring(p1+1);
	var ope = req.url.substring(p0+1, p1);
	var arg = req.body;
	
	var module = zsModules[ext];
	if (!module) throw("No module registered for extension '" + ext + "'");
	var func = module[ope];
	if (!func || !(func instanceof Function))  throw("No operation '" + ope + "' found in module '" + ext + "'");

	res.setHeader('Content-Type', 'application/json');
	var buildResponse = function(ret) {
		var r = {status:"OK", message:"OK"};
		if (ret) {
			if (Array.isArray(ret)) {
				r.collection = ret;
			} else if (ret instanceof Object) {
				r.object = ret;
			} else {
				r.basicReturnType = true;
				r.object = {value:ret};
			}
		}
		return r;
	}
	var buildErrorResponse = function(err) {
		console.log("error", err);
		var r = {status:"ERROR", message:err};
		return r;
	}
	var params = _zsgetParamNames(func);
	var paramValues = [];
	for (var i=0; i<params.length; i++) {
		paramValues.push(arg[params[i]]);
	}
	try {
		var resultOrPromise = func.apply(module, paramValues);
		if (resultOrPromise instanceof Promise) {
			resultOrPromise
			.then((ret) => {
				res.send(JSON.stringify(buildResponse(ret)));
			})
			.catch((err) => {
				res.send(JSON.stringify(buildErrorResponse(err)));
			});
		} else  {
			res.send(JSON.stringify(buildResponse(resultOrPromise)));
		}
	} catch(err) {
		res.send(JSON.stringify(buildErrorResponse(err)));
	}
}

var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function _zsgetParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null) result = [];
  return result;
}